// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Constance.proto
#pragma warning disable 1591, 0612, 3021
#region Designer generated code

using pb = global::Google.Protobuf;
using pbc = global::Google.Protobuf.Collections;
using pbr = global::Google.Protobuf.Reflection;
using scg = global::System.Collections.Generic;
namespace Gear.Runtime.Data.Gen {

  /// <summary>Holder for reflection information generated from Constance.proto</summary>
  public static partial class ConstanceReflection {

    #region Descriptor
    /// <summary>File descriptor for Constance.proto</summary>
    public static pbr::FileDescriptor Descriptor {
      get { return descriptor; }
    }
    private static pbr::FileDescriptor descriptor;

    static ConstanceReflection() {
      byte[] descriptorData = global::System.Convert.FromBase64String(
          string.Concat(
            "Cg9Db25zdGFuY2UucHJvdG8SFUdlYXIuUnVudGltZS5EYXRhLkdlbhoKUGFy",
            "dC5wcm90byI6CglDb25zdGFuY2USLQoIcGFydExpc3QYASADKAsyGy5HZWFy",
            "LlJ1bnRpbWUuRGF0YS5HZW4uUGFydGIGcHJvdG8z"));
      descriptor = pbr::FileDescriptor.FromGeneratedCode(descriptorData,
          new pbr::FileDescriptor[] { global::Gear.Runtime.Data.Gen.PartReflection.Descriptor, },
          new pbr::GeneratedClrTypeInfo(null, new pbr::GeneratedClrTypeInfo[] {
            new pbr::GeneratedClrTypeInfo(typeof(global::Gear.Runtime.Data.Gen.Constance), global::Gear.Runtime.Data.Gen.Constance.Parser, new[]{ "PartList" }, null, null, null)
          }));
    }
    #endregion

  }
  #region Messages
  public sealed partial class Constance : pb::IMessage<Constance> {
    private static readonly pb::MessageParser<Constance> _parser = new pb::MessageParser<Constance>(() => new Constance());
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pb::MessageParser<Constance> Parser { get { return _parser; } }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public static pbr::MessageDescriptor Descriptor {
      get { return global::Gear.Runtime.Data.Gen.ConstanceReflection.Descriptor.MessageTypes[0]; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    pbr::MessageDescriptor pb::IMessage.Descriptor {
      get { return Descriptor; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Constance() {
      OnConstruction();
    }

    partial void OnConstruction();

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Constance(Constance other) : this() {
      partList_ = other.partList_.Clone();
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public Constance Clone() {
      return new Constance(this);
    }

    /// <summary>Field number for the "partList" field.</summary>
    public const int PartListFieldNumber = 1;
    private static readonly pb::FieldCodec<global::Gear.Runtime.Data.Gen.Part> _repeated_partList_codec
        = pb::FieldCodec.ForMessage(10, global::Gear.Runtime.Data.Gen.Part.Parser);
    private readonly pbc::RepeatedField<global::Gear.Runtime.Data.Gen.Part> partList_ = new pbc::RepeatedField<global::Gear.Runtime.Data.Gen.Part>();
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public pbc::RepeatedField<global::Gear.Runtime.Data.Gen.Part> PartList {
      get { return partList_; }
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override bool Equals(object other) {
      return Equals(other as Constance);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public bool Equals(Constance other) {
      if (ReferenceEquals(other, null)) {
        return false;
      }
      if (ReferenceEquals(other, this)) {
        return true;
      }
      if(!partList_.Equals(other.partList_)) return false;
      return true;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override int GetHashCode() {
      int hash = 1;
      hash ^= partList_.GetHashCode();
      return hash;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public override string ToString() {
      return pb::JsonFormatter.ToDiagnosticString(this);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void WriteTo(pb::CodedOutputStream output) {
      partList_.WriteTo(output, _repeated_partList_codec);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public int CalculateSize() {
      int size = 0;
      size += partList_.CalculateSize(_repeated_partList_codec);
      return size;
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(Constance other) {
      if (other == null) {
        return;
      }
      partList_.Add(other.partList_);
    }

    [global::System.Diagnostics.DebuggerNonUserCodeAttribute]
    public void MergeFrom(pb::CodedInputStream input) {
      uint tag;
      while ((tag = input.ReadTag()) != 0) {
        switch(tag) {
          default:
            input.SkipLastField();
            break;
          case 10: {
            partList_.AddEntriesFrom(input, _repeated_partList_codec);
            break;
          }
        }
      }
    }

  }

  #endregion

}

#endregion Designer generated code
