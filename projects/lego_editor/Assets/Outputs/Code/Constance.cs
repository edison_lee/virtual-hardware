﻿using System.Collections.Generic;
using UnityEngine;
public class Constance : ScriptableObject
{
	public List<ActionData> ActionDataList = new List<ActionData>();
	public List<DeviceData> DeviceDataList = new List<DeviceData>();
	public List<PartData> PartDataList = new List<PartData>();
	public List<QuestionData> QuestionDataList = new List<QuestionData>();
	public List<TipData> TipDataList = new List<TipData>();
	public List<ToolData> ToolDataList = new List<ToolData>();
}