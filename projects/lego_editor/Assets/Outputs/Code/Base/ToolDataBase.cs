﻿using UnityEngine;
public class ToolDataBase : ScriptableObject
{
	public int id;
	public string toolName;
	public string imageName;
	public string modelName;
	public int order;
	public bool needParam;
}