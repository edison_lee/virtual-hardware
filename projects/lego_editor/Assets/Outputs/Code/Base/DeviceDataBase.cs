﻿using UnityEngine;
public class DeviceDataBase : ScriptableObject
{
	public int id;
	public string model;
	public int type;
}