using UnityEngine;
public class LegoPartBehaviour : MonoBehaviour
{
    [SerializeField]
    private int m_PartId;
    public int PartId { get { return m_PartId; } set { m_PartId = value; } }
    void OnMouseUpAsButton()
    {
        this.SendMessageUpwards("OnPartClick", this.name);
    }
}