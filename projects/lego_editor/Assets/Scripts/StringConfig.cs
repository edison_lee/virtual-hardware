using UnityEngine;

using Gear;
public static class StringConfig
{
    private static DataSOConfig m_Config;
    public static DataSOConfig Config
    {
        get
        {
            if (m_Config == null)
            {
                m_Config = Resources.Load<DataSOConfig>("DSOConfig");
            }
            return m_Config;
        }
    }
    public static string PathOutputBundle
    {
        get
        {
            return Config.OutputABPath;
        }
    }
}
