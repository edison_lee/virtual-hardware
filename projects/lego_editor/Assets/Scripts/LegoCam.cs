﻿using UnityEngine;
using UnityEngine.EventSystems;
public class LegoCam : MonoBehaviour
{
    public Transform Target;
    private float ViewRotateSpeedX = 10f;
    private float ViewRotateSpeedY = 8f;
    private float MaxDist = 4f;
    private float MinDist = 0.2f;
    private float Distance = 2f;
    public float ZoomSpeed = 0.04f;
    private float x = 45f;
    private float y = 45f;
    private Vector2 oldPosition1;
    private Vector2 oldPosition2;
    public static LegoCam Instance;
    private bool dirty = false;
    private bool isScaling = false;
    private int m_LastTouchCount = 0;
    public void ForceDirty()
    {
        dirty = true;
    }
    void Awake()
    {
        Instance = this;
    }
    void OnDestroy()
    {
        Instance = null;
    }
    private bool debug_flag = true;
    void Update()
    {
        if (Target == null) return;
#if UNITY_EDITOR || UNITY_STANDALONE
        mouseProcess();

#else

        int ncount = Input.touchCount;
        bool isSameCount = m_LastTouchCount == ncount;
        if (ncount == 0)
        {
            //无触摸
            debug_flag = true;
        }
        else if (ncount == 1)
        {
            Touch touch = Input.GetTouch(0);

            //单指触摸，如果
            if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                return;

            if (Input.GetTouch(0).phase == TouchPhase.Moved)
                updateXY();
        }
        else if (ncount == 2)
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            if (EventSystem.current.IsPointerOverGameObject(touch1.fingerId) || EventSystem.current.IsPointerOverGameObject(touch2.fingerId))
                return;

            if (isSameCount)
            {
                if (touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
                {
                    var tempPosition1 = touch1.position;
                    var tempPosition2 = touch2.position;
                    calcDist(isEnlarge(oldPosition1, oldPosition2, tempPosition1, tempPosition2));
                    oldPosition1 = tempPosition1;
                    oldPosition2 = tempPosition2;
                }
            }
            else
            {
                oldPosition1 = touch1.position;
                oldPosition2 = touch2.position;
            }
        }
        else
        {
            // //大于2指触摸
            // if (debug_flag)
            // {
            //     if (ncount == 6)
            //     {
            //         if (PanelOperation.Instance != null)
            //         {
            //             PanelOperation.Instance.ShowOrHideDebug();
            //             debug_flag = false;
            //         }
            //     }
            // }
        }
        m_LastTouchCount = ncount;
#endif
    }
    void LateUpdate()
    {
        if (Target == null) return;
        // if (!dirty) return;
        // y = ClampAngle(y, yMinLimit, yMaxLimit);
        Quaternion rotation = Quaternion.Euler(y, x, 0);
        transform.rotation = rotation;
        transform.position = rotation * (Vector3.forward * -Distance) + Target.position;
        // dirty = false;
    }

    private void mouseProcess()
    {
        // if (EventSystem.current.IsPointerOverGameObject()) return;
        ZoomSpeed = 0.1f;
        if (Input.GetMouseButton(0))
        {
            updateXY();
        }

        // 鼠标滚轮调整相机距离，向上滚动拉近，向下滚动拉远
        float mouseScroll = Input.GetAxis("Mouse ScrollWheel");
        if (mouseScroll != 0)
        {
            calcDist(mouseScroll > 0);
        }
    }

    private void updateXY()
    {
        float offsetx = Input.GetAxis("Mouse X") * ViewRotateSpeedX;
        float offsety = Input.GetAxis("Mouse Y") * ViewRotateSpeedY;

        if (offsetx != 0f)
        {
            x += offsetx;
            dirty = true;
        }

        if (offsety != 0f)
        {
            y -= offsety;
            dirty = true;
        }
    }
    bool isEnlarge(Vector2 oP1, Vector2 oP2, Vector2 nP1, Vector2 nP2)
    {
        //函数传入上一次触摸两点的位置与本次触摸两点的位置计算出用户的手势
        var leng1 = Mathf.Sqrt((oP1.x - oP2.x) * (oP1.x - oP2.x) + (oP1.y - oP2.y) * (oP1.y - oP2.y));
        var leng2 = Mathf.Sqrt((nP1.x - nP2.x) * (nP1.x - nP2.x) + (nP1.y - nP2.y) * (nP1.y - nP2.y));
        return leng2 > leng1;
    }
    private void calcDist(bool zoomIn)
    {
        float newValue = Distance;
        if (zoomIn)
        {
            if (newValue > MinDist)
            {
                newValue -= ZoomSpeed;
            }
        }
        else
        {
            if (newValue < MaxDist)
            {
                newValue += ZoomSpeed;
            }
        }
        newValue = Mathf.Clamp(newValue, MinDist, MaxDist);
        if (newValue != Distance)
        {
            Distance = newValue;
            dirty = true;
        }
    }
    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}