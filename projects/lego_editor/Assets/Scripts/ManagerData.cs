using UnityEngine;
using System.Collections.Generic;
public class ManagerData
{
    private static ManagerData m_Instance;
    public static ManagerData Instance
    {
        get
        {
            if (m_Instance == null)
                m_Instance = new ManagerData();
            return m_Instance;
        }
    }

    private static Constance m_Constance;
    public static Constance Constance
    {
        get
        {
            if (m_Constance == null)
            {
                AssetBundle bundle = AssetBundle.LoadFromFile(StringConfig.PathOutputBundle + "/constance");
                m_Constance = bundle.LoadAsset("constance") as Constance;
                bundle.Unload(false);
                if (m_Constance == null)
                {
                    Debug.LogWarning("静态数据是空数据！=> ManagerData.Constance");
                }
            }
            return m_Constance;
        }
    }

    public List<PartData> PartDataList
    {
        get
        {
            return Constance.PartDataList;
        }
    }
    public List<ActionData> ActionDataList
    {
        get
        {
            return Constance.ActionDataList;
        }
    }

    public List<DeviceData> DeviceDataList
    {
        get
        {
            return Constance.DeviceDataList;
        }
    }

    public int GetActionIdByClipName(string clipName)
    {
        var r = ActionDataList.Find(a => a.clipName.Equals(clipName));
        return r ? r.id : 0;
    }
}