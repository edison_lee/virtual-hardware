﻿using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.Animations;
using System.Collections.Generic;

public class LegoModelImportSettings : AssetPostprocessor
{
    private const string ANIMATION_FLAG = "@";
    public void OnPreprocessModel()
    {
        ModelImporter modelImporter = assetImporter as ModelImporter;
        string fileName, extension;
        fileName = extension = string.Empty;

        if (!TryGetPathInfo(assetPath, ref fileName, ref extension)) return;
        bool isAnimation = fileName.Contains(ANIMATION_FLAG);

        if (isAnimation)
        {
            //模型导入设置
            modelImporter.importAnimation = true;

            //提取clip，预处理clip，复制到其他目录
            string clipSavePath = Path.GetDirectoryName(assetPath).Replace("Original", "Animation") + "/";
            AnimationClip clip = AssetDatabase.LoadAssetAtPath(assetPath, typeof(AnimationClip)) as AnimationClip;
            if (clip == null)
            {
                Debug.Log("the clip at path :'" + assetPath + "'is null.");
                return;
            }
            AnimationClip newClip = new AnimationClip();
            EditorUtility.CopySerialized(clip, newClip);
            string[] vals = fileName.Split('@');

            string animName = vals[1];
            if (!animName.Contains("_Original"))
            {
                AnimationEvent clipEndEvent = new AnimationEvent();
                clipEndEvent.functionName = "OnClipEnd";
                clipEndEvent.time = newClip.length;
                clipEndEvent.stringParameter = vals[1];
                AnimationEvent[] events = new AnimationEvent[] { clipEndEvent };
                AnimationUtility.SetAnimationEvents(newClip, events);
            }

            Utility.CheckAndCreateDirectory(clipSavePath, false);
            AssetDatabase.CreateAsset(newClip, clipSavePath + clip.name + ".anim");
        }
        else
        {
            //模型导入设置
            modelImporter.importAnimation = false;

            bool isCollision = fileName.Contains("Part_");
            if (isCollision)
            {
                modelImporter.isReadable = false;
                modelImporter.importCameras = false;
                modelImporter.importLights = false;
                modelImporter.importMaterials = false;
            }
            else
            {

            }
        }
    }
    
    private static bool TryGetPathInfo(string fullPath, ref string fileName, ref string extension)
    {
        if (string.IsNullOrEmpty(fullPath))
            return false;
        fileName = Path.GetFileNameWithoutExtension(fullPath);
        extension = Path.GetExtension(fullPath).ToLower();
        return true;
    }
}
