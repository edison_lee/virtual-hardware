using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class LegoMenus
{
    [MenuItem("Lego/模型预处理", false, 1)]
    public static void ModelPreprocess()
    {
        LegoModelPreprocess.Execute();
    }
    [MenuItem("Lego/模型打包", false, 2)]
    public static void ModelBuild()
    {
        LegoModelBuild.Execute();
    }
    [MenuItem("Lego/一键更新模型", false, 3)]
    public static void ModelUpdate()
    {
        LegoModelPreprocess.Execute();
        LegoModelBuild.Execute();
    }
}