﻿using System.IO;
using System;
using UnityEngine;
using System.Collections.Generic;
public static class Utility
{
    public static void CheckAndCreateDirectory(string path, bool deleteRecursive = false)
    {
        if (Directory.Exists(path))
        {
            if (deleteRecursive)
            {
                IterateDirectoriesRecursive(path, str =>
                {
                    string[] files = Directory.GetFiles(str);
                    foreach (string f in files)
                    {
                        File.Delete(f);
                    }
                });
                Directory.Delete(path, true);
                Directory.CreateDirectory(path);
            }
        }
        else
            Directory.CreateDirectory(path);
    }

    public static void IterateDirectoriesRecursive(string path, Action<string> unityProcessor)
    {
        unityProcessor(path);
        string[] paths = Directory.GetDirectories(path);
        foreach (string p in paths)
        {
            IterateDirectoriesRecursive(p, unityProcessor);
        }
    }

    public static Transform FindChildRecursive(Transform trans, string targetName)
    {
        List<Transform> childrens = new List<Transform>();
        trans.GetComponentsInChildren<Transform>(childrens);
        return childrens.Find(c => c.name == targetName);
        // if (trans && trans.name == targetName)
        // {
        //     return trans;
        // }
        // else
        // {
        //     Transform child = null;
        //     foreach (Transform t in trans)
        //     {
        //         child = FindChildRecursive(t, targetName);
        //         if (child) break;
        //     }
        //     return child;
        // }
    }
}


