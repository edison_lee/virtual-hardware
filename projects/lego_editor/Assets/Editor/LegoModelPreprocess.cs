using UnityEngine;
using UnityEditor;
using System.IO;
using UnityEditor.Animations;
using System.Collections.Generic;
public static class LegoModelPreprocess
{
    public static void Execute()
    {
        foreach (DeviceData dd in ManagerData.Constance.DeviceDataList)
        {
            dealWithOneDevice(dd);
        }
        AssetDatabase.Refresh();
    }
    private static void dealWithOneDevice(DeviceData deviceData)
    {
        string deviceName = deviceData.model;
        string controllerPath = "Assets/Standard Assets/FBXs/Devices/" + deviceName + "/" + deviceName + ".controller";
        AnimatorController ctrl = CreateDefaultAnimController(controllerPath);


        string animPath = Application.dataPath + "/Standard Assets/FBXs/Devices/" + deviceName + "/Animation/";
        string[] fs = Directory.GetFiles(animPath, "*.anim");
        foreach (string f in fs)
        {
            string nf = f.Replace(Application.dataPath, "Assets");
            AnimationClip ac = AssetDatabase.LoadAssetAtPath<AnimationClip>(nf);
            if (ac)
            {
                ctrl.AddMotion(ac);
            }
        }

        //------------------------加载------------------------
        string modelPath = "Assets/Standard Assets/FBXs/Devices/" + deviceName + "/Original/" + deviceName + ".fbx";
        GameObject fbx = AssetDatabase.LoadAssetAtPath<GameObject>(modelPath);
        GameObject go = GameObject.Instantiate(fbx);

        //------------------------生成动画控制器.controller文件------------------------
        Animator anim = go.GetComponent<Animator>();
        if (anim == null)
            anim = go.AddComponent<Animator>();
        anim.runtimeAnimatorController = ctrl;

        //------------------------资源检查和预处理------------------------
        List<PartData> parts = ManagerData.Instance.PartDataList.FindAll(p => p.deviceId == deviceData.id);
        foreach (PartData pd in parts)
        {
            Transform child = Utility.FindChildRecursive(go.transform, "Part_" + pd.indexName);
            if (child == null)
            {
                Debug.LogWarning("the child named:" + pd.indexName + " cant be found in model " + go.name);
                continue;
            }

            Mesh mesh = null;

            MeshFilter mf = child.GetComponentInChildren<MeshFilter>();
            if (mf)
            {
                mesh = mf.sharedMesh;
            }
            if (mesh == null)
            {
                SkinnedMeshRenderer smr = child.GetComponentInChildren<SkinnedMeshRenderer>();
                if (smr)
                {
                    mesh = smr.sharedMesh;
                }
            }
            MeshCollider mc = child.GetComponent<MeshCollider>();
            if (!mc)
            {
                mc = child.gameObject.AddComponent<MeshCollider>();
            }

            if (!mc.sharedMesh)
            {
                mc.sharedMesh = mesh;
            }
            LegoPartBehaviour m_PartBehaviour = child.gameObject.AddComponent<LegoPartBehaviour>();
            m_PartBehaviour.PartId = pd.id;

            // 由于美术给的碰撞体网格挂上后位置不对，所以暂时使用原始网格，
            // Mesh m = AssetDatabase.LoadAssetAtPath<Mesh>(originalPath + "Part_" + pd.indexName + ".fbx");
            // mc.sharedMesh = m;
        }

        //------------------------保存prefab------------------------
        string prefabPath = "Assets/Prefabs/Devices/" + deviceName + ".prefab";
        PrefabUtility.CreatePrefab(prefabPath, go);
        GameObject.DestroyImmediate(go);
        AssetImporter.GetAtPath(prefabPath).assetBundleName = deviceName;
    }
    private static AnimationClip DefaultClip;
    private static AnimationClip DefaultAnimationClip
    {
        get
        {
            if (!DefaultClip)
            {
                DefaultClip = AssetDatabase.LoadAssetAtPath<AnimationClip>("Assets/Standard Assets/Default.anim");
            }
            return DefaultClip;
        }
    }
    private static AnimatorController CreateDefaultAnimController(string fullPath)
    {
        return AnimatorController.CreateAnimatorControllerAtPathWithClip(fullPath, DefaultAnimationClip);
    }
}