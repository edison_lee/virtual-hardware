﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    private const string DefaultName = "Take 001";

    [SerializeField]
    private float m_AnimSpeed;
    [SerializeField]
    private string[] m_ModelNames = { };
    [SerializeField]
    private LegoCam m_Camera;
    private GameObject m_CurrentModel;

    public void OnClickPart(int index)
    {
        string modelName = m_ModelNames[index - 1];
        GameObject prefab = Resources.Load<GameObject>(modelName);
        m_CurrentModel = GameObject.Instantiate(prefab, Vector3.zero, Quaternion.identity);

        Transform trans;
        if (modelName.Equals("CPU"))
        {
            trans = m_CurrentModel.transform.Find("对象022");
        }
        else
        {
            trans = m_CurrentModel.transform;
        }

        m_Camera.Target = trans;
        Animation anim = m_CurrentModel.GetComponent<Animation>();
        anim[DefaultName].speed = m_AnimSpeed;
        anim.wrapMode = WrapMode.Loop;
        anim.Play();
    }
    public void OnClickExit()
    {
        Application.Quit();
    }
    public void OnClickBack()
    {
        clear();
    }
    public void OnClickPlay()
    {
        setAnimSpeed(m_AnimSpeed);
    }
    public void OnClickPause()
    {
        setAnimSpeed(0);
    }

    //------------------------------------

    private void clear()
    {
        if (m_CurrentModel)
        {
            GameObject.DestroyImmediate(m_CurrentModel);
        }
    }
    private void setAnimSpeed(float speed)
    {
        if (m_CurrentModel)
        {
            Animation anim = m_CurrentModel.GetComponent<Animation>();
            anim[DefaultName].speed = speed;
        }
    }
}
