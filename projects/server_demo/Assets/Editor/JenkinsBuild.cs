﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class JenkinsAdapter
{
    [MenuItem("Jenkins/JenkinsBuild")]
    public static void Build()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

        PlayerSettings.keystorePass = "123456";
        PlayerSettings.keyaliasPass = "123456";
        PlayerSettings.Android.bundleVersionCode = 6;

        List<string> sceneList = new List<string>();
        EditorBuildSettingsScene[] temp = EditorBuildSettings.scenes;
        for (int i = 0, iMax = temp.Length; i < iMax; ++i)
            sceneList.Add(temp[i].path);
        string sdkPath = string.Empty;
        string path = string.Empty;
#if UNITY_EDITOR_OSX
        path = "/Users/lizhixiong/Lego/apk/";
        sdkPath = "/Users/lizhixiong/Unity/Project-Unity/sdk";
#elif UNITY_EDITOR_WIN
        path = "C:/Users/leon/Desktop/lego_output/apk/";
        sdkPath = "D:/Android/sdk_r25.2.3";
#endif
        string timeSufix = string.Format("{0:yyyyMMdd_HHmm}", DateTime.Now);
        EditorPrefs.SetString("AndroidSdkRoot", sdkPath);
        BuildPipeline.BuildPlayer(sceneList.ToArray(), path + "com.dce.Lego_ServerDemo_" + timeSufix + ".apk", BuildTarget.Android, BuildOptions.None);
    }
}

