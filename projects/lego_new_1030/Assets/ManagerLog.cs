﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
public class ManagerLog : MonoBehaviour
{
    public static ManagerLog instance;
    Text txt;
    StringBuilder sb = new StringBuilder();
    void Awake()
    {
        instance = this;
        txt = GetComponent<Text>();
    }
    public void AppendLog(string log)
    {
        txt.text += log + "\n";
    }
    public void Trigger()
    {
        GameObject go = txt.transform.parent.gameObject;
        go.SetActive(!go.activeSelf);
    }
    void OnDestroy()
    {
        instance = null;
    }
}
