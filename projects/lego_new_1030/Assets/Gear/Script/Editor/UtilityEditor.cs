using UnityEngine;
using UnityEditor;
using System.IO;
public static class UtilityEditor
{
    public static void ClearOrCreateDirectory(string directory)
    {
        if (Directory.Exists(directory))
            AssetDatabase.DeleteAsset(directory);
        else
            Directory.CreateDirectory(directory);
    }
}