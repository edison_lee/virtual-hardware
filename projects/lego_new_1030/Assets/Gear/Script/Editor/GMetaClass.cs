﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class GMetaClass
{

    public string SavePath;
    public string ClassName;
    public string BaseClassName;
    public List<string> References = new List<string>();
    public List<GMetaProperty> Properties = new List<GMetaProperty>();

    private static string TAB = "\t";
    private static string EOF = "\r\n";

    public void AddRef(string refer)
    {
        References.Add(refer);
    }
    public void AddProp(GMetaProperty pro)
    {
        Properties.Add(pro);
    }
    public void Generate()
    {
        StringBuilder builder = new StringBuilder();

        foreach (string oneUse in References)
        {
            builder.Append("using " + oneUse + ";").Append(EOF);
        }

        string line = string.Empty;
        if (string.IsNullOrEmpty(BaseClassName))
        {
            line = "public class " + ClassName;
        }
        else
        {
            line = "public class " + ClassName + " : " + BaseClassName + "";
        }

        builder.Append(line).Append(EOF);
        builder.Append("{").Append(EOF);

        foreach (GMetaProperty pro in Properties)
        {
            builder.Append(TAB);
            builder.Append("public " + pro.type + " " + pro.name + ";");
            builder.Append(EOF);
        }
        builder.Append("}");
        Write(builder);
    }

    private void Write(StringBuilder builder)
    {
        if (!Directory.Exists(SavePath))
        {
            Directory.CreateDirectory(SavePath);
        }
        // Debug.Log(SavePath);
        // Debug.Log("Code : \n" + builder.ToString());
        System.IO.File.WriteAllText(SavePath + ClassName + ".cs", builder.ToString(), System.Text.UnicodeEncoding.UTF8);
    }
}
