﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using Excel;
using System.Data;
using System;
using System.Reflection;

public class ExcelSerilize : MonoBehaviour
{
    [MenuItem("Gear/ExcelToClass")]
    static void ToClass()
    {
        string dataBasePath = Configs.DataClassPath1 + "Base";
        UtilityEditor.ClearOrCreateDirectory(dataBasePath);

        GMetaClass dataMapClass = new GMetaClass();
        dataMapClass.ClassName = "DataMap";
        dataMapClass.BaseClassName = "ScriptableObject";
        dataMapClass.SavePath = Configs.DataClassPath1;
        dataMapClass.AddRef("System.Collections.Generic");
        dataMapClass.AddRef("UnityEngine");

        string[] files = Directory.GetFiles(Configs.ExcelPath, "*.xlsx", SearchOption.TopDirectoryOnly);
        foreach (string file in files)
        {
            try
            {
                FileInfo info = new FileInfo(file);
                FileStream stream = info.Open(FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                DataSet result = excelReader.AsDataSet();

                string clsName = result.Tables[1].Rows[2][0].ToString();
                int rowCount = result.Tables[0].Rows.Count;
                int colCount = result.Tables[0].Columns.Count;

                GMetaClass clsMeta = new GMetaClass();
                clsMeta.SavePath = Configs.DataClassPath1 + "/Base/";
                clsMeta.BaseClassName = "ScriptableObject";
                clsMeta.AddRef("UnityEngine");
                clsMeta.ClassName = clsName + "Base";
                for (int col = 0; col < colCount; col++)
                {
                    GMetaProperty prop = new GMetaProperty();
                    prop.name = result.Tables[1].Rows[0][col].ToString();
                    prop.type = result.Tables[1].Rows[1][col].ToString();
                    clsMeta.AddProp(prop);
                }
                clsMeta.Generate();

                if (!File.Exists(Configs.DataClassPath1 + clsName + ".cs"))
                {
                    GMetaClass clsBaseMeta = new GMetaClass();
                    clsBaseMeta.SavePath = Configs.DataClassPath1;
                    clsBaseMeta.BaseClassName = clsMeta.ClassName;
                    clsBaseMeta.AddRef("UnityEngine");
                    clsBaseMeta.ClassName = clsName;
                    clsBaseMeta.Generate();
                }

                GMetaProperty mapProperty = new GMetaProperty();
                mapProperty.name = clsName + "List = new List<" + clsName + ">()";
                mapProperty.type = "List<" + clsName + ">";
                dataMapClass.AddProp(mapProperty);

                excelReader.Close();
                stream.Close();
            }
            catch (Exception e)
            {
                Debug.LogError("异常：" + file);
            }
        }
        dataMapClass.Generate();
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }

    [MenuItem("Gear/ExcelToSerData")]
    static void ToSerData()
    {
        UtilityEditor.ClearOrCreateDirectory(Configs.DataBytePath1);

        DataMap map = new DataMap();
        Type mapType = map.GetType();
        string[] files = Directory.GetFiles(Configs.ExcelPath, "*.xlsx", SearchOption.TopDirectoryOnly);
        foreach (string file in files)
        {
            try
            {
                FileInfo info = new FileInfo(file);
                FileStream stream = info.Open(FileMode.Open, FileAccess.Read);
                IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                DataSet result = excelReader.AsDataSet();

                string clsName = result.Tables[1].Rows[2][0].ToString();
                int rowCount = result.Tables[0].Rows.Count;
                int colCount = result.Tables[0].Columns.Count;

                string mapFieldName = clsName + "List";
                FieldInfo finfo = mapType.GetField(mapFieldName);
                object val = finfo.GetValue(map);
                IList list = val as IList;
                //IList list = mapType.GetField(mapFieldName).GetValue(map) as IList;

                for (int row = 1; row < rowCount; row++)
                {
                    //Type tt = DataManager.Instance.GetType(clsName);
                    ScriptableObject asset = ScriptableObject.CreateInstance(clsName);
                    Type tt = asset.GetType();
                    string id = result.Tables[0].Rows[row][0].ToString();

                    if (string.IsNullOrEmpty(id)) continue;
                    int rid;
                    if (!int.TryParse(id, out rid)) continue;
                    if (rid <= 0) continue;

                    for (int col = 0; col < colCount; col++)
                    {
                        string name = result.Tables[1].Rows[0][col].ToString();
                        string value = result.Tables[0].Rows[row][col].ToString();
                        string type = result.Tables[1].Rows[1][col].ToString();
                        if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(value))
                        {
                            FieldInfo ifo = tt.GetField(name);
                            object cvalue = System.ComponentModel.TypeDescriptor.GetConverter(ifo.FieldType).ConvertFrom(value);
                            ifo.SetValue(asset, cvalue);
                        }
                    }
                    string savePath = Configs.DataBytePath1 + clsName + "/";
                    if (!Directory.Exists(savePath))
                    {
                        Directory.CreateDirectory(savePath);
                    }
                    if (asset != null)
                    {
                        list.Add(asset);
                        AssetDatabase.CreateAsset(asset, savePath + clsName + "_" + id + ".asset");
                    }
                }
                excelReader.Close();
                stream.Close();
            }
            catch (Exception e)
            {
                Debug.LogError("异常：" + file);
            }
        }
        AssetDatabase.CreateAsset(map, "Assets/Resources/Constance.asset");
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
}

