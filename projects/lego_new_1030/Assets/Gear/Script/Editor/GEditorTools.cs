using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
public static class GEditorTools
{
    [MenuItem("Gear/清空本地缓存")]
    static void ClearAllLocalData()
    {
        PlayerPrefs.DeleteAll();
    }
}
