using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;
using UnityEditor;
using System.Text;
using System.IO;
using System;
using System.Collections.Generic;
public class GUICodeGenerator
{
    static string CodeFrame =
@"    protected override void Awake()
    {
        base.Awake();
        //TODO 初始化
    }
    public override void SetData(object data)
    {
        base.SetData(data);
        //TODO 初始化数据
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        //TODO 点击事件
    }
";
    public static Dictionary<Type, int> Priorities = new Dictionary<Type, int>();

    public GUICodeGenerator()
    {
        Priorities[typeof(RawImage)] = 1001;
        Priorities[typeof(Image)] = 1001;
        Priorities[typeof(Text)] = 1001;

        Priorities[typeof(Button)] = 1002;
        Priorities[typeof(GButton)] = 1002;
        Priorities[typeof(Scrollbar)] = 1002;
        Priorities[typeof(Slider)] = 1002;
        Priorities[typeof(Toggle)] = 1002;
        Priorities[typeof(InputField)] = 1002;

        Priorities[typeof(GListBase)] = 1003;
    }
    private static string getPropertyPath(GameObject property, GameObject root)
    {
        Stack<string> stack = new Stack<string>();
        while (property && property != root)
        {
            stack.Push(property.name);
            property = property.transform.parent.gameObject;
        }
        return string.Join("/", stack.ToArray());
    }
    private static Type getPriorityType(GameObject go)
    {
        Type type = null;
        UIBehaviour ui = go.GetComponent<GBase>();
        if (ui)
        {
            type = ui.GetType();
        }
        else
        {
            ui = go.GetComponent<Selectable>();
            if (ui)
            {
                type = ui.GetType();
            }
            else
            {
                ui = go.GetComponent<Graphic>();
                if (ui)
                {
                    type = ui.GetType();
                }
                else
                {
                    type = typeof(GameObject);
                }
            }
        }
        return type;
    }
    private static int getPriority(Type t)
    {
        if (t != null)
        {
            foreach (KeyValuePair<Type, int> pair in Priorities)
            {
                if (t == pair.Key || t.IsSubclassOf(pair.Key))
                    return pair.Value;
            }
        }
        return 0;
    }
    private static bool isUIClassOrSubClass(Type t)
    {
        foreach (KeyValuePair<Type, int> pair in Priorities)
        {
            if (t == pair.Key || t.IsSubclassOf(pair.Key))
                return true;
        }
        return false;
    }
    public static void GeneratePanel(GameObject root, Transform[] properties)
    {
        string pathCode = Application.dataPath + Configs.UIGeneratePathCode;
        string pathPrefab = "Assets" + Configs.UIGeneratePathPrefab;
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder propsBuilder = new StringBuilder();
        StringBuilder propsFindBuilder = new StringBuilder();
        for (int i = 0; i < properties.Length; i++)
        {
            Transform trans = properties[i];
            Type propType = getPriorityType(trans.gameObject);
            string propName = trans.name;
            string propPath = getPropertyPath(trans.gameObject, root);
            if (propType != null)
            {
                propsBuilder.AppendFormat("    public {0} {1};\n", propType.Name, propName);
                if (propType == typeof(GameObject))
                {
                    propsFindBuilder.AppendFormat("        {0} = Trans.Find(\"{1}\").gameObject;\n", propName, getPropertyPath(trans.gameObject, root), propType.Name);
                }
                else if (propType == typeof(GButton) || propType.IsSubclassOf(typeof(GButton)))
                {
                    propsFindBuilder.AppendFormat("        {0} = Trans.Find(\"{1}\").GetComponent<{2}>();\n", propName, getPropertyPath(trans.gameObject, root), propType.Name);
                    propsFindBuilder.AppendFormat("        {0}.OnClick = OnButtonClickHandler;\n", propName);
                }
                else if (propType == typeof(GUnlimitedList))
                {
                    propsFindBuilder.AppendFormat("        {0} = Trans.Find(\"{1}\").GetComponent<{2}>();\n", propName, getPropertyPath(trans.gameObject, root), propType.Name);
                    // propsFindBuilder.AppendFormat("        {0}.OnClick = OnButtonClickHandler;\n", propName);
                }
                else
                {
                    propsFindBuilder.AppendFormat("        {0} = Trans.Find(\"{1}\").GetComponent<{2}>();\n", propName, getPropertyPath(trans.gameObject, root), propType.Name);
                }
            }
        }
        string props = propsBuilder.ToString();
        string propsFind = propsFindBuilder.ToString();

        stringBuilder.AppendLine("using UnityEngine.UI;");
        stringBuilder.AppendLine("using UnityEngine;");
        stringBuilder.AppendFormat("public class {0}Base : GPanelBase\n", root.name);
        stringBuilder.AppendLine("{");
        stringBuilder.AppendFormat("{0}\n", props);
        stringBuilder.AppendLine("    protected override void Awake()");
        stringBuilder.AppendLine("    {");
        stringBuilder.AppendLine("        base.Awake();");
        stringBuilder.AppendFormat("{0}\n", propsFind);
        stringBuilder.AppendLine("    }");
        stringBuilder.AppendLine("}");
        File.WriteAllText(pathCode + "Base/" + root.name + "Base.cs", stringBuilder.ToString(), UnicodeEncoding.UTF8);

        if (!File.Exists(pathCode + root.name + ".cs"))
        {
            stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("using UnityEngine.UI;");
            stringBuilder.AppendLine("using UnityEngine;");
            stringBuilder.AppendFormat("public class {0} : {0}Base\n", root.name);
            stringBuilder.AppendLine("{");
            stringBuilder.Append(CodeFrame);
            stringBuilder.AppendLine("}");
            File.WriteAllText(pathCode + root.name + ".cs", stringBuilder.ToString(), UnicodeEncoding.UTF8);
        }

        GameObject prefab = PrefabUtility.CreatePrefab(pathPrefab + root.name + ".prefab", root, ReplacePrefabOptions.Default);
        GameObject.DestroyImmediate(root);
        AssetDatabase.Refresh();
        AssetDatabase.SaveAssets();
    }

    public static void GenerateItem(GameObject root, GameObject[] properties)
    {

    }
}