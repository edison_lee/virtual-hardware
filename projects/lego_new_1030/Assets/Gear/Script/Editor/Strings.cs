public static class Strings
{
    public const string MENU = "Gear/";

    public const string MENU_UI = MENU + "UI/";
    public const string MENU_UI_GENERATE = MENU_UI + "Generate";


    public const string MENU_DATA = MENU + "Data/";
    public const string MENU_DATA_GEN_CLASS = MENU_DATA + "GenClass";
    public const string MENU_DATA_GEN_BYTE = MENU_DATA + "GenByte";


    public const string MENU_ASSET = MENU + "Asset/";
    public const string MENU_ASSET_GEN_ALL = MENU_ASSET + "GenAll";
    public const string MENU_ASSET_GEN_ANIMATION = MENU_ASSET + "GenAnimation";
    public const string MENU_ASSET_GEN_DEVICE = MENU_ASSET + "GenDevice";
    public const string MENU_ASSET_GEN_TOOL = MENU_ASSET + "GenTool";

}