﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class ManagerUI
{
    private GameObject root;
    private GameObject scene;
    private List<GPanelBase> list = new List<GPanelBase>();
    private static ManagerUI _instance;
    public static ManagerUI Instance
    {
        get
        {
            if (_instance == null)
                _instance = new ManagerUI();
            return _instance;
        }
    }

    public ManagerUI()
    {
        root = GameObject.Find(Configs.UIRootPathInHierarchy);
        scene = root.transform.Find("UIScene").gameObject;
        _instance = this;
    }

    public void CheckBackScene()
    {
        foreach (GPanelBase p in list)
        {
            if (p.Setting.UseSceneBack && p.IsShowing())
            {
                scene.SetActive(true);
                return;
            }
        }
        scene.SetActive(false);
    }

    public void Open<T>(object data = null) where T : GPanelBase
    {
        T ui = Get<T>();
        if (ui == null)
        {
            Type t = typeof(T);
            string prefabPath = Configs.UIGeneratePathPrefab.Substring(Configs.UIGeneratePathPrefab.IndexOf("Prefab")) + t.Name;
            GameObject prefab = Resources.Load<GameObject>(prefabPath);
            GameObject panel = GameObject.Instantiate(prefab) as GameObject;
            ui = panel.AddComponent<T>();
            ui.Create(this.root.transform);
            list.Add(ui);
        }
        ui.Open(data);
        CheckBackScene();
    }

    public void Close(GPanelBase ui)
    {
        if (!ui.Setting.IsHideMode)
            list.Remove(ui);
        ui.Close();
        CheckBackScene();
    }
    public void Close<T>() where T : GPanelBase
    {
        T oldui = Get<T>();
        if (oldui == null)
        {
            Debug.Log("the panel " + typeof(T).Name + " hasn't be opened!");
            return;
        }
        Close(oldui);
    }
    private T Get<T>() where T : GPanelBase
    {
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].GetType().Equals(typeof(T)))
                return list[i] as T;
        }
        return null;
    }
    public bool IsShow<T>() where T : GPanelBase
    {
        T oldui = Get<T>();
        return oldui != null && oldui.gameObject.activeSelf;
    }
    public void ShowSceneBackForce()
    {
        if (!scene.activeSelf)
        {
            scene.SetActive(true);
        }
    }
    public void HideSceneBackForce()
    {
        if (scene.activeSelf)
        {
            scene.SetActive(false);
        }
    }
    public void ShowTip(string text)
    {
        GRunningTip.Show(text);
    }
}
