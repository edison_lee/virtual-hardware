using UnityEngine;
public class ManagerInput
{
    private static ManagerInput instance;
    public static ManagerInput Instance
    {
        get
        {
            if (instance == null)
                instance = new ManagerInput();
            return instance;
        }
    }

    public void Tick()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        processPcStandAlone();
#else
        processMobileDevices();
#endif
    }

    private void processPcStandAlone()
    {

    }
    private void processMobileDevices()
    {

    }
}