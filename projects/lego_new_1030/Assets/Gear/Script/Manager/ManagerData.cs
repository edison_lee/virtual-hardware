﻿using System.Collections.Generic;
using UnityEngine;

public static class ManagerData
{
    private static DataMap map;
    public static DataMap Map
    {
        get
        {
            if (map == null)
                map = Resources.Load<DataMap>("Constance");
            return map;
        }
    }
    public static T Get<T>(int id) where T : ScriptableObject
    {
        string typeName = typeof(T).Name;
        return Resources.Load<T>("StaticData/" + typeName + "/" + typeName + "_" + id);
    }
    public static List<QuestionData> GetAllQuestionByDeviceId(int id)
    {
        return null;
    }

    public static PartData GetPartDataByModelName(string modelName)
    {
        return Map.PartDataList.Find(p => p.modelName == modelName);
    }

    public static List<PartData> GetRootParts()
    {
        return Map.PartDataList.FindAll(p => p.belongPartId == 0);
    }
    public static DeviceData GetDeviceDataByName(string dvcName)
    {
        return Map.DeviceDataList.Find(d => d.model.Equals(dvcName));
    }
}
