using System.Collections.Generic;
using System;
using UnityEngine.U2D;
using UnityEngine;
public class ManagerResource
{
    private Dictionary<string, SpriteAtlas> m_AtlasMap = new Dictionary<string, SpriteAtlas>();
    private static ManagerResource instance;
    public static ManagerResource Instance
    {
        get
        {
            if (instance == null)
                instance = new ManagerResource();
            return instance;
        }
    }

    public SpriteAtlas GetAtlasByName(string atlasName)
    {
        SpriteAtlas atlas;
        if (!m_AtlasMap.TryGetValue(atlasName, out atlas))
        {
            atlas = Resources.Load<SpriteAtlas>("Atlas/" + atlasName);
            m_AtlasMap[atlasName] = atlas;
        }
        if (atlas == null)
        {
            Debug.Log("the atlas cannot be fround.AtlasName=" + atlasName);
        }
        return atlas;
    }

    //Tool_Hand
    public Sprite GetSpriteByName(string spriteName)
    {
        bool isPart = spriteName.Contains("Part_");
        string atlasName = string.Empty;
        string[] vars = spriteName.Split('_');
        if (isPart)
        {
            atlasName = "Icons_Part_" + vars[0];
        }
        else
        {
            atlasName = "Icons_Tool";
        }

        SpriteAtlas atlas = GetAtlasByName(atlasName);

        if (atlas)
        {
            Sprite s = atlas.GetSprite(spriteName);
            if (s != null)
            {
                return s;
            }
        }
        Debug.Log("the sprite cannot be fround.SpriteName=" + spriteName);
        return null;
    }

    public Sprite GetDeviceSpriteByName(string deviceName)
    {
        SpriteAtlas atlas = GetAtlasByName("Icons_Device");

        if (atlas)
        {
            Sprite s = atlas.GetSprite(deviceName);
            if (s != null)
            {
                return s;
            }
        }
        Debug.Log("the sprite cannot be fround.SpriteName=" + deviceName);
        return null;
    }
}