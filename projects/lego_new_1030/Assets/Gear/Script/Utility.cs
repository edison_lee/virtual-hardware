using UnityEngine;
public static class Utility
{
    public static void SetGameObjectRecursively(GameObject obj, int layer)
    {
        obj.layer = layer;
        foreach (Transform t in obj.transform)
        {
            SetGameObjectRecursively(t.gameObject, layer);
        }
    }
}