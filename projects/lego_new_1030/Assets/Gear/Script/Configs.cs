﻿using UnityEngine;

public static class Configs
{
    public const string TagPanel = "Panel";
    public const string TagProperty = "Property";
    public const string TagListProperty = "ListProperty";
    public const string UIRootPathInHierarchy = "Main/RootCanvas";
    public const string UIGeneratePathCode = "/Script/UI/";
    public const string UIGeneratePathPrefab = "/Resources/Prefab/UI/";
    public const string MenuPathRoot = "Gear/";
    public const string MenuPathUI = MenuPathRoot + "UI/";
    public const string MenuPathUIGenerate = MenuPathUI + "Generate";
#if UNITY_EDITOR_OSX
    public const string ExcelPath = "/Users/lizhixiong/Lego/svn/LEGO-Table";
#elif UNITY_EDITOR_WIN
    public const string ExcelPath = "E:/Projects/Work/project-svn/lego_doc/LEGO-Table";
    // public const string ExcelPath = "C:/Users/leon/Desktop/Merged";
#endif
    public static string DataClassPath1 = Application.dataPath + "/Script/Data/Gen/";
    public static string DataBytePath1 = "Assets/Resources/StaticData/";
}
