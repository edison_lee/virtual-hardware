﻿using UnityEngine.EventSystems;

public class GPanelSetting : UIBehaviour
{
    public const int LAYER_SCALE = 5;
    public int Layer;
    public bool UseSceneBack;
    public bool IsHideMode;
    public int GetRealLayer()
    {
        return Layer * LAYER_SCALE;
    }
}
