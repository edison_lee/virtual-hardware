using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;
public class GRunningTipModel
{
    static Vector2 DEFAULT_POS = Vector2.up * 150;
    public static Font _DefaultFont;
    public bool _UseDefaultPos = true;
    public Vector3 _Pos;
    public string _Text;
    public string _Color;
    public bool _MultyLine;
    public float _Offset;
    public float _Delay;
    public float _Duration;
    public Font _Font;
    public int _FontSize;
    public Ease _EaseType = Ease.InQuint;
    private static Transform _TipRoot;
    public static Transform TipRoot
    {
        get
        {
            if (_TipRoot == null)
                _TipRoot = GameObject.Find(Configs.UIRootPathInHierarchy).transform;
            return _TipRoot;
        }
    }

    public Transform GetParent()
    {
        return _TipRoot;
    }
    public float GetEndValue(float startValue)
    {
        return startValue + _Offset;
    }
    //位置暂时不支持世界中的物体到UI中位置的转换
    public Vector3 GetStartPos()
    {
        if (_UseDefaultPos)
        {
            return DEFAULT_POS;
        }
        else
        {
            return TipRoot.worldToLocalMatrix.MultiplyPoint(_Pos);
            // CanvasScaler canvasScaler = GameObject.Find("UIRootCanvas").gameObject.GetComponent<CanvasScaler>();
            // float offect = (Screen.width / canvasScaler.referenceResolution.x) * (1 - canvasScaler.matchWidthOrHeight) + (Screen.height / canvasScaler.referenceResolution.y) * canvasScaler.matchWidthOrHeight;
            // Vector2 a = RectTransformUtility.WorldToScreenPoint(Camera.main, _Pos);
            // return new Vector3(a.x / offect, a.y / offect,0f);
        }
    }
    public Color GetColor()
    {
        Color rst;
        ColorUtility.TryParseHtmlString(this._Color, out rst);
        return rst;
    }
    public string GetHtmlText()
    {
        return "<color=" + _Color + ">" + this._Text + "</color>";
    }
    public bool IsValid()
    {
        return !string.IsNullOrEmpty(_Text);// && delay>=0f && delay < 60f && duration >= 0f && duration < 60f;
    }
}