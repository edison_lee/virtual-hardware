﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class GButtonColorElastic : GButtonColor
{
    [SerializeField]
    private float _ScaleButtonDown = .7f;
    [SerializeField]
    private float _ScaleButtonUp = 1f;
    [SerializeField]
    private float _ScaleButtonSelect = 1.05f;
    [SerializeField]
    private float _ScaleButtonUnselect = 1f;
    [SerializeField]
    private float _Time = .3f;

    public override void ButtonDownEffect()
    {
        DOTween.Kill(gameObject);
        base.ButtonDownEffect();
        RectTrans.DOScale(Vector3.one * _ScaleButtonDown, _Time).SetUpdate(true);
    }

    protected override void OnDisable()
    {
        DOTween.Kill(gameObject);
        transform.localScale = Vector3.one;
    }

    public override void ButtonUpEffect()
    {
        DOTween.Kill(gameObject);
        base.ButtonUpEffect();
        RectTrans.DOScale(Vector3.one * _ScaleButtonUp, _Time).SetUpdate(true);
    }

    public override void ButtonSelectEffect()
    {
        if (DOTween.IsTweening(gameObject))
        {
            return;
        }
        //DOTween.Kill(gameObject);
        base.ButtonSelectEffect();
        RectTrans.DOScale(Vector3.one * _ScaleButtonSelect, .1f).SetUpdate(true);
    }
    public override void ButtonDeselectEffect()
    {
        if (DOTween.IsTweening(gameObject))
        {
            return;
        }
        //DOTween.Kill(gameObject);
        base.ButtonDeselectEffect();
        RectTrans.DOScale(Vector3.one, .1f).SetUpdate(true);
    }
}
