﻿using UnityEngine;
using System.Collections;
using System;
public class SyncOpera : Opera
{


    public SyncOpera(Action<object> completeHandler = null)
        : base(completeHandler)
    { }
    public override void Execute()
    {
        CoroutineGo.StartCoroutine(SyncExecute());
    }

    public override void End()
    {
        base.End();
    }

    protected virtual IEnumerator SyncExecute()
    {
        yield return null;
    }

}

