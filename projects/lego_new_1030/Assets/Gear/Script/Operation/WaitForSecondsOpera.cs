﻿using UnityEngine;
using System.Collections;
using System;

public class WaitForSecondsOpera : SyncOpera
{

    private float time;
    public WaitForSecondsOpera(float time, Action<object> completeHandler = null)
        : base(completeHandler)
    {
        this.time = time;
    }

    protected override IEnumerator SyncExecute()
    {
        yield return new WaitForSeconds(time);
        result = time;
        End();
    }
}
