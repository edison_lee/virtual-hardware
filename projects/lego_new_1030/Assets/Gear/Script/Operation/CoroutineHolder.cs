using UnityEngine;
using System.Collections.Generic;
using System;
public class CoroutineHolder : MonoBehaviour
{
    private List<Action<float>> m_Tickers = new List<Action<float>>();
    public void AddTicker(Action<float> ticker)
    {
        if (m_Tickers.Contains(ticker))
        {
            Debug.Log("已经添加了一个相同的计时器。");
            return;
        }
        m_Tickers.Add(ticker);
    }
    public void RemoveTicker(Action<float> ticker)
    {
        if (!m_Tickers.Contains(ticker))
        {
            Debug.Log("要移除的计时器不存在。");
            return;
        }
        m_Tickers.Remove(ticker);
    }
    public void Clear()
    {
        m_Tickers.Clear();
    }
    void Update()
    {
        if (m_Tickers != null && m_Tickers.Count > 0)
        {
            for (int i = 0; i < m_Tickers.Count; i++)
            {
                var tick = m_Tickers[i];
                tick(Time.deltaTime);
            }
        }
    }
}
