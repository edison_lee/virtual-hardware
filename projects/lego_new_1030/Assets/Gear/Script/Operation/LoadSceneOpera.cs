﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
public class LoadSceneOpera : SyncOpera
{
    private string sceneName;
    private Action<float> onProgress;
    public LoadSceneOpera(string name, Action<float> onProgress = null, Action<object> completeHandler = null)
        : base(completeHandler)
    {
        this.sceneName = name;
        this.onProgress = onProgress;
    }
    protected override IEnumerator SyncExecute()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(sceneName);
        while (!async.isDone)
        {
            if (onProgress != null)
                onProgress(async.progress);
            yield return async;
        }
        End();
    }

}
