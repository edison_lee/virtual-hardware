﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class Main : MonoBehaviour
{
    public const string LOCAL_ACCOUNT = "local_account";
    public const string LOCAL_PASSWORD = "local_password";
    public const string LOCAL_HASLOGIN = "local_haslogin";
    public GameObject logBtnGo;
    public static Main Instance;
    [NonSerialized]
    public bool IsOpendByPlatform = false;

    [NonSerialized]
    public bool HasEverLogin = true;
    private Dictionary<GuidPage, bool> guidRecored = new Dictionary<GuidPage, bool>();
    public bool IsPanelOpened(GuidPage p)
    {
        bool rst;
        guidRecored.TryGetValue(p, out rst);
        guidRecored[p] = true;
        return rst;
    }
    void Awake()
    {
        Instance = this;
        GameObject.DontDestroyOnLoad(gameObject);
        bool.TryParse(PlayerPrefs.GetString(LOCAL_HASLOGIN), out HasEverLogin);
#if UNITY_EDITOR || UNITY_STANDALONE
        OnPlatform("_");
#else
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        jo.Call("onFirstUnityStart");
#endif
    }


    private bool IsPlatformInservice(string platform)
    {
        return platform.Equals("gikoo");
    }
    public void OnPlatform(string str)
    {
        string[] vars = str.Split('_');
        string uid = vars[0];
        string platform = vars[1];

        //被平台唤起
        // if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(platform) && !uid.Equals("null") && !platform.Equals("null") && IsPlatformInservice(platform))
        if (!string.IsNullOrEmpty(uid) && !string.IsNullOrEmpty(platform) && !uid.Equals("null"))
        {
            new RpcUserLogin(uid, "", result =>
            {
                PlayerPrefs.SetString(LOCAL_ACCOUNT, uid);
                PlayerPrefs.SetString(LOCAL_HASLOGIN, "true");
                IsOpendByPlatform = true;
                Home();
            })
            .OnError(msg =>
            {
                AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
                jo.Call("showTip", msg);
                Application.Quit();
            }).Execute();
        }
        //正常登陆
        else
        {
            List<DataRanking> list = new List<DataRanking>();
            Action<string, string> t = onLogin;
            ManagerUI.Instance.Open<PanelLogin>(t);
        }
    }
    private void onLogin(string account, string password)
    {
        new RpcUserLogin(account, password, result =>
        {
            PlayerPrefs.SetString(LOCAL_ACCOUNT, account);
            PlayerPrefs.SetString(LOCAL_PASSWORD, account);
            PlayerPrefs.SetString(LOCAL_HASLOGIN, "true");
            ManagerUI.Instance.Close<PanelLogin>();
            Home();
        }).Execute();
    }

    public void Home()
    {
        new RpcUserInfo((r1) =>
        {
            ManagerUI.Instance.Open<PanelMain>();
        }).Execute();
    }

    //挑战结束
    //model 机型moto_g5s, mode 模式：0拆 1装， useTime 用时（秒）
    public void OnExamEnd(string model, int mode, int useTime)
    {
        PanelRankInfoModel m = new PanelRankInfoModel();
        m.mode = mode;
        m.useTime = useTime;
        new RpcExamEnd(useTime, o1 =>
        {
            ManagerUI.Instance.Open<PanelRankInfo>(m);
        }).Execute();
    }
    void OnDestroy()
    {
        Instance = null;
    }
}