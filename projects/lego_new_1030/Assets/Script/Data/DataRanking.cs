using System;
[System.Serializable]
public class DataRanking : Data
{
    public int ranking;
    public int userNo;
    public string userName;
    public string country;
    public string area;
    public string model;
    public string modelName;
    public int useTime;
}