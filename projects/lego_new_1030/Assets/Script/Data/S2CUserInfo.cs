public class S2CUserInfo : Data
{
    public string userName;
    public string headId;
    public int sex;
    public string country;
    public S2CUserInfoExamItem[] exams;

    public int loginCount;
    public int ranking;
}