﻿using UnityEngine;
public class QuestionDataBase : ScriptableObject
{
	public int id;
	public int deviceId;
	public string description;
	public int type;
	public string options;
	public bool isRandomDistribution;
	public string answer;
}