﻿using UnityEngine;
public class PartDataBase : ScriptableObject
{
	public int id;
	public string partName;
	public string hangName;
	public string modelName;
	public string imageName;
	public int deviceId;
	public bool recovery;
	public int belongPartId;
	public string pn;
	public int focusIndex;
	public bool active;
}