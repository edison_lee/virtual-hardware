﻿using UnityEngine;
public class Base : ScriptableObject
{
	public int playOrder;
	public int id;
	public string instruction;
	public string listName;
	public bool isKeyClip;
	public bool isAssembly;
	public int partId;
	public int toolId;
	public string clickObjectName;
	public int deviceId;
	public int heatTime;
	public int heatTemperature;
}