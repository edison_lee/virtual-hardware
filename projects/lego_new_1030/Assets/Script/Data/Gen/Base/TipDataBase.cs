﻿using UnityEngine;
public class TipDataBase : ScriptableObject
{
	public int id;
	public string content;
	public string actionIds;
	public string imageName;
}