﻿using UnityEngine;
public class ActionClipDataBase : ScriptableObject
{
	public int id;
	public int actionId;
	public string frameRegion;
	public int targetType;
	public int targetId;
	public bool isKeyClip;
}