﻿using UnityEngine;
public class ModelDataBase : ScriptableObject
{
	public int id;
	public string modelName;
}