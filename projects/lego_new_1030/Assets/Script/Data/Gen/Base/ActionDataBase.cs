﻿using UnityEngine;
public class ActionDataBase : ScriptableObject
{
	public int id;
	public int playOrder;
	public string instruction;
	public string listName;
	public bool isAssembly;
	public int partId;
	public int toolId;
	public string clickObjectName;
	public int deviceId;
	public int heatTime;
	public int heatTemperature;
}