﻿using System.Collections.Generic;
public class ActionData : ActionDataBase
{
    private List<ActionClipData> m_ActionClipDatas = new List<ActionClipData>();
    public List<ActionClipData> ClipDatas
    {
        get
        {
            if (m_ActionClipDatas == null || m_ActionClipDatas.Count <= 0)
            {
                m_ActionClipDatas = ManagerData.Map.ActionClipDataList.FindAll(clip => clip.actionId == this.id);
            }
            return m_ActionClipDatas;
        }
    }
    private List<ActionClipData> m_StaticClips = new List<ActionClipData>();
    public List<ActionClipData> StaticClips
    {
        get
        {
            if (m_StaticClips.Count <= 0)
            {
                m_StaticClips = ClipDatas.FindAll(c => c.IsStatic);
            }
            return m_StaticClips;
        }
    }
    private List<ActionClipData> m_NormalClips = new List<ActionClipData>();
    public List<ActionClipData> NormalClips
    {
        get
        {
            if (m_NormalClips.Count <= 0)
            {
                m_NormalClips = ClipDatas.FindAll(c => !c.IsStatic);
            }
            return m_NormalClips;
        }
    }
    public PartData GetPartData()
    {
        return ManagerData.Get<PartData>(this.partId);
    }
    public ToolData GetToolData()
    {
        return ManagerData.Get<ToolData>(this.toolId);
    }
}