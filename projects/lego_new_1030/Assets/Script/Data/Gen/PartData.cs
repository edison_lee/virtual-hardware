﻿using System;
using UnityEngine;
using System.Collections.Generic;
public class PartData : PartDataBase
{
    private PartNodeType nodeType = PartNodeType.None;
    public PartNodeType NodeType
    {
        get
        {
            nodeType = nodeType == PartNodeType.None ? this.belongPartId == 0 ? PartNodeType.Root : ManagerData.Map.PartDataList.Exists(pd => pd.belongPartId == this.id) ? PartNodeType.Normal : PartNodeType.Leaf : nodeType;
            return nodeType;
        }
    }
    private List<PartData> children = new List<PartData>();
    public List<PartData> Children
    {
        get
        {
            if (children == null || children.Count <= 0)
            {
                children = ManagerData.Map.PartDataList.FindAll(data => data.belongPartId == this.id);
            }
            return children;
        }
    }
    private PartData m_Parent;
    public PartData Parent
    {
        get
        {
            if (!m_Parent)
                m_Parent = ManagerData.Get<PartData>(this.belongPartId);
            return m_Parent;
        }
    }

    private List<ActionData> actions = new List<ActionData>();
    public List<ActionData> Actions
    {
        get
        {
            if (actions == null || actions.Count <= 0)
            {
                actions = ManagerData.Map.ActionDataList.FindAll(data => data.partId == this.id);
            }
            return actions;
        }
    }

    // private bool m_IsPositionBeforAssemblyDirty = true;
    // private Vector3 m_PositionBeforAssembly = Vector3.zero;
    // public Vector3 PositionBeforAssembly
    // {
    //     get
    //     {
    //         try
    //         {
    //             if (m_IsPositionBeforAssemblyDirty)
    //             {
    //                 if (!string.IsNullOrEmpty(posBeforeAssembly))
    //                 {
    //                     string[] prms = this.posBeforeAssembly.Split(',');
    //                     m_PositionBeforAssembly = new Vector3(float.Parse(prms[0]), float.Parse(prms[1]), float.Parse(prms[2]));
    //                 }
    //                 m_IsPositionBeforAssemblyDirty = false;
    //             }
    //         }
    //         catch (Exception e)
    //         {
    //             Debug.LogError(e.StackTrace);
    //             Debug.LogWarning("PartId=" + this.id.ToString() + ", posBeforeAssembly=" + this.posBeforeAssembly);
    //         }
    //         return m_PositionBeforAssembly;
    //     }
    // }



    private static LegoPartPos m_LegoPartPosInfo;
    public static LegoPartPos PartPosInfo
    {
        get
        {
            if (m_LegoPartPosInfo == null)
                m_LegoPartPosInfo = Resources.Load<LegoPartPos>("PartPos/PartPosInfo");
            return m_LegoPartPosInfo;
        }
    }
    private LegoPartPosItem m_PartPosItem;
    public Vector3 PositionBeforAssembly
    {
        get
        {
            if (m_PartPosItem == null)
            {
                m_PartPosItem = PartPosInfo.Get(modelName);
            }
            return m_PartPosItem.PartPosition;
        }
    }
    public bool IsRoot { get { return belongPartId == 0; } }
    public bool IsFocusPart { get { return focusIndex > 0; } }
    public DeviceData Device
    {
        get
        {
            return ManagerData.Get<DeviceData>(this.deviceId);
        }
    }
    public DeviceData GetDeviceData()
    {
        return ManagerData.Get<DeviceData>(this.deviceId);
    }

    private List<string> m_Pns = null;
    public List<string> GetPNs()
    {
        if (m_Pns == null || m_Pns.Count <= 0)
            m_Pns = new List<string>(pn.Split(','));
        return m_Pns;
    }

}