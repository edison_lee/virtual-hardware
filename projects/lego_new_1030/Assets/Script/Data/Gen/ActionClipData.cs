﻿using UnityEngine;
public class ActionClipData : ActionClipDataBase
{
    public static float SPEED_DEFAULT = 0.1f;
    public static float Speed { get { return SPEED_DEFAULT; } }
    private int startFrame = -1;
    private int endFrame = -1;
    public int StartFrame
    {
        get
        {
            if (startFrame < 0)
            {
                string[] p = this.frameRegion.Split('-');
                startFrame = int.Parse(p[0]);
                endFrame = int.Parse(p[1]);
            }
            return startFrame;
        }
    }
    public int EndFrame
    {
        get
        {
            if (endFrame < 0)
            {
                string[] p = this.frameRegion.Split('-');
                startFrame = int.Parse(p[0]);
                endFrame = int.Parse(p[1]);
            }
            return endFrame;
        }
    }
    public bool IsTool
    {
        get
        {
            return targetType == 1;
        }
    }
    public bool IsInternal
    {
        get
        {
            return targetId == BelongAction.partId;
        }
    }
    private ActionData m_BelongAction;
    public ActionData BelongAction
    {
        get
        {
            if (m_BelongAction == null)
            {
                m_BelongAction = ManagerData.Get<ActionData>(actionId);
            }
            return m_BelongAction;
        }
    }

    public bool IsStatic
    {
        get
        {
            return (string.IsNullOrEmpty(frameRegion) || frameRegion == "0" || frameRegion.IndexOf("-") < 0) ? true : false;
        }
    }

    public string GetTargetModelName()
    {
        if (IsTool)
        {
            ToolData td = ManagerData.Get<ToolData>(targetId);
            return td.modelName;
        }
        else
        {
            PartData pd = ManagerData.Get<PartData>(targetId);
            if (pd == null)
            {
                Debug.LogError("Class 'ActionClipData'\nid=" + this.id.ToString() + ", targetId=" + this.targetId.ToString());
            }
            return pd.modelName;
        }
    }
    public string GetAnimName()
    {
        return "Anim_" + this.id.ToString();
    }
    public float GetAnimTimeInSeconds()
    {
        float frameCount = EndFrame - StartFrame;
        return frameCount / 30f / Speed;
    }
}