﻿using UnityEngine;
using System.Collections.Generic;
public class TipData : TipDataBase
{
    private List<int> m_BelongActions = new List<int>();
    public List<int> GetBelongActions()
    {
        if (m_BelongActions == null || m_BelongActions.Count <= 0)
        {
            List<string> list = new List<string>(this.actionIds.Split(','));
            m_BelongActions = list.ConvertAll<int>(id => int.Parse(id));
        }
        return m_BelongActions;
    }
}