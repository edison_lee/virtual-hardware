﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class DeviceData : DeviceDataBase
{
    private List<PartData> parts = new List<PartData>();
    public List<PartData> Parts
    {
        get
        {
            if (parts == null || parts.Count <= 0)
            {
                parts = ManagerData.Map.PartDataList.FindAll(p => p.deviceId == this.id);
            }
            return parts;
        }
    }
    private List<ActionData> actions = new List<ActionData>();
    public List<ActionData> Actions
    {
        get
        {
            if (actions == null || actions.Count <= 0)
            {
                actions = ManagerData.Map.ActionDataList.FindAll(a => Parts.Find(p => p.id == a.partId) != null);
            }
            return actions;
        }

    }
    private List<ActionClipData> actionClips = new List<ActionClipData>();
    public List<ActionClipData> ActionClips
    {
        get
        {
            if (actionClips == null || actionClips.Count <= 0)
            {
                actionClips = ManagerData.Map.ActionClipDataList.FindAll(clip => clip && (!clip.IsStatic) && Actions.Find(a => a.id == clip.actionId) != null);
            }
            return actionClips;
        }
    }
    private List<ActionClipData> m_ClipsToPlay = new List<ActionClipData>();
    public List<ActionClipData> ClipsToPlay
    {
        get
        {
            if (m_ClipsToPlay == null || m_ClipsToPlay.Count <= 0)
            {
                m_ClipsToPlay = ManagerData.Map.ActionClipDataList.FindAll(clip => clip && Actions.Find(a => a.id == clip.actionId) != null);
            }
            return m_ClipsToPlay;
        }
    }
    private List<ToolData> tools = new List<ToolData>();
    public List<ToolData> Tools
    {
        get
        {
            if (tools == null || tools.Count <= 0)
            {
                tools = ManagerData.Map.ToolDataList.FindAll(t => ActionClips.Find(clip => clip.targetId == t.id) != null);
            }
            return tools;
        }
    }
    private int ActionDataSortFunc(ActionData a, ActionData b)
    {
        return a.playOrder.CompareTo(b.playOrder);
    }
    private List<ActionData> actionsAssembly = new List<ActionData>();
    private List<ActionData> actionsDissembly = new List<ActionData>();
    public List<ActionData> GetActionsByOperationType(bool isAssembly)
    {
        if (isAssembly)
        {
            if (actionsAssembly == null || actionsAssembly.Count <= 0)
            {
                actionsAssembly = Actions.FindAll(a => a.isAssembly);
                actionsAssembly.Sort(ActionDataSortFunc);
            }
            return actionsAssembly;
        }
        else
        {
            if (actionsDissembly == null || actionsDissembly.Count <= 0)
            {
                actionsDissembly = Actions.FindAll(a => !a.isAssembly);
                actionsDissembly.Sort(ActionDataSortFunc);
            }
            return actionsDissembly;
        }
    }
    public string GetPrefabName()
    {
        return model + "_Part_Phone";
    }
    public string GetDisplayName()
    {
        return "MOTO-" + model;
    }
    public ActionData GetActionDataByPlayOrder(int order)
    {
        return Actions.Find(a => a.playOrder == order);
    }
    private List<PartData> m_FocusPartDatas = new List<PartData>();
    public List<PartData> GetFocusPartDatas()
    {
        if (m_FocusPartDatas.Count <= 0)
        {
            m_FocusPartDatas = Parts.FindAll(p => p.focusIndex > 0);
            m_FocusPartDatas.Sort((p1, p2) => p1.focusIndex.CompareTo(p2.focusIndex));
        }
        return m_FocusPartDatas;
    }
}