﻿using UnityEngine;
public class ToolData : ToolDataBase
{
    public string GetPrefabPath()
    {
        return "Prefab/Tool/" + GetObjectName();
    }
    public string GetObjectName()
    {
        return "Tool_" + id.ToString() + modelName.Replace("Tool", string.Empty);
    }
    public string GetAnimatorControllerFileName(DeviceData device, bool withSuffix = true)
    {
        return device.model + "_" + modelName + (withSuffix ? ".controller" : string.Empty);
    }
}