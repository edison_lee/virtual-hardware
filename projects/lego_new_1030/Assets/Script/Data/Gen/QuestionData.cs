﻿using UnityEngine;
using System.Collections.Generic;
public class QuestionData : QuestionDataBase
{
    private string[] Options;
    public bool IsSingleOption { get { return type == 1; } }
    public bool IsAnswerCorrect(Dictionary<int, bool> options)
    {
        string[] answers = answer.Split(',');
        for (int i = 0; i < answers.Length; i++)
        {
            bool flag = false;
            foreach (KeyValuePair<int, bool> kvp in options)
            {
                int ans;
                if (int.TryParse(answers[i], out ans) && kvp.Value)
                {
                    if (ans == kvp.Key)
                    {
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag)
            {
                return false;
            }
        }
        return true;
    }
    public bool IsSingleAnswerCorrect(int anserIndex)
    {
        string[] answers = answer.Split(',');
        for (int i = 0; i < answers.Length; i++)
        {
            int ans;
            if (int.TryParse(answers[i], out ans))
            {
                if (ans == anserIndex)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public string[] GetOptions()
    {
        if (Options == null || Options.Length == 0)
            Options = options.Split('\n');
        return Options;
    }
}