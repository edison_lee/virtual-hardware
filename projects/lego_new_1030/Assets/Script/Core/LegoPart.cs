using UnityEngine;
using UnityEngine.EventSystems;
[ExecuteInEditMode]
public class LegoPart : LegoEntity
{
    [SerializeField]
    private PartData m_PartData;
    public PartData partData { get { return m_PartData; } set { m_PartData = value; } }
    private bool m_IsBinded = true;
    public bool IsBinded { get { return m_IsBinded; } set { m_IsBinded = value; } }
    private static LegoPartPos m_LegoPartPosInfo;
    public static LegoPartPos PartPosInfo
    {
        get
        {
            if (m_LegoPartPosInfo == null)
                m_LegoPartPosInfo = Resources.Load<LegoPartPos>("PartPos/" + Profile.Instance.CurrentDeviceData.model + "PartPosInfo");
            return m_LegoPartPosInfo;
        }
    }
    private Vector3 GetDisassemblyDefaultPos()
    {
        LegoPartPosItem item = PartPosInfo.Get(m_PartData.modelName);
        return item != null ? item.PartPosition : Vector3.zero;
    }
    public void BreakAlone(bool resetPos = false)
    {
        if(!m_PartData.active)return;
        transform.SetParent(null);
        if (resetPos)
            transform.position = GetDisassemblyDefaultPos();
        IsBinded = false;
    }
    public void JoinAlone(bool reset = false)
    {
        GameObject pnt = GameObject.Find(m_PartData.hangName);
        if (pnt)
        {
            transform.SetParent(pnt.transform, true);
            if (reset)
            {
                transform.localPosition = Vector3.zero;
                transform.localRotation = Quaternion.identity;
            }
            IsBinded = true;
        }
    }
    public void BreakRecursive(bool resetPos = false)
    {
        BreakAlone(resetPos);
        LegoPart[] lps = GetComponentsInChildren<LegoPart>();
        for (int i = 0; i < lps.Length; i++)
        {
            lps[i].BreakAlone(resetPos);
        }
    }
    public void JoinRecursive(bool resetPos = false)
    {
        JoinAlone(resetPos);
        if (m_PartData.Children != null && m_PartData.Children.Count > 0)
        {
            for (int i = 0; i < m_PartData.Children.Count; i++)
            {
                PartData pd = m_PartData.Children[i];
                GameObject child = GameObject.Find(pd.modelName);
                if (child)
                {
                    LegoPart lp = child.GetComponent<LegoPart>();
                    if (lp)
                    {
                        lp.JoinRecursive(resetPos);
                    }
                }
            }
        }
    }

    private bool IsPointerOverUI()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        return EventSystem.current.IsPointerOverGameObject();
#else
        int cnt = Input.touchCount;
        for (int i = 0; i < cnt; i++)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId))
                return true;
        }
        return false;
#endif
    }
    public void OnMouseUpAsButton()
    {
        if (IsPointerOverUI()) return;
        LegoWorld.Instance.OnPartSelected(m_PartData);
    }
    public void TrackPosLog()
    {
        Debug.Log(transform.position);
    }
    public void ShowSilhouette()
    {
        Utility.SetGameObjectRecursively(gameObject, LayerMask.NameToLayer("SilhouettePart"));
    }
    public void HideSilhouette()
    {
        Utility.SetGameObjectRecursively(gameObject, LayerMask.NameToLayer("Default"));
    }
}