using UnityEngine;
using UnityEngine.Animations;
[RequireComponent(typeof(LegoClip))]
public class LegoTool : LegoEntity
{
    [SerializeField]
    private ToolData m_ToolData;
    public ToolData toolData { get { return m_ToolData; } set { m_ToolData = value; } }
    public void InitAnimator()
    {
        if (!m_ToolData)
        {
            Debug.LogError("the tool data is null.");
            return;
        }
        string assetPath = "AnimationController/" + m_ToolData.GetAnimatorControllerFileName(Profile.Instance.CurrentDeviceData, false);
        RuntimeAnimatorController animatorController = Resources.Load<RuntimeAnimatorController>(assetPath);
        if (!animatorController)
        {
            Debug.LogError("the animator controller cannot be loaded. PATH=" + assetPath);
            return;
        }
        ClipItem.SetAnimController(animatorController);
    }
}