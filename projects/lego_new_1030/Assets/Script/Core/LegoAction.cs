﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class LegoAction : Opera
{
    private ActionData m_ActionData;
    private Action<ActionData> m_ActionStartHandler;
    private List<LegoClipAction> m_Clips = new List<LegoClipAction>();
    public LegoAction(ActionData data, Action<ActionData> actionStartHandler, Action<object> completeHandler = null) : base(completeHandler)
    {
        m_ActionData = data;
        m_ActionStartHandler = actionStartHandler;
        result = data;
    }
    public override void Execute()
    {
        onActionStart();
    }

    //动作开始
    private void onActionStart()
    {
        if (m_ActionStartHandler != null)
            m_ActionStartHandler(m_ActionData);

        if (m_ActionData && m_ActionData.NormalClips != null && m_ActionData.NormalClips.Count > 0)
        {
            m_ActionData.NormalClips.ForEach(ctp =>
            {
                var lca = new LegoClipAction(ctp, onClipEnd);
                m_Clips.Add(lca);
                lca.Execute();
            });
        }

        setStaticClipObjectiVisible(true);
    }
    //动作剪辑结束
    private void onClipEnd(object clip)
    {
        // Debug.Log("is all complete : " + m_Clips.TrueForAll(a => a.IsComplete).ToString());
        if (m_Clips.TrueForAll(a => a.IsComplete))
        {
            onActionEnd();
        }
    }
    //动作结束
    private void onActionEnd()
    {
        // setStaticClipObjectiVisible(false);
        End();
    }
    private void setStaticClipObjectiVisible(bool visible)
    {
        if (m_ActionData && m_ActionData.StaticClips != null && m_ActionData.StaticClips.Count > 0)
        {
            m_ActionData.StaticClips.ForEach(data =>
            {
                if (data.IsTool)
                {
                    LegoTool lTool = LegoWorld.Instance.GetLegoTool(data.targetId);
                    if (lTool && !lTool.gameObject.activeSelf)
                    {
                        lTool.gameObject.SetActive(visible);
                    }
                }
            });
        }
    }
    public void Pause()
    {
        m_Clips.ForEach(c => c.onClipPause());
    }
    public void Continue()
    {
        m_Clips.ForEach(c => c.onClipContinue());
    }

    public override void Destroy()
    {
        m_Clips.ForEach(c => c.Destroy());
        base.Destroy();
    }
}