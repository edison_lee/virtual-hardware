using System;
using UnityEngine;

using System.Collections.Generic;
[RequireComponent(typeof(Animator))]
public class LegoClip : MonoBehaviour
{
    private const int DefaultLayer = 0;
    private const float CrossFadeTrasitionDuration = 0f;
    private Action<int> m_OnClipStart, m_OnClipEnd;
    private Animator m_Anim;
    private List<AnimationClip> m_Clips = new List<AnimationClip>();

    private ActionClipData m_CurrentClipData;
    private AnimationClip m_CurrentClip;
    private float m_NormalizedTime = 0f;
    void Awake()
    {
        m_Anim = GetComponent<Animator>();
        if (m_Anim && m_Anim.runtimeAnimatorController)
            m_Clips = new List<AnimationClip>(m_Anim.runtimeAnimatorController.animationClips);
    }
    public void RegistCallBack(Action<int> onStart, Action<int> onEnd)
    {
        m_OnClipStart = onStart;
        m_OnClipEnd = onEnd;
    }
    public void RemoveCallBack()
    {
        m_OnClipStart = m_OnClipEnd = null;
    }


    //clipData为空，继续播放当前动画。不为空，重新播放动画。默认使用CrossFade
    public void PlayClip(ActionClipData clipData = null)
    {
        if (clipData != null)
        {
            m_CurrentClipData = clipData;
            string clipName = "Anim_" + clipData.id.ToString();
            m_NormalizedTime = 0;
            if (m_Clips.Exists(c => c.name.Equals(clipName)))
                m_Anim.CrossFade(clipName, CrossFadeTrasitionDuration, DefaultLayer, m_NormalizedTime);
            else
                OnClipEnd(m_CurrentClipData.id);
        }
        m_Anim.speed = ActionClipData.Speed;
    }

    public void Pause()
    {
        m_Anim.speed = 0;
        m_NormalizedTime = m_Anim.GetCurrentAnimatorStateInfo(DefaultLayer).normalizedTime;
    }
    // public void Reset()
    // {
    //     m_Anim.Play("Default");
    //     m_NormalizedTime = 0f;
    // }
    public void SetAnimController(RuntimeAnimatorController rac)
    {
        m_Anim.runtimeAnimatorController = rac;
        if (m_Anim && m_Anim.runtimeAnimatorController)
            m_Clips = new List<AnimationClip>(m_Anim.runtimeAnimatorController.animationClips);
    }
    public void OnClipStart(int clipId)
    {
        if (m_OnClipStart != null)
            m_OnClipStart(clipId);
    }
    public void OnClipEnd(int clipId)
    {
        // Debug.Log(clipId.ToString() + " ended!");
        if (m_OnClipEnd != null)
            m_OnClipEnd(clipId);
    }
}