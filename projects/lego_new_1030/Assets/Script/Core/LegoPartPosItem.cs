﻿using UnityEngine;
using System;
[Serializable]
public class LegoPartPosItem
{
    public string PartName;
    public Vector3 PartPosition;
}
