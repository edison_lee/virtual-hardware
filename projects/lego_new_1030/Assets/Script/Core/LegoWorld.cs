using UnityEngine;
using System;
using System.Collections.Generic;
public class LegoWorld
{
    private static LegoWorld instance;
    public static LegoWorld Instance
    {
        get
        {
            if (instance == null)
                instance = new LegoWorld();
            return instance;
        }
    }
    private Dictionary<int, LegoTool> m_ToolMap = new Dictionary<int, LegoTool>();
    private Dictionary<int, LegoPart> m_PartMap = new Dictionary<int, LegoPart>();
    private LegoPart m_RootPart;
    private GQueue m_ActionQueue;
    private GameMode m_Mode;
    public GameMode Mode { get { return m_Mode; } }
    private bool m_IsAssemply;
    public bool IsAssembly { get { return m_IsAssemply; } }

    private LegoCam m_Camera;
    private List<ActionData> Actions { get { return Profile.Instance.CurrentDeviceData.GetActionsByOperationType(IsAssembly); } }
    private ActionData m_CurrentAction;
    public ActionData CurrentAction { get { return m_CurrentAction; } }
    private ToolData m_CurrentTool;
    private PanelOperation m_OperationPanel { get { return PanelOperation.Instance; } }
    private PartData SelectedPart { get { return m_OperationPanel.CurrentPartData; } }
    private GameObject m_CurrentDevicePrefab;
    private int m_Order = 0;

    // 函数说明     游戏开始函数
    // 参数说明     model机型, gameType游戏类型(教学、比赛), opType操作类型(0拆、1装)     
    public void StartUp(GameMode gm, bool isAssembly, Action<object> callBack = null, int target = 0, int mode = 0)
    {
        CheckGuid(gm, isAssembly, () =>
        {
            if (gm == GameMode.Teaching)
            {
                SceneEnter(gm, isAssembly);
            }
            else if (gm == GameMode.Competition)
            {
                new RpcExamStart(target, isAssembly ? 1 : 0, (o) =>
                    {
                        if (callBack != null)
                            callBack(null);
                        SceneEnter(gm, isAssembly);
                    }).Execute();
            }
            else if (gm == GameMode.Examination)
            {
                ManagerUI.Instance.Open<PanelExam>();
            }
        });
    }
    //检测新手引导
    private void CheckGuid(GameMode gm, bool isAssembly, Action callBack)
    {
        GuidPage gp = (GuidPage)gm;
        if (!Main.Instance.HasEverLogin && !Main.Instance.IsPanelOpened(gp))
        {
            ManagerUI.Instance.Open<PanelGuid>(new PanelGuidParam(gp, () =>
            {
                callBack();
            }));
        }
        else
        {
            callBack();
        }
    }
    private void SceneEnter(GameMode gm, bool isAssembly)
    {
        m_Mode = gm;
        m_IsAssemply = isAssembly;
        initScene();
        initGame();
    }
    //初始化场景
    private void initScene()
    {
        //--------------------------------部件相关--------------------------------
        DeviceData device = Profile.Instance.CurrentDeviceData;
        if (m_CurrentDevicePrefab == null || !m_CurrentDevicePrefab.name.Equals(device.GetPrefabName()))
        {
            string path = "Prefab/Device/" + device.GetPrefabName();
            m_CurrentDevicePrefab = Resources.Load<GameObject>(path);
        }

        GameObject objectDevice = GameObject.Instantiate<GameObject>(m_CurrentDevicePrefab);
        objectDevice.name = objectDevice.name.Replace("(Clone)", string.Empty);

        // m_PartMap.Clear();
        Profile.Instance.CurrentDeviceData.Parts.ForEach(p =>
        {
            GameObject go = GameObject.Find(p.modelName);
            if (go)
            {
                LegoPart lp = go.GetComponent<LegoPart>();
                if (lp)
                {
                    // Debug.Log("Part-" + p.id.ToString());
                    m_PartMap.Add(p.id, lp);
                    if (p.IsRoot)
                    {
                        m_RootPart = lp;
                        // if (m_IsAssemply)
                        //     lp.BreakRecursive(true);
                    }

                    if (m_IsAssemply)
                        lp.BreakAlone(true);
                }
            }
        });

        // MeshCollider[] meshColliders = m_RootPart.GetComponentsInChildren<MeshCollider>();
        // for (int i = 0; i < meshColliders.Length; i++)
        // {
        //     meshColliders[i].enabled = m_Mode == GameMode.Competition;
        // }

        foreach (KeyValuePair<int, LegoPart> pair in m_PartMap)
        {
            MeshCollider mc = pair.Value.GetComponent<MeshCollider>();
            if (mc)
            {
                if (m_Mode != GameMode.Competition)
                {
                    GameObject.Destroy(mc);
                }
            }
        }


        //--------------------------------工具相关--------------------------------
        var tools = device.Tools;
        for (int i = 0; i < tools.Count; i++)
        {
            ToolData tool = tools[i];
            LegoTool lTool;
            if (!m_ToolMap.TryGetValue(tool.id, out lTool))
            {
                var prefab = Resources.Load<GameObject>(tool.GetPrefabPath());
                if (prefab)
                {
                    GameObject obj = GameObject.Instantiate<GameObject>(prefab);
                    obj.name = obj.name.Replace("(Clone)", string.Empty);
                    lTool = obj.GetComponent<LegoTool>();
                    m_ToolMap[tool.id] = lTool;
                }
            }
            lTool.InitAnimator();
            lTool.gameObject.SetActive(false);
        }


        //--------------------------------相机相关--------------------------------
        if (m_Camera == null)
        {
            //加载相机
            var prefab = Resources.Load<GameObject>("Prefab/LegoCamera");
            GameObject objectCamera = GameObject.Instantiate<GameObject>(prefab);
            m_Camera = objectCamera.GetComponent<LegoCam>();
        }
        else
        {
            m_Camera.gameObject.SetActive(true);
        }
        m_Camera.Target = m_RootPart.transform;
    }
    private void initGame()
    {
        ManagerUI.Instance.Open<PanelOperation>();
        m_OperationPanel.ResetFocusPartEnable(m_IsAssemply);

        if (m_Mode == GameMode.Teaching)
        {
            playActionFrom(Actions[0], false);
        }
        else if (m_Mode == GameMode.Competition)
        {
            m_CurrentAction = Actions[0];
            m_OperationPanel.UpdateActionInfo(m_CurrentAction);
        }
    }
    public void SceneExit()
    {
        if (m_Camera)
            m_Camera.gameObject.SetActive(false);

        if (m_ActionQueue != null)
        {
            m_ActionQueue.Destroy();
            m_ActionQueue = null;
        }
        m_CurrentAction = null;
        m_CurrentTool = null;
        m_RootPart = null;
        foreach (KeyValuePair<int, LegoPart> pair in m_PartMap)
            if (pair.Value != null)
                GameObject.DestroyImmediate(pair.Value.gameObject);
        m_PartMap.Clear();
    }

    //教学模式
    #region Teaching
    private ActionData m_CurrentStartAction;
    private bool m_IsSingleActionTeaching;
    public void StartTeachingFrom(ActionData act, bool isSingle)
    {
        if (act == null) return;
        SceneExit();
        initScene();
        playActionFrom(act, isSingle);
    }
    private void playActionFrom(ActionData act, bool isSingle)
    {
        m_CurrentStartAction = act;
        m_IsSingleActionTeaching = isSingle;

        int idx = Actions.IndexOf(act);
        List<Opera> operas = new List<Opera>();
        for (int i = 0; i < Actions.Count; i++)
        {
            ActionData ad = Actions[i];
            if (i < idx)
            {
                LegoPart lp = GetLegoPart(ad.partId);
                if (IsAssembly)
                {
                    lp.JoinAlone(true);
                }
                else
                {
                    lp.BreakAlone(true);
                }
            }
            else
            {
                operas.Add(new LegoAction(ad, onActionStartTeaching, onActionEndTeaching));
                if (isSingle)
                    break;
            }
        }
        m_ActionQueue = new GQueue(operas, false, onActionQueueComplete);
        m_ActionQueue.Execute();
    }

    //动作开始播放回调 
    private void onActionStartTeaching(ActionData data)
    {
        m_CurrentAction = data;
        m_OperationPanel.UpdateActionInfo(data);
        m_OperationPanel.UpdatePartInfo(data.GetPartData());
        m_OperationPanel.UpdateToolInfo(data.GetToolData());
    }
    private void onActionEndTeaching(object result)
    {
        ActionData a = result as ActionData;
        if (a.GetPartData().IsFocusPart)
        {
            if (PanelOperation.Instance)
                PanelOperation.Instance.UpdateFocusPartEnable(a.GetPartData().focusIndex, !a.isAssembly);
        }
    }
    //所有动作播完
    private void onActionQueueComplete(object obj)
    {
        // LegoPartPos ppi = ScriptableObject.CreateInstance<LegoPartPos>();
        // Profile.Instance.CurrentDeviceData.Parts.ForEach(p =>
        // {
        //     LegoPartPosItem item = new LegoPartPosItem();
        //     item.PartName = p.modelName;
        //     item.PartPosition = GetLegoPart(p.id).transform.position;
        //     ppi.Add(item);
        // });
        // UnityEditor.AssetDatabase.CreateAsset(ppi, "Assets/Resources/PartPos/" + Profile.Instance.CurrentDeviceData.model + "PartPosInfo.asset");
        new TimerOpera(0.1f, null, null, o => StartTeachingFrom(Actions[0], false)).Execute();
    }
    #endregion


    //比赛模式
    #region Competition
    private bool m_IsPlaying;
    private void playCurrentAction()
    {
        if (m_IsPlaying) return;

        new LegoAction(m_CurrentAction, null, onActionEndCompetition).Execute();
        m_IsPlaying = true;
    }
    private bool isLastActionOfMode()
    {
        return Actions != null && Actions.Count > 0 && Actions[Actions.Count - 1].playOrder == m_CurrentAction.playOrder;
    }
    //比赛模式动作播放结束
    private void onActionEndCompetition(object rst)
    {
        if (m_CurrentAction == null) return;

        if (isLastActionOfMode())
        {
            ManagerUI.Instance.Close(m_OperationPanel);
            this.SceneExit();
            Main.Instance.OnExamEnd(Profile.Instance.CurrentDeviceData.GetDisplayName(), m_IsAssemply ? 1 : 0, PanelOperation.Instance.GetUsedTime);
        }
        else
        {
            m_Order = m_CurrentAction.playOrder + 1;
            m_CurrentAction = Profile.Instance.CurrentDeviceData.GetActionDataByPlayOrder(m_Order);
            m_OperationPanel.UpdateActionInfo(m_CurrentAction);
        }
        m_IsPlaying = false;
    }
    public void OnToolSelected(ToolData toolData)
    {
        m_CurrentTool = toolData;
        if (IsSelectedPartCorrect())
        {
            if (!toolData.needParam)
            {
                if (IsSelectedToolCorrect())
                {
                    playCurrentAction();
                }
            }
        }
    }
    public void OnTimeAndTemperatureSubmit(int temperature, int timeInMinute)
    {
        if (IsSelectedPartCorrect() && IsSelectedToolCorrect())
        {
            if (m_CurrentTool.needParam && m_CurrentAction.heatTemperature == temperature && m_CurrentAction.heatTime == timeInMinute)
            {
                playCurrentAction();
            }
        }
    }
    private bool IsSelectedPartCorrect()
    {
        return SelectedPart && SelectedPart.id == m_CurrentAction.partId;
    }
    private bool IsSelectedToolCorrect()
    {
        return m_CurrentTool && m_CurrentTool.id == m_CurrentAction.toolId;
    }
    public void OnPartSelected(PartData partData)
    {
        if (m_OperationPanel)
        {
            m_OperationPanel.UpdatePartInfo(partData);
        }
    }
    private LegoPart m_SilhouettePart;
    public void ShowPartSilhouette(PartData partData)
    {
        if (m_SilhouettePart != null)
        {
            m_SilhouettePart.HideSilhouette();
        }
        if (partData != null)
        {
            m_SilhouettePart = GetLegoPart(partData.id);
            m_SilhouettePart.ShowSilhouette();
        }
    }
    #endregion



    //公共接口
    #region Common Interface
    public void OnFlipPart()
    {
        //TODO Edison 部件翻转暂时不做，目前G5S的资源没有做工具动画的适配
    }
    public void BreakAll()
    {
        m_RootPart.BreakRecursive(true);
    }
    public void JoinAll()
    {
        m_RootPart.JoinRecursive(true);
    }
    public LegoTool GetLegoTool(int toolId)
    {
        return m_ToolMap[toolId];

        // ToolData td = ManagerData.Map.ToolDataList.Find(t => t.id == toolId);
        // Debug.Log(td.GetObjectName());
        // GameObject to = GameObject.Find(td.GetObjectName());
        // return to.GetComponent<LegoTool>();
    }
    public LegoPart GetLegoPart(int partId)
    {
        // Debug.Log("part id = " + partId.ToString());
        return m_PartMap[partId];

        // PartData td = ManagerData.Map.PartDataList.Find(t => t.id == partId);
        // Debug.Log(td.modelName);
        // GameObject to = GameObject.Find(td.modelName);
        // return to.GetComponent<LegoPart>();
    }
    public void ChangeFocusPart(int partId)
    {
        LegoPart lpart = GetLegoPart(partId);
        if (lpart)
        {
            m_Camera.Target = lpart.transform;
            m_Camera.ForceDirty();
        }
    }
    public void Play()
    {
        LegoAction la = m_ActionQueue.Current as LegoAction;
        if (la != null)
        {
            la.Continue();
        }
        return;
    }
    public void Pause()
    {
        LegoAction la = m_ActionQueue.Current as LegoAction;
        if (la != null)
        {
            la.Pause();
        }
    }
    #endregion
}
