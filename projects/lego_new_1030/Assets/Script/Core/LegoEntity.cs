using UnityEngine;
using UnityEngine.Animations;
[RequireComponent(typeof(LegoClip))]
public class LegoEntity : MonoBehaviour
{
    private LegoClip m_ClipItem;
    public LegoClip ClipItem { get { return m_ClipItem; } }
    void Awake()
    {
        m_ClipItem = GetComponent<LegoClip>();
    }
}