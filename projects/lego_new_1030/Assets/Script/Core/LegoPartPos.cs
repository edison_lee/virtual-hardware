﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegoPartPos : ScriptableObject
{
    [SerializeField]
    public List<LegoPartPosItem> positions = new List<LegoPartPosItem>();
    public void Add(LegoPartPosItem item)
    {
        positions.Add(item);
    }

    public LegoPartPosItem Get(string modelName)
    {
        return positions.Find(item => item.PartName == modelName);
    }
}
