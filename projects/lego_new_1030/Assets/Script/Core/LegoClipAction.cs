using UnityEngine;
using System;
using System.Collections.Generic;

public class LegoClipAction : Opera
{
    private ActionClipData m_ClipData;
    private LegoClip m_Clip;
    private bool m_IsComplete;
    public bool IsComplete { get { return m_IsComplete; } }
    private LegoPart m_OPPart;
    public LegoClipAction(ActionClipData data, Action<object> completeHandler = null) : base(completeHandler)
    {
        m_ClipData = data;
        result = this;
        if (data.IsTool)
        {
            LegoTool lTool = LegoWorld.Instance.GetLegoTool(data.targetId);
            if (lTool)
            {
                m_Clip = lTool.ClipItem;
            }
        }
        else
        {
            LegoPart animPart = LegoWorld.Instance.GetLegoPart(data.targetId);
            if (animPart)
            {
                m_Clip = animPart.ClipItem;
            }
        }
        if (m_Clip)
        {
            m_Clip.RegistCallBack(onClipStart, onClipEnd);
        }
        else
        {
            Debug.LogError("lego clip cannot be fround. ActionClipData.id=" + data.id.ToString());
        }
    }
    public override void Execute()
    {
        if (!m_Clip.gameObject.activeSelf)
            m_Clip.gameObject.SetActive(true);
        m_Clip.PlayClip(m_ClipData);
        // Debug.LogWarning("clip stared : " + m_ClipData.id.ToString());
    }
    private void onClipStart(int clipId)
    {
        if (!m_ClipData.IsTool)
        {
            m_OPPart = LegoWorld.Instance.GetLegoPart(m_ClipData.BelongAction.partId);
            if (!m_OPPart) return;

            //begin of assembly
            if (m_ClipData.BelongAction.isAssembly)
            {
                m_OPPart.JoinAlone(!m_ClipData.IsInternal);
            }
            //begin of disassembly
            else
            {
                m_OPPart.JoinAlone();
            }
        }
    }
    private void onClipEnd(int clipId)
    {
        if (m_ClipData.IsTool)
        {
            m_Clip.gameObject.SetActive(false);
        }
        else
        {
            if (!m_OPPart) return;
            if (m_ClipData.BelongAction.isAssembly)
            {
                //装结束
            }
            else
            {
                //拆结束
                if (m_ClipData.isKeyClip)
                {
                    m_OPPart.BreakAlone(true);
                }
            }

            if (m_ClipData.isKeyClip && m_OPPart.partData.IsFocusPart)
            {
                if (PanelOperation.Instance)
                    PanelOperation.Instance.UpdateFocusPartEnable(m_OPPart.partData.focusIndex, !m_ClipData.BelongAction.isAssembly);
            }
        }
        m_Clip.RemoveCallBack();
        m_IsComplete = true;

        End();
    }
    public void onClipPause()
    {
        m_Clip.Pause();
    }
    public void onClipContinue()
    {
        m_Clip.PlayClip();
    }
    public override void Destroy()
    {
        m_Clip.RemoveCallBack();
        base.Destroy();
    }
}