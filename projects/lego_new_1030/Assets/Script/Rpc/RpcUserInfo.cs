/*
请求 url：   /user/info.do 
请求参数:    Long userNo                      // 用户帐号
响应参数：   String userName                  // 姓名或昵称
            String headId                    // 头像ID
	        Integer sex                      // 性别
            String country                   // 国家
            Integer loginCount               // 月登陆次数
            Integer ranking                  // 最高排名
 */
using System;
using UnityEngine;
public class RpcUserInfo : RpcBase<S2CUserInfo>
{
    public RpcUserInfo(Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "user";
        this.funcName = "info";
        C2SUserInfo data = new C2SUserInfo();
        sendData = data;
    }
    protected override void OnData(S2CUserInfo data)
    {
        Profile.Instance.UserInfo = data;
    }
}