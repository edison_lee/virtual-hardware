/*
请求 url：   /ranking/list.do  （Post请求）
请求参数:    Long userNo                         //用户帐号
            Long model                          //型号
            Integer examType                    //0 拆机  1 装机
            Integer page                        //默认为第一页 ；一页发送10条数据
响应参数：   Integer        count                  //Ranking数据结构见上《排行榜信息》
            List<Ranking>  list                  //Ranking数据结构见上《排行榜信息》
 */
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;
public class RpcRankingList : RpcBase<List<DataRanking>>
{
    public RpcRankingList(DeviceData device, int type, Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "ranking";
        this.funcName = "list";
        C2SRankingList data = new C2SRankingList();
        data.model = device.GetDisplayName();
        data.examType = type;
        sendData = data;
    }
}