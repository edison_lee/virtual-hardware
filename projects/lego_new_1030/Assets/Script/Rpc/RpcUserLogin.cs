using System;
using UnityEngine;
public class RpcUserLogin : RpcBase<S2CUserLogin>
{
    public RpcUserLogin(string account, string password, Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "user";
        this.funcName = "login";
        C2SUserLogin data = new C2SUserLogin();
        data.mobile = account;
        data.userPwd = password;
        sendData = data;
    }
    protected override void OnData(S2CUserLogin data)
    {
        Profile.Instance.UserNumber = data.userNo;
    }
}