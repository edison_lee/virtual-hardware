// 请求 url：    /exam/end.do  （Post请求）
// 请求参数:     Long userNo                        // 用户帐号
//               Integer useTime                    //使用时间

// 响应参数：    result                             //0 成功
using System;
using UnityEngine;
public class RpcExamEnd : RpcBase<S2CExamEnd>
{
    public RpcExamEnd(int useTime, Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "exam";
        this.funcName = "end";
        C2SExamEnd data = new C2SExamEnd();
        data.examType = LegoWorld.Instance.IsAssembly ? 1 : 0;
        data.useTime = useTime;
        sendData = data;
    }
}