/*
请求 url：    /exam/questions.do  （Post请求）
请求参数:     Integer userNo                     //用户帐号
              Integer useTime                    //使用时间
              Integer isPass                     //是否通过  0 没有  1 通过
              String testQuestions               //考试题  这里是json格式   {key:val}  key:试题ID   val:选项

响应参数：    result                                            //0 成功
              String data
 */
using System;
using UnityEngine;
using System.Collections.Generic;
using System.Runtime.Serialization;
public class RpcExamResult : RpcBase<S2CExamResult>
{
    public RpcExamResult(PanelExamResultModel model, Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "exam";
        this.funcName = "questions";
        C2SExamResult data = new C2SExamResult();
        data.model = Profile.Instance.CurrentDeviceData.GetDisplayName();
        data.useTime = model.UseTime;
        data.testQuestions = model.GetSelectedJson();
        data.isPass = model.IsPass();
        sendData = data;
    }
}