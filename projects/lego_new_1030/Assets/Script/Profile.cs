using System.Collections.Generic;
public class Profile : Data
{

    private static Profile instance;
    public static Profile Instance
    {
        get
        {
            if (instance == null)
                instance = new Profile();
            return instance;
        }
    }

    private S2CUserInfo m_UserInfo;
    public S2CUserInfo UserInfo { get { return m_UserInfo; } set { m_UserInfo = value; } }
    private int m_UserNumber;
    public int UserNumber { get { return m_UserNumber; } set { m_UserNumber = value; } }
    public int Sex { get { return m_UserInfo.sex; } }
    public string Country { get { return m_UserInfo.country; } }
    public string UserName { get { return m_UserInfo.userName; } }
    public S2CUserInfoExamItem[] DeviceInfos { get { return m_UserInfo.exams; } }
    public int CurrentDeviceId = 10000001;
    public int DefaultDeviceId = 10000001;
    private DeviceData currentDeviceData;
    public DeviceData CurrentDeviceData
    {
        get
        {
            // if (currentDeviceData == null)
                currentDeviceData = ManagerData.Get<DeviceData>(CurrentDeviceId);
            return currentDeviceData;
        }
    }
    public string GetLoginTime()
    {
        return m_UserInfo.loginCount.ToString();
    }
    public string GetRankString()
    {
        string rst = string.Empty;
        if (this.m_UserInfo.ranking > 0)
        {
            rst = this.m_UserInfo.ranking.ToString();
        }
        else
        {
            rst = "no ranking";
        }
        return rst;
    }
}