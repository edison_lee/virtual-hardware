public enum GuidPage
{
    Main = 0,
    Teaching,
    Competition,
    Examination
}
public enum GameMode
{
    Teaching = 1,            //教学
    Competition,    //比赛
    Examination
}
public enum PartNodeType
{
    None,
    Root,
    Normal,
    Leaf
}