﻿
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
[CustomEditor(typeof(LegoPart), true)]
[CanEditMultipleObjects]
public class LegoPartEditor : Editor
{
    public LegoPart partComp { get { return target as LegoPart; } }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();

        if (GUILayout.Button("拆下-单个"))
        {
            partComp.BreakAlone(true);
        }

        if (GUILayout.Button("拆下-递归"))
        {
            partComp.BreakRecursive(true);
        }

        if (GUILayout.Button("装上-单个"))
        {
            partComp.JoinAlone(true);
        }

        if (GUILayout.Button("装上-递归"))
        {
            partComp.JoinRecursive(true);
        }
        serializedObject.ApplyModifiedProperties();
    }
}