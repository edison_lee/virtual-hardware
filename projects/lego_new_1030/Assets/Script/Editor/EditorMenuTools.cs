using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using UnityEditor.Animations;
public class EditorMenuTools : AssetPostprocessor
{
    static string animationClipDir = "Assets/StandardAssets/AnimationClip/";
    static string partAnimationControllerDir = "Assets/StandardAssets/AnimationController/";
    static string toolAnimationControllerDir = "Assets/Resources/AnimationController/";
    static string toolPrefabDir = "Assets/Resources/Prefab/Tool/";
    static string devicePrefabDir = "Assets/Resources/Prefab/Device/";
    private const char FILE_NAME_SEGMENT_CHAR = '_';
    private const string ANIMATION_SUFFIX = "_Anim";
    private static List<ActionClipData> ActionInfoList { get { return ManagerData.Map.ActionClipDataList.FindAll(a => !a.IsStatic); } }
    private static List<ActionClipData> m_ActionClipPart = new List<ActionClipData>();
    private static List<ActionClipData> ActionClipPart
    {
        get
        {
            if (m_ActionClipPart == null || m_ActionClipPart.Count <= 0)
            {
                m_ActionClipPart = ActionInfoList.FindAll(e => !e.IsTool);
            }
            return m_ActionClipPart;
        }
    }
    #region 模型导入Unity预处理
    public void OnPreprocessModel()
    {
        string fileName, extension;
        fileName = extension = string.Empty;

        if (!TryGetPathInfo(assetPath, ref fileName, ref extension)) return;
        bool isAnimation = fileName.Contains(ANIMATION_SUFFIX);

        ModelImporter modelImporter = assetImporter as ModelImporter;
        modelImporter.importAnimation = isAnimation;
        modelImporter.addCollider = false;

        if (isAnimation)
        {
            modelImporter.isReadable = false;
            modelImporter.animationType = ModelImporterAnimationType.Generic;

            string[] vars = fileName.Split('_');
            DeviceData dvc = ManagerData.GetDeviceDataByName(vars[0]);
            if (dvc == null)
            {
                Debug.Log("设备名称" + vars[0] + "找不到设备数据");
                return;
            }

            List<ActionClipData> targetClips = dvc.ActionClips;
            if (targetClips == null || targetClips.Count <= 0)
            {
                Debug.Log("动作信息为空，请检查动作信息表！");
                Object.Destroy(modelImporter);
                return;
            }

            List<ModelImporterClipAnimation> clipList = new List<ModelImporterClipAnimation>();
            for (int i = 0; i < targetClips.Count; i++)
            {
                var clip = new ModelImporterClipAnimation();
                var info = targetClips[i];
                string modelName = info.GetTargetModelName();
                if (fileName.Contains(modelName))
                {
                    clip.name = info.GetAnimName();
                    clip.firstFrame = info.StartFrame;
                    clip.lastFrame = info.EndFrame;
                    clipList.Add(clip);
                }
            }
            modelImporter.clipAnimations = clipList.ToArray();
        }
        else
        {
            // modelImporter.animationType = ModelImporterAnimationType.None;
            modelImporter.isReadable = true;
        }
    }
    public void OnPostprocessModel(GameObject go)
    {
        string fileName = string.Empty;
        string extension = string.Empty;
        bool isAnim;
        if (!TryGetPathInfo(assetPath, ref fileName, ref extension))
            return;
        isAnim = fileName.Contains(ANIMATION_SUFFIX);
        if (isAnim) return;

        string[] vars = fileName.Split(FILE_NAME_SEGMENT_CHAR);

        if (vars.Length <= 2) return;

        PartData partData = ManagerData.GetPartDataByModelName(fileName);

        if (partData && partData.IsRoot) return;

        Mesh mesh = null;

        MeshFilter mf = go.GetComponentInChildren<MeshFilter>();
        if (mf)
        {
            mesh = mf.sharedMesh;
        }

        if (mesh == null)
        {
            SkinnedMeshRenderer smr = go.GetComponentInChildren<SkinnedMeshRenderer>();
            if (smr)
            {
                mesh = smr.sharedMesh;
            }
        }

        if (mesh == null)
        {
            Debug.LogError("model named " + go.name + " cannot find the mesh.");
        }

        MeshCollider mc = go.AddComponent<MeshCollider>();
        if (mc)
        {
            mc.convex = false;
            mc.sharedMesh = mesh;

        }
        else
        {
            Debug.LogError("mesh collider component add failed.");
        }
    }
    #endregion

    //Step-1    生成动画文件以及动画事件
    [MenuItem(Strings.MENU_ASSET_GEN_ANIMATION, false, 1)]
    public static void CreateAnimationClipAndClipEvent()
    {
        UtilityEditor.ClearOrCreateDirectory(animationClipDir);
        string animPath = Application.dataPath + "/StandardAssets/Fbx/Animation/";
        string[] filePaths = Directory.GetFiles(animPath);
        foreach (string path in filePaths)
        {
            string p = path.Substring(path.IndexOf("Assets"));
            Object[] objs = AssetDatabase.LoadAllAssetsAtPath(p);
            foreach (Object o in objs)
            {
                if (o.GetType() == typeof(AnimationClip) && o.name.IndexOf("Anim_") >= 0)
                {
                    AnimationClip newClip = new AnimationClip();
                    EditorUtility.CopySerialized(o, newClip);
                    int clipId = int.Parse(newClip.name.Split('_')[1]);
                    AnimationEvent clipStartEvent = new AnimationEvent();
                    clipStartEvent.functionName = "OnClipStart";
                    clipStartEvent.time = 0f;
                    clipStartEvent.intParameter = clipId;
                    AnimationEvent clipEndEvent = new AnimationEvent();
                    clipEndEvent.functionName = "OnClipEnd";
                    clipEndEvent.time = newClip.length;
                    clipEndEvent.intParameter = clipId;
                    AnimationEvent[] events = new AnimationEvent[] { clipStartEvent, clipEndEvent };
                    AnimationUtility.SetAnimationEvents(newClip, events);
                    AssetDatabase.CreateAsset(newClip, animationClipDir + o.name + ".anim");
                }
            }
        }

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    //Step-3    生成设备的prefab，初始化部件信息
    [MenuItem(Strings.MENU_ASSET_GEN_DEVICE, false, 2)]
    public static void CreateDevicePrefab()
    {
        UtilityEditor.ClearOrCreateDirectory(partAnimationControllerDir);
        UtilityEditor.ClearOrCreateDirectory(devicePrefabDir);
        List<PartData> rootParts = ManagerData.GetRootParts();
        foreach (PartData pd in rootParts)
        {
            GameObject phoneRoot = JoinParts(pd);
            if (phoneRoot)
            {
                PrefabUtility.CreatePrefab(devicePrefabDir + phoneRoot.name + ".prefab", phoneRoot);
                Object.DestroyImmediate(phoneRoot);
            }
        }
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    [MenuItem(Strings.MENU_ASSET_GEN_TOOL, false, 3)]
    public static void CreateToolPrefabsAndAnimators()
    {
        UtilityEditor.ClearOrCreateDirectory(toolAnimationControllerDir);
        UtilityEditor.ClearOrCreateDirectory(toolPrefabDir);
        //创建所有工具的prefab
        var tools = ManagerData.Map.ToolDataList;
        tools.ForEach(tool =>
        {
            string path = "Assets/StandardAssets/Fbx/Model/" + tool.modelName + ".FBX";
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            if (prefab)
            {
                GameObject obj = Object.Instantiate(prefab, Vector3.zero, Quaternion.identity);
                obj.name = obj.name.Replace("(Clone)", string.Empty);
                obj.transform.Rotate(Vector3.left, 90f);
                obj.AddComponent<LegoTool>().toolData = tool;
                PrefabUtility.CreatePrefab(toolPrefabDir + tool.GetObjectName() + ".prefab", obj);
                GameObject.DestroyImmediate(obj);
            }
            else
            {
                Debug.Log("tool prefab cannot be fround. PrefabPath=" + path);
            }
        });

        //生成每个机型的每个工具的动画控制文件
        ManagerData.Map.DeviceDataList.ForEach(dvc =>
        {
            dvc.Tools.ForEach(td =>
            {
                string animControllerFullPath = toolAnimationControllerDir + td.GetAnimatorControllerFileName(dvc);
                AnimatorController ctrl = CreateDefaultAnimController(animControllerFullPath);
                if (ctrl)
                {
                    dvc.ActionClips.ForEach(ac =>
                    {
                        if (ac.targetType == 1 && ac.targetId == td.id)
                        {
                            string ap = animationClipDir + ac.GetAnimName() + ".anim";
                            AnimationClip clip = AssetDatabase.LoadAssetAtPath<AnimationClip>(ap);
                            if (clip)
                                ctrl.AddMotion(clip);
                            else
                                Debug.LogWarning(ap);
                        }
                    });
                }
            });
        });

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }
    //生成设备的prefab
    [MenuItem(Strings.MENU_ASSET_GEN_ALL, false, 4)]
    public static void MenuAssetGenPrefab()
    {
        // EditorUtility.DisplayProgressBar()

        CreateAnimationClipAndClipEvent();
        CreateDevicePrefab();
        CreateToolPrefabsAndAnimators();

    }
    // static AnimationClip DefaultClip = new AnimationClip() { name = "Default" };
    private static AnimatorController CreateDefaultAnimController(string fullPath)
    {
        return AnimatorController.CreateAnimatorControllerAtPathWithClip(fullPath, new AnimationClip() { name = "Default" });
    }
    private static GameObject JoinParts(PartData partData)
    {
        if (partData == null)
        {
            // Debug.Log("part data is null!");
            return null;
        }

        string prefabPath = "Assets/StandardAssets/Fbx/Model/" + partData.modelName + ".FBX";
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(prefabPath);
        if (prefab == null)
        {
            // Debug.Log("the prefab is null. path = " + prefabPath);
            return null;
        }
        GameObject inst = Object.Instantiate(prefab, Vector3.zero, Quaternion.identity);
        inst.name = inst.name.Replace("(Clone)", string.Empty);
        LegoPart part = inst.AddComponent<LegoPart>();
        part.partData = partData;
        Animator animtor = inst.GetComponent<Animator>();
        if (animtor == null)
        {
            animtor = inst.AddComponent<Animator>();
        }
        AnimatorController controller = CreateDefaultAnimController(partAnimationControllerDir + partData.modelName + ".controller");
        ActionClipPart.ForEach(info =>
        {
            if (info.GetTargetModelName() == partData.modelName)
            {
                AnimationClip clip = AssetDatabase.LoadAssetAtPath<AnimationClip>(animationClipDir + info.GetAnimName() + ".anim");
                if (clip)
                {
                    controller.AddMotion(clip);
                }
            }
        });

        if (controller)
        {
            animtor.runtimeAnimatorController = controller;
        }

        if (partData.NodeType == PartNodeType.Root)
        {
            inst.transform.Rotate(Vector3.left, 90f);
            if (partData.Children != null && partData.Children.Count > 0)
            {
                foreach (PartData pd in partData.Children)
                {
                    JoinParts(pd);
                }
            }
        }
        else if (partData.NodeType == PartNodeType.Normal)
        {
            HangToParent(inst, partData);
            if (partData.Children != null && partData.Children.Count > 0)
            {
                foreach (PartData pd in partData.Children)
                {
                    JoinParts(pd);
                }
            }
        }
        else if (partData.NodeType == PartNodeType.Leaf)
        {
            HangToParent(inst, partData);
        }
        return inst;
    }
    private static bool TryGetPathInfo(string fullPath, ref string fileName, ref string extension)
    {
        if (string.IsNullOrEmpty(fullPath))
            return false;
        fileName = Path.GetFileNameWithoutExtension(fullPath);
        extension = Path.GetExtension(fullPath).ToLower();
        return true;
    }
    private static void HangToParent(GameObject child, PartData partData)
    {
        string parentPrefabName = partData.Parent.modelName;
        GameObject parent = GameObject.Find(parentPrefabName);
        if (parent == null)
        {
            Debug.LogError("找不到父部件：" + partData.Parent.modelName);
            Object.DestroyImmediate(child);
            return;
        }
        Transform trans = parent.transform.Find(partData.hangName);
        if (trans == null)
        {
            Debug.LogError("找不到挂点：" + partData.ToString());
            Object.DestroyImmediate(child);
            return;
        }
        child.transform.SetParent(trans, false);
    }
}