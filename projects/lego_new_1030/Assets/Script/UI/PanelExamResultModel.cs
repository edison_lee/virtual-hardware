using System.Collections.Generic;
public class PanelExamResultModel
{
    private int useTime;
    private List<PanelExamResultChoice> choices = new List<PanelExamResultChoice>();

    public int UseTime
    {
        get
        {
            return useTime;
        }
        set
        {
            useTime = value;
        }
    }

    private int passCount = 0;
    public int PassCount
    {
        get
        {
            if (passCount <= 0)
            {
                foreach (PanelExamResultChoice c in choices)
                {
                    QuestionData data = ManagerData.Get<QuestionData>(c.QuestionId);
                    if (data && data.IsAnswerCorrect(c.Choices))
                    {
                        passCount++;
                    }
                }
            }
            return passCount;

        }
    }
    public void AddChoice(int id, Dictionary<int, bool> selectMap, bool isCorrect)
    {
        PanelExamResultChoice choice = new PanelExamResultChoice();
        choice.QuestionId = id;
        choice.Choices = selectMap;
        choice.IsCorrect = isCorrect;
        choices.Add(choice);
    }
    public void ClearChoices()
    {
        choices.Clear();
    }
    public string GetSelectedJson()
    {
        string json = "";
        foreach (PanelExamResultChoice c in choices)
        {
            json += "{" + c.GetSelectJson() + "},";
        }
        if (!string.IsNullOrEmpty(json))
            json = json.Substring(0, json.Length - 1);
        json = string.Format("[{0}]", json);
        return json;
    }
    public int IsPass()
    {
        return PassCount >= PanelExam.PASS_COUNT ? 1 : 0;
    }

    public int GetScorePercent()
    {
        return PassCount * 20;
    }
}
