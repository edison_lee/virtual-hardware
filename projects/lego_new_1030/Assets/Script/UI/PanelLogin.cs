﻿using UnityEngine.UI;
using UnityEngine;
using System;
using DG.Tweening;
public class PanelLogin : PanelLoginBase
{
    private Action<string, string> m_OnLogin;
    public override void SetData(object data)
    {
        m_OnLogin = data as Action<string, string>;
        this.InputAccount.text = PlayerPrefs.GetString(Main.LOCAL_ACCOUNT, "");
        this.InputPassword.text = PlayerPrefs.GetString(Main.LOCAL_PASSWORD, "");
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnExit.gameObject)
        {
            Application.Quit();
        }
        else if (go == this.BtnLogin.gameObject)
        {
            if (!string.IsNullOrEmpty(InputAccount.text))
            {
                m_OnLogin(InputAccount.text, InputPassword.text);
            }
        }
        else if (go == this.BtnForgot.gameObject)
        {
            PanelAlertModel m = new PanelAlertModel();
            m.Message = "Please reset or retrieve your password in Lenovo Traning";
            m.OnClose = () =>
            {
                Debug.Log("on alert close.");
            };
            ManagerUI.Instance.Open<PanelAlert>(m);
        }
    }
}
