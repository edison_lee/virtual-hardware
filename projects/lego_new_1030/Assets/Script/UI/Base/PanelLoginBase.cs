﻿using UnityEngine.UI;
using UnityEngine;
public class PanelLoginBase : GPanelBase
{
    public GButtonColorElastic BtnLogin;
    public InputField InputAccount;
    public GButtonColorElastic BtnExit;
    public InputField InputPassword;
    public GButton BtnForgot;

    protected override void Awake()
    {
        base.Awake();
        BtnLogin = Trans.Find("Container/BtnLogin").GetComponent<GButtonColorElastic>();
        BtnLogin.OnClick = OnButtonClickHandler;
        InputAccount = Trans.Find("Container/InputAccount").GetComponent<InputField>();
        BtnExit = Trans.Find("Container/BtnExit").GetComponent<GButtonColorElastic>();
        BtnExit.OnClick = OnButtonClickHandler;
        InputPassword = Trans.Find("Container/InputPassword").GetComponent<InputField>();
        BtnForgot = Trans.Find("Container/BtnForgot").GetComponent<GButton>();
        BtnForgot.OnClick = OnButtonClickHandler;

    }
}
