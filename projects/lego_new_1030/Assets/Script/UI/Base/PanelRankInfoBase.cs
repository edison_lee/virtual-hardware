﻿using UnityEngine.UI;
using UnityEngine;
public class PanelRankInfoBase : GPanelBase
{
    public Image ImgHead;
    public GameObject FrameMode;
    public GButtonColor TxtEquipName;
    public GButtonColor TxtTime;
    public Text TxtRank;
    public GButtonColorElastic BtnRetry;
    public GButtonColorElastic BtnBack;
    public GButtonColorElastic BtnChai;
    public GButtonColorElastic BtnZhuang;
    public GUnlimitedList ListRank;

    protected override void Awake()
    {
        base.Awake();
        ImgHead = Trans.Find("Container/ImgHead").GetComponent<Image>();
        FrameMode = Trans.Find("Container/FrameMode").gameObject;
        TxtEquipName = Trans.Find("Container/TxtEquipName").GetComponent<GButtonColor>();
        TxtEquipName.OnClick = OnButtonClickHandler;
        TxtTime = Trans.Find("Container/TxtTime").GetComponent<GButtonColor>();
        TxtTime.OnClick = OnButtonClickHandler;
        TxtRank = Trans.Find("Container/TxtRank").GetComponent<Text>();
        BtnRetry = Trans.Find("Container/BtnRetry").GetComponent<GButtonColorElastic>();
        BtnRetry.OnClick = OnButtonClickHandler;
        BtnBack = Trans.Find("Container/BtnBack").GetComponent<GButtonColorElastic>();
        BtnBack.OnClick = OnButtonClickHandler;
        BtnChai = Trans.Find("Container/BtnChai").GetComponent<GButtonColorElastic>();
        BtnChai.OnClick = OnButtonClickHandler;
        BtnZhuang = Trans.Find("Container/BtnZhuang").GetComponent<GButtonColorElastic>();
        BtnZhuang.OnClick = OnButtonClickHandler;
        ListRank = Trans.Find("Container/ViewRank/ListRank").GetComponent<GUnlimitedList>();

    }
}
