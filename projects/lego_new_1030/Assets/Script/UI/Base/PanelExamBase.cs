﻿using UnityEngine.UI;
using UnityEngine;
public class PanelExamBase : GPanelBase
{
    public GLayout Option1;
    public GLayout Option2;
    public GLayout Option3;
    public GLayout Option4;
    public GLayout Option5;
    public Text TxtEquipName;
    public GButtonColor BtnClose;
    public GLayout LayRoot;
    public GLayout LayOptions;
    public Text TxtQuestionNum;
    public GLayout Option6;
    public GLayout TxtQuestion;
    public GButtonColorElastic BtnSubmit;

    protected override void Awake()
    {
        base.Awake();
        Option1 = Trans.Find("Container/ViewExam/LayRoot/LayOptions/Option1").GetComponent<GLayout>();
        Option2 = Trans.Find("Container/ViewExam/LayRoot/LayOptions/Option2").GetComponent<GLayout>();
        Option3 = Trans.Find("Container/ViewExam/LayRoot/LayOptions/Option3").GetComponent<GLayout>();
        Option4 = Trans.Find("Container/ViewExam/LayRoot/LayOptions/Option4").GetComponent<GLayout>();
        Option5 = Trans.Find("Container/ViewExam/LayRoot/LayOptions/Option5").GetComponent<GLayout>();
        TxtEquipName = Trans.Find("Container/TxtEquipName").GetComponent<Text>();
        BtnClose = Trans.Find("Container/BtnClose").GetComponent<GButtonColor>();
        BtnClose.OnClick = OnButtonClickHandler;
        LayRoot = Trans.Find("Container/ViewExam/LayRoot").GetComponent<GLayout>();
        LayOptions = Trans.Find("Container/ViewExam/LayRoot/LayOptions").GetComponent<GLayout>();
        TxtQuestionNum = Trans.Find("Container/ViewExam/LayRoot/LayQuestion/TxtQuestionNum").GetComponent<Text>();
        Option6 = Trans.Find("Container/ViewExam/LayRoot/LayOptions/Option6").GetComponent<GLayout>();
        TxtQuestion = Trans.Find("Container/ViewExam/LayRoot/LayQuestion/TxtQuestion").GetComponent<GLayout>();
        BtnSubmit = Trans.Find("Container/ViewExam/LayRoot/LayButtons/BtnSubmit").GetComponent<GButtonColorElastic>();
        BtnSubmit.OnClick = OnButtonClickHandler;

    }
}
