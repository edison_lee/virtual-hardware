﻿using UnityEngine.UI;
using UnityEngine;
public class PanelTipBase : GPanelBase
{
    public RawImage ImgTip;
    public Text TxtMessage;
    public GButtonColorElastic BtnClose;

    protected override void Awake()
    {
        base.Awake();
        ImgTip = Trans.Find("Container/ImgBack/ImgTip").GetComponent<RawImage>();
        TxtMessage = Trans.Find("Container/ImgBack/ViewMessage/TxtMessage").GetComponent<Text>();
        BtnClose = Trans.Find("Container/ImgBack/BtnClose").GetComponent<GButtonColorElastic>();
        BtnClose.OnClick = OnButtonClickHandler;
    }
}
