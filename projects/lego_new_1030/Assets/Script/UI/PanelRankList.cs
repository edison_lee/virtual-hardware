﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System;
public class PanelRankList : PanelRankListBase
{
    private int m_Mode = 0;
    private DeviceData m_CurrentDevice;
    private List<DataRanking> rankingList;

    protected override void Awake()
    {
        base.Awake();
        ListRank.OnInitItem = initItem;
        ListRank.OnSelectItemButton = clickItemButton;
        ListDeivces.OnInitItem = initItem;
        ListDeivces.OnSelectItem = clickItem;
        BtnShowDeviceList.onClick.AddListener(()=>OnButtonClickHandler(BtnShowDeviceList.gameObject));
        m_CurrentDevice = Profile.Instance.CurrentDeviceData;
    }
    public override void SetData(object data)
    {
        base.SetData(data);
        refresh();
    }
    private void onListData(object result)
    {
        TxtDeviceName.text = m_CurrentDevice.GetDisplayName();
        rankingList = result as List<DataRanking>;
        ListRank.SetItemCount(rankingList.Count);
    }
    private void onChallengeResult(object result)
    {
        DataRanking userRank = result as DataRanking;
        this.TxtRank.text = (userRank != null && userRank.ranking > 0) ? userRank.ranking.ToString() : "0";
    }
    private void refresh()
    {
        new RpcRankingInfo(m_Mode, onChallengeResult).Execute();
        new RpcRankingList(m_CurrentDevice, m_Mode, onListData).Execute();
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnBack.gameObject)
        {
            ManagerUI.Instance.Close(this);
        }
        else if (go == this.BtnChai.gameObject)
        {
            m_Mode = 0;
            refresh();
        }
        else if (go == this.BtnZhuang.gameObject)
        {
            m_Mode = 1;
            refresh();
        }
        else if (go == this.BtnShowDeviceList.gameObject)
        {
            ListDeivces.SetItemCount(ManagerData.Map.DeviceDataList.Count);
        }
    }
    private void initItem(GameObject list, GameObject item, int index)
    {
        Debug.Log(list.name);
        if (list == ListRank.gameObject)
        {
            item.transform.Find("ImgIcon1").gameObject.SetActive(false);
            item.transform.Find("ImgIcon2").gameObject.SetActive(false);
            item.transform.Find("ImgIcon3").gameObject.SetActive(false);

            DataRanking dr = rankingList[index];

            if (dr != null)
            {
                Transform rankIcon = item.transform.Find("ImgIcon" + (index + 1));
                if (rankIcon)
                {
                    rankIcon.gameObject.SetActive(true);
                }
                Text TxtRank = item.transform.Find("TxtRank").GetComponent<Text>();
                Text TxtName = item.transform.Find("TxtName").GetComponent<Text>();
                Text TxtTime = item.transform.Find("TxtTime").GetComponent<Text>();
                TxtRank.text = (index + 1).ToString();
                TxtName.text = dr.userName;
                TxtTime.text = new TimeSpan(0, 0, Convert.ToInt32(dr.useTime)).ToString();
            }
        }
        else if (list == ListDeivces.gameObject)
        {
            item.transform.Find("TxtDeviceName").GetComponent<Text>().text = ManagerData.Map.DeviceDataList[index].GetDisplayName();
        }
    }
    private void clickItem(GameObject list, GameObject item, int index)
    {
        if (list == ListDeivces.gameObject)
        {
            DeviceData newData = ManagerData.Map.DeviceDataList[index];
            if (newData != m_CurrentDevice)
            {
                m_CurrentDevice = newData;
                refresh();
            }
        }
    }
    private void clickItemButton(GameObject list, GameObject item, int index)
    {
        if (list == ListRank.gameObject)
        {
            DataRanking dr = rankingList[index];
            LegoWorld.Instance.StartUp(GameMode.Competition, m_Mode == 1, rst =>
              {
                  ManagerUI.Instance.Close(this);
                  ManagerUI.Instance.Close<PanelMain>();
              }, dr.userNo, m_Mode);
        }
    }
}
