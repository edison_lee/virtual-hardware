﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System;
public class PanelOperation : PanelOperationBase
{
    public static PanelOperation Instance;
    private List<string> m_Pns = new List<string>();
    private List<ActionData> m_Actions = new List<ActionData>();
    private List<ToolData> m_Tools = new List<ToolData>();
    private List<string> m_ToolNames = new List<string>();
    private PartData m_CurrentPartData;
    public PartData CurrentPartData { get { return m_CurrentPartData; } }
    private ToolData m_CurrentToolData;
    public ToolData CurrentToolData { get { return m_CurrentToolData; } }
    private TimerOpera m_TimerOpera;
    private int m_TotalSeconds;
    public int GetUsedTime { get { return m_TotalSeconds; } }
    private PanelOperationParamTool m_ParamTool;
    private TipData m_CurrentTipData;
    //1
    protected override void Awake()
    {
        Instance = this;
        base.Awake();
        m_ParamTool = this.FrameTempInfo.GetComponent<PanelOperationParamTool>();
        this.ListActions.OnInitItem = initItem;
        this.ListActions.OnSelectItem = clickItem;
        this.ListPartNumber.OnInitItem = initItem;
        this.ListTool.OnInitItem = initItem;
        this.ListTool.OnSelectItem = clickItem;
        this.TogglePartNumber.onValueChanged.AddListener((select) =>
        {
            if (select)
                updatePnList();
        });
        this.ToggleTitle.onValueChanged.AddListener((select) =>
        {
            if (select)
                onShowActionList();
        });

        TogPart1.onValueChanged.AddListener(select => togPartSelectChange(1, select));
        TogPart2.onValueChanged.AddListener(select => togPartSelectChange(2, select));
        TogPart3.onValueChanged.AddListener(select => togPartSelectChange(3, select));
        TogPart4.onValueChanged.AddListener(select => togPartSelectChange(4, select));

        ImgTips.onClick.AddListener(() => this.OnButtonClickHandler(ImgTips.gameObject));

        SliderSpeed.onValueChanged.AddListener(sliderValueChange);
        SliderXL.onValueChanged.AddListener(testViewDistSpeed);

        BtnPartLast.onClick.AddListener(() => this.OnButtonClickHandler(BtnPartLast.gameObject));
        BtnPartNext.onClick.AddListener(() => this.OnButtonClickHandler(BtnPartNext.gameObject));

#if UNITY_EDITOR || UNITY_STANDALONE

#else
        ToggleDebug.gameObject.SetActive(false);
#endif
    }
    private void togPartSelectChange(int index, bool select)
    {
        if (!select) return;
        var parts = Profile.Instance.CurrentDeviceData.GetFocusPartDatas();
        var part = parts.Find(p => p.focusIndex == index);
        LegoWorld.Instance.ChangeFocusPart(part.id);
    }
    private void testViewDistSpeed(float v)
    {
        LegoCam.Instance.ZoomSpeed = v * 0.01f;
    }
    private void sliderValueChange(float v)
    {
        ActionClipData.SPEED_DEFAULT = v;
    }
    //1
    public override void SetData(object data)
    {
        bool isTeaching = LegoWorld.Instance.Mode == GameMode.Teaching;

        this.TxtModelName.text = Profile.Instance.CurrentDeviceData.GetDisplayName();

        this.ToggleTitle.interactable = isTeaching;
        this.ToggleTitle.isOn = false;
        this.TogglePartNumber.isOn = false;
        this.FrameActionList.gameObject.SetActive(false);
        this.FramePartNumberList.gameObject.SetActive(false);
        this.FrameTempInfo.gameObject.SetActive(false);
        m_Tools = Profile.Instance.CurrentDeviceData.Tools;
        m_Tools.ForEach(t =>
        {
            if (!m_ToolNames.Contains(t.imageName))
                m_ToolNames.Add(t.imageName);
        });
        this.ListTool.SetItemCount(m_ToolNames.Count);

        UpdateActionInfo();
        UpdatePartInfo();
        UpdateToolInfo();
        UpdateFocusPartsText();

        // ImgTips.gameObject.SetActive(false);

        if (isTeaching)
        {
            BtnSubmit.gameObject.SetActive(false);
            FrameCtrl.SetActive(true);
            BtnPlay.gameObject.SetActive(false);
            BtnPause.gameObject.SetActive(true);
            FrameTime.gameObject.SetActive(false);
        }
        else
        {
            BtnSubmit.gameObject.SetActive(true);
            FrameCtrl.SetActive(false);
            FrameTime.gameObject.SetActive(true);
            startTimer(int.MaxValue);
        }

        ImgAssemly.gameObject.SetActive(LegoWorld.Instance.IsAssembly);
        ImgDessemly.gameObject.SetActive(!LegoWorld.Instance.IsAssembly);
    }
    //100%
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnFlip.gameObject)
        {
            onFlip();
        }
        else if (go == this.BtnSubmit.gameObject)
        {
            onSubmit();
        }
        else if (go == this.BtnClose.gameObject)
        {
            onClose();

            // ManagerUI.Instance.Close(this);
            // LegoWorld.Instance.SceneExit();
            // Main.Instance.OnExamEnd(Profile.Instance.CurrentDeviceData.GetDisplayName(), LegoWorld.Instance.IsAssembly ? 1 : 0, 2000);
        }
        else if (go == this.BtnPlay.gameObject)
        {
            LegoWorld.Instance.Play();
        }
        else if (go == this.BtnPause.gameObject)
        {
            LegoWorld.Instance.Pause();
        }
        else if (go == this.BtnPartLast.gameObject)
        {
            var parts = Profile.Instance.CurrentDeviceData.Parts.FindAll(p => !p.IsRoot);
            var idx = parts.IndexOf(m_CurrentPartData);
            idx--;
            if (idx < 0)
                idx = parts.Count - 1;
            LegoWorld.Instance.OnPartSelected(parts[idx]);
        }
        else if (go == this.BtnPartNext.gameObject)
        {
            var parts = Profile.Instance.CurrentDeviceData.Parts.FindAll(p => !p.IsRoot);
            var idx = parts.IndexOf(m_CurrentPartData);
            idx++;
            if (idx >= parts.Count)
                idx = 0;
            LegoWorld.Instance.OnPartSelected(parts[idx]);
        }
        else if (go == this.ImgTips.gameObject)
        {
            ManagerUI.Instance.Open<PanelTip>(m_CurrentTipData);
        }

        //测试按钮
        else if (go == this.BtnTestChangePart.gameObject)
        {
            testChangePart();
        }
        else if (go == this.BtnBreak.gameObject)
        {
            LegoWorld.Instance.BreakAll();
        }
        else if (go == this.BtnJoin.gameObject)
        {
            LegoWorld.Instance.JoinAll();
        }

        else if (go == this.BtnClearCache.gameObject)
        {
            PlayerPrefs.DeleteAll();
        }
    }
    //100%
    private void initItem(GameObject list, GameObject item, int index)
    {
        if (list == ListActions.gameObject)
        {
            onInitActionItem(item, index);
        }
        else if (list == ListPartNumber.gameObject)
        {
            onInitPnItem(item, index);
        }
        else if (list == ListTool.gameObject)
        {
            onInitToolItem(item, index);
        }
    }
    private ToolData getSelectedToolByImageName(string imageName)
    {
        ToolData rst = null;
        List<ToolData> selectTools = m_Tools.FindAll(t => t.imageName == imageName);
        if (selectTools.Count > 1 && LegoWorld.Instance.Mode == GameMode.Competition)
        {
            rst = selectTools.Find(t => t.id == LegoWorld.Instance.CurrentAction.toolId);
            //如果这几个工具都不是当前操作要用的就随便用一个
            if (rst == null)
            {
                rst = selectTools[0];
            }
        }
        else if (selectTools.Count == 1)
        {
            rst = selectTools[0];
        }
        return rst;
    }
    //100%
    private void clickItem(GameObject list, GameObject item, int index)
    {
        if (list == ListActions.gameObject)
        {
            LegoWorld.Instance.StartTeachingFrom(this.m_Actions[index], ToggleActionMode.isOn);
            this.ToggleTitle.isOn = false;
        }
        else if (list == ListTool.gameObject)
        {
            if (LegoWorld.Instance.Mode == GameMode.Teaching) return;
            ToolData td = getSelectedToolByImageName(m_ToolNames[index]);
            if (!td) return;
            UpdateToolInfo(td);
            LegoWorld.Instance.OnToolSelected(td);
        }
    }
    //100%
    private void updatePnList()
    {
        int cnt = 0;
        if (m_Pns != null && m_Pns.Count > 0)
        {
            cnt = m_Pns.Count;
        }
        this.ListPartNumber.SetItemCount(cnt);
    }
    //100%
    private void onInitPnItem(GameObject item, int index)
    {
        item.transform.Find("TxtPartNumber").GetComponent<Text>().text = m_Pns[index];
    }
    //100%
    private void onShowActionList()
    {
        m_Actions = Profile.Instance.CurrentDeviceData.GetActionsByOperationType(LegoWorld.Instance.IsAssembly);
        this.ListActions.SetItemCount(m_Actions.Count);
    }
    //100%
    private void onInitActionItem(GameObject item, int index)
    {
        item.transform.Find("TxtContent").GetComponent<Text>().text = m_Actions[index].listName;
    }
    //100%
    private void onInitToolItem(GameObject item, int index)
    {
        Image imgTool = item.transform.Find("ImgTool").GetComponent<Image>();
        imgTool.overrideSprite = ManagerResource.Instance.GetSpriteByName(m_ToolNames[index]);
        imgTool.SetNativeSize();
    }
    //100%
    private void onFlip()
    {
        LegoWorld.Instance.OnFlipPart();
    }
    //记录参数，动作触发检测2
    private void onSubmit()
    {
        LegoWorld.Instance.OnTimeAndTemperatureSubmit(m_ParamTool.CurrentTemperature, m_ParamTool.CurrentTime);
        m_ParamTool.Close();
    }
    //关闭界面，销毁场景内所有物体100%
    private void onClose()
    {
        ManagerUI.Instance.Close(this);
        LegoWorld.Instance.SceneExit();
        Main.Instance.Home();
    }
    //更新部件信息100%
    public void UpdatePartInfo(PartData data = null)
    {
        LegoWorld.Instance.ShowPartSilhouette(data);
        m_CurrentPartData = data;
        if (data == null)
        {
            FramePartContent.SetActive(false);
            return;
        }
        FramePartContent.SetActive(true);
        ImgRecyclable.gameObject.SetActive(data.recovery);
        ImgPart.overrideSprite = ManagerResource.Instance.GetSpriteByName(data.imageName);
        ImgPart.SetNativeSize();
        TxtPartName.text = data.partName;
        m_Pns = data.GetPNs();
        updatePnList();


    }
    //更新工具信息100%
    public void UpdateToolInfo(ToolData tool = null)
    {
        m_CurrentToolData = tool;
        if (tool == null)
        {
            ImageTool.gameObject.SetActive(false);
            if (m_ParamTool.IsShow)
                m_ParamTool.Close();
            return;
        }
        ImageTool.gameObject.SetActive(true);
        ImageTool.overrideSprite = ManagerResource.Instance.GetSpriteByName(tool.imageName);
        ImageTool.SetNativeSize();

        if (LegoWorld.Instance.Mode == GameMode.Teaching) return;

        if (tool.needParam)
        {
            int time = 0;
            int temperature = 0;
            if (LegoWorld.Instance.CurrentAction)
            {
                time = LegoWorld.Instance.CurrentAction.heatTime;
                temperature = LegoWorld.Instance.CurrentAction.heatTemperature;



                //临时代码
                string suffix = "min";
                int incresment = 1;
                if (LegoWorld.Instance.CurrentAction.id == 10300162)
                {
                    suffix = "sec";
                    incresment = 10;
                }
                //临时代码



                m_ParamTool.Show(true, time, temperature, suffix, incresment);
                return;
            }
            m_ParamTool.Show(false, time, temperature);
        }
        else
        {
            if (m_ParamTool.IsShow)
                m_ParamTool.Close();
        }
    }
    //更新当前动作信息100%
    public void UpdateActionInfo(ActionData data = null)
    {
        m_CurrentTipData = null;
        if (data == null)
        {
            TxtActionDesc.text = "";
            TxtActionName.text = "";
            ImgTips.gameObject.SetActive(false);
            return;
        }

        TxtActionDesc.text = data.instruction;
        TxtActionName.text = data.listName;

        m_CurrentTipData = ManagerData.Map.TipDataList.Find(t => t.GetBelongActions().Contains(data.id));
        ImgTips.gameObject.SetActive(m_CurrentTipData);
    }
    private void startTimer(int timeInSeconds)
    {
        if (m_TimerOpera != null)
        {
            m_TimerOpera.Destroy();
        }

        Action updateTimerText = () =>
        {
            var ts = new TimeSpan(0, 0, m_TotalSeconds);
            var totalMinutes = (int)ts.TotalMinutes;
            var seconds = ts.Seconds;
            var mstr = totalMinutes < 10 ? "0" + totalMinutes.ToString() : totalMinutes.ToString();
            var sstr = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
            this.TxtTime.text = mstr + ":" + sstr;
        };

        m_TotalSeconds = 0;
        updateTimerText();
        m_TimerOpera = new TimerOpera(timeInSeconds, () =>
        {
            m_TotalSeconds += 1;
            updateTimerText();
        }, null);
        m_TimerOpera.Execute();
    }
    protected override void OnClose()
    {
        if (m_TimerOpera != null)
        {
            m_TimerOpera.Destroy();
        }
    }

    private void testChangePart()
    {
        int id = UnityEngine.Random.Range(10100001, 10100031);
        UpdatePartInfo(ManagerData.Get<PartData>(id));
    }
    protected override void OnDestroy()
    {
        Instance = null;
    }
    private void setAllTextInChildren(GameObject go, string txt)
    {
        Text[] ts = go.GetComponentsInChildren<Text>();
        for (int i = 0; i < ts.Length; i++)
        {
            ts[i].text = txt;
        }
    }
    public void UpdateFocusPartsText()
    {
        List<Toggle> togs = new List<Toggle> { TogPart1, TogPart2, TogPart3, TogPart4 };
        var parts = Profile.Instance.CurrentDeviceData.GetFocusPartDatas();
        parts.ForEach(p =>
        {
            LegoPart lPart = LegoWorld.Instance.GetLegoPart(p.id);
            bool focusEnable = p.focusIndex == 1 ? true : lPart && !lPart.IsBinded;
            Transform trans = FrameDisable.transform.Find("Part" + p.focusIndex.ToString());
            if (trans)
            {
                setAllTextInChildren(trans.gameObject, p.partName);
            }
            setAllTextInChildren(togs[p.focusIndex - 1].gameObject, p.partName);
        });
    }
    public void ResetFocusPartEnable(bool isAllOpened)
    {
        if (isAllOpened)
        {
            var parts = Profile.Instance.CurrentDeviceData.GetFocusPartDatas();
            parts.ForEach(p =>
            {
                Transform trans = FrameDisable.transform.Find("Part" + p.focusIndex.ToString());
                if (trans)
                {
                    trans.gameObject.SetActive(false);
                }
            });
        }
        else
        {
            var parts = Profile.Instance.CurrentDeviceData.GetFocusPartDatas();
            parts.ForEach(p =>
            {
                Transform trans = FrameDisable.transform.Find("Part" + p.focusIndex.ToString());
                if (trans)
                {
                    trans.gameObject.SetActive(p.focusIndex != 1);
                }
            });
        }
    }
    public void UpdateFocusPartEnable(int index, bool enable)
    {
        Transform trans = FrameDisable.transform.Find("Part" + index.ToString());
        if (trans)
        {
            trans.gameObject.SetActive(!enable);
        }
    }

    public void ShowOrHideDebug()
    {
        // this.ToggleDebug.isOn = !this.ToggleDebug.isOn;
        GameObject go = transform.Find("Container/FrameDebug").gameObject;
        go.SetActive(!go.activeSelf);

        // ToggleDebug.isOn = !ToggleDebug.isOn;
    }
}
