﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
public class PanelExamResult : PanelExamResultBase
{
    private PanelExamResultModel model;
    public override void SetData(object data)
    {
        model = data as PanelExamResultModel;
        bool pass = model.IsPass() > 0;
        this.ImgFail.gameObject.SetActive(!pass);
        this.ImgPass.gameObject.SetActive(pass);
        this.TxtScore.text = model.GetScorePercent().ToString() + "%";
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnRetry.gameObject)
        {
            ManagerUI.Instance.Close(this);
            ManagerUI.Instance.Open<PanelExam>();
        }
        else if (go == this.BtnClose.gameObject)
        {
            ManagerUI.Instance.Close(this);
            ManagerUI.Instance.Close<PanelExam>();
            Main.Instance.Home();
        }
    }
}
