using System.Collections.Generic;
public class PanelExamResultChoice
{
    public int QuestionId;
    public Dictionary<int, bool> Choices = new Dictionary<int, bool>();
    public bool IsCorrect;

    public string GetSelectJson()
    {
        string str = "";
        foreach (KeyValuePair<int, bool> c in Choices)
        {
            if (c.Value)
            {
                str += c.Key.ToString() + ",";
            }
        }
        if (!string.IsNullOrEmpty(str))
            str = str.Substring(0, str.Length - 1);
        return QuestionId.ToString() + ":'" + str + "'";
    }
}