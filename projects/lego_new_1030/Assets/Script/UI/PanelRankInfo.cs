﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System;
public class PanelRankInfo : PanelRankInfoBase
{
    private List<DataRanking> rankList = new List<DataRanking>();
    private PanelRankInfoModel model;
    protected override void Awake()
    {
        base.Awake();
        ListRank.OnInitItem = initItem;
        ListRank.OnSelectItemButton = clickItemButton;
    }
    public override void SetData(object data)
    {
        base.SetData(data);
        model = data as PanelRankInfoModel;
        this.TxtEquipName.text = Profile.Instance.CurrentDeviceData.GetDisplayName();
        this.TxtTime.text = new TimeSpan(0, 0, Convert.ToInt32(model.useTime)).ToString();
        setModeImg();
        refresh();
    }
    private void setModeImg()
    {
        this.FrameMode.transform.Find("ImgDess").gameObject.SetActive(model.mode != 1);
        this.FrameMode.transform.Find("ImgAss").gameObject.SetActive(model.mode == 1);
    }
    private void onListData(object result)
    {
        rankList = result as List<DataRanking>;
        ListRank.SetItemCount(rankList.Count);
    }
    private void onChallengeResult(object result)
    {
        DataRanking userRank = result as DataRanking;
        this.TxtRank.text = (userRank != null && userRank.ranking > 0) ? userRank.ranking.ToString() : "0";
    }
    private void refresh()
    {
        new RpcRankingInfo(model.mode, onChallengeResult).Execute();
        new RpcRankingList(Profile.Instance.CurrentDeviceData, model.mode, onListData).Execute();
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnBack.gameObject)
        {
            ManagerUI.Instance.Close(this);
            Main.Instance.Home();
        }
        else if (go == this.BtnChai.gameObject)
        {
            model.mode = 0;
            refresh();
        }
        else if (go == this.BtnZhuang.gameObject)
        {
            model.mode = 1;
            refresh();
        }
        else if (go == this.BtnRetry.gameObject)
        {
            LegoWorld.Instance.StartUp(GameMode.Competition, model.mode == 1, rst =>
              {
                  ManagerUI.Instance.Close(this);
              });
        }
    }
    private void initItem(GameObject list, GameObject item, int index)
    {
        item.transform.Find("ImgIcon1").gameObject.SetActive(false);
        item.transform.Find("ImgIcon2").gameObject.SetActive(false);
        item.transform.Find("ImgIcon3").gameObject.SetActive(false);

        DataRanking dr = rankList[index];

        if (dr != null)
        {
            Transform rankIcon = item.transform.Find("ImgIcon" + (index + 1));
            if (rankIcon)
            {
                rankIcon.gameObject.SetActive(true);
            }
            Text TxtRank = item.transform.Find("TxtRank").GetComponent<Text>();
            Text TxtName = item.transform.Find("TxtName").GetComponent<Text>();
            Text TxtTime = item.transform.Find("TxtTime").GetComponent<Text>();
            TxtRank.text = (index + 1).ToString();
            TxtName.text = dr.userName;
            TxtTime.text = new TimeSpan(0, 0, Convert.ToInt32(dr.useTime)).ToString();
        }
    }
    private void clickItemButton(GameObject list, GameObject item, int index)
    {
        DataRanking dr = rankList[index];
        LegoWorld.Instance.StartUp(GameMode.Competition, model.mode == 1, rst =>
          {
              ManagerUI.Instance.Close(this);
              ManagerUI.Instance.Close<PanelMain>();
          }, dr.userNo, model.mode);
    }
}
