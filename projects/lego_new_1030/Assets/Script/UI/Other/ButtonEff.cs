using UnityEngine;
using DG.Tweening;
public class ButtonEff : GBase
{
    [SerializeField]
    private RectTransform m_Brother1;
    [SerializeField]
    private RectTransform m_Brother2;
    [SerializeField]
    private RectTransform m_Child1;
    [SerializeField]
    private RectTransform m_Child2;
    private Vector3 m_Pos1;
    private Vector3 m_Pos2;
    private bool m_IsOpen;
    private bool m_IsTweening;
    private float m_TweenTime = .3f;
    protected override void Awake()
    {
        m_IsOpen = false;
        m_IsTweening = false;
        m_Child1.gameObject.SetActive(false);
        m_Child2.gameObject.SetActive(false);
    }

    public void Trigger()
    {
        if (m_IsTweening) return;
        m_IsTweening = true;
        if (m_IsOpen)
        {
            m_Brother1.gameObject.SetActive(true);
            m_Brother2.gameObject.SetActive(true);
            m_Child1.position = m_Pos1;
            m_Child2.position = m_Pos2;
            m_Child1.gameObject.SetActive(false);
            m_Child2.gameObject.SetActive(false);
            m_IsOpen = false;
            m_IsTweening = false;
        }
        else
        {
            m_Brother1.gameObject.SetActive(false);
            m_Brother2.gameObject.SetActive(false);
            m_Child1.gameObject.SetActive(true);
            m_Child2.gameObject.SetActive(true);
            m_Pos1 = m_Child1.position;
            m_Pos2 = m_Child2.position;
            m_Child1.transform.DOMove(m_Brother1.position, m_TweenTime).SetEase(Ease.OutBack);
            m_Child2.transform.DOMove(m_Brother2.position, m_TweenTime).SetEase(Ease.OutBack).OnComplete(() =>
            {
                m_IsOpen = true;
                m_IsTweening = false;
            });

        }
    }
}