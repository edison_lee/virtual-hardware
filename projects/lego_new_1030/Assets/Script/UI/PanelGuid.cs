﻿using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using System;
public class PanelGuid : PanelGuidBase
{
    private Action onClose;
    public override void SetData(object data)
    {
        PanelGuidParam p = data as PanelGuidParam;
        this.FrameContainer.transform.GetChild((int)p.guidType).gameObject.SetActive(true);
        this.BtnOk.transform.GetChild(0).GetComponent<Image>().DOColor(Color.clear, .5f).SetLoops(int.MaxValue, LoopType.Yoyo).SetEase(Ease.Linear);
        onClose = p.onClose;
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        ManagerUI.Instance.Close(this);
        if (onClose != null)
        {
            onClose();
        }
    }
}
