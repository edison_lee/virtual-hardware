using System;
public class PanelGuidParam : Data
{
    public GuidPage guidType = GuidPage.Main;
    public Action onClose;
    public PanelGuidParam(GuidPage page, Action onClose)
    {
        this.guidType = page;
        this.onClose = onClose;
    }
}