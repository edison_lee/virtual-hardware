﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    public string deviceName = "X4";
    public GameObject deviceObject;
    private DeviceData deviceData;
    private LegoCam mainCamera;
    void Awake()
    {
        deviceData = ManagerData.GetDeviceDataByName(deviceName);
        GameObject prefab = Resources.Load<GameObject>("Prefab/Device/" + deviceData.GetPrefabName());
        deviceObject = GameObject.Instantiate(prefab);
        deviceObject.name = deviceObject.name.Replace("(Clone)","");
        mainCamera = GetComponent<LegoCam>();
        mainCamera.Target = deviceObject.transform;
    }
    public void PlayClip(int clipId)
    {
        new LegoClipAction(deviceData.ActionClips.Find(c => c.id == clipId), obj =>
        {
            Debug.Log("clip complete!");
        }).Execute();
    }
}
