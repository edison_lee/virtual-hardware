/*
给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。

示例:

输入: [-2,1,-3,4,-1,2,1,-5,4],
输出: 6
解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。
进阶:

如果你已经实现复杂度为 O(n) 的解法，尝试使用更为精妙的分治法求解。
 */

using System;
using UnityEngine;
public class Leetcode53 : LeetcodeExecution
{
    public void Execute()
    {
        int[] nums = new int[] { 1 };
        Debug.Log(MaxSubArray(nums));
    }
    public int MaxSubArray(int[] nums)
    {
        int max = int.MinValue;
        int lmax = 0;
        for (int i = 0; i < nums.Length; i++)
        {
            lmax = Math.Max(nums[i], lmax + nums[i]);
            max = Math.Max(lmax, max);
        }
        return max;
    }
}