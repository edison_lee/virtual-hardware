/*
给定字符串J 代表石头中宝石的类型，和字符串 S代表你拥有的石头。 S 中每个字符代表了一种你拥有的石头的类型，你想知道你拥有的石头中有多少是宝石。

J 中的字母不重复，J 和 S中的所有字符都是字母。字母区分大小写，因此"a"和"A"是不同类型的石头。

示例 1:

输入: J = "aA", S = "aAAbbbb"
输出: 3
示例 2:

输入: J = "z", S = "ZZ"
输出: 0
注意:

S 和 J 最多含有50个字母。
J 中的字符不重复。
 */

using UnityEngine;
using System.Collections.Generic;
public class Leetcode771 : LeetcodeExecution
{
    public void Execute()
    {
        // string J = "aA", S = "aAAbbbb";
        string J = "z", S = "ZZ";

        int result = NumJewelsInStones(J, S);

        Debug.Log(result);
    }
    public int NumJewelsInStones(string J, string S)
    {
        int result = 0;
        Dictionary<char, bool> map = new Dictionary<char, bool>();
        foreach (var j in J)
            map.Add(j, true);
        foreach (var s in S)
            if (map.ContainsKey(s))
                result++;
        return result;
    }
}