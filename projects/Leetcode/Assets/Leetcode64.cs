/*
给定一个包含非负整数的 m x n 网格，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。

说明：每次只能向下或者向右移动一步。

示例:

输入:
[
  [1,3,1],
  [1,5,1],
  [4,2,1]
]
输出: 7
解释: 因为路径 1→3→1→1→1 的总和最小。
 */

using System;
using UnityEngine;
public class Leetcode64 : LeetcodeExecution
{
    public void Execute()
    {
        int[,] grid = {
            { 1, 3, 1 },
            { 1, 5, 1 },
            { 4, 2, 1 }
            };
        Debug.Log(MinPathSum(grid));
    }
    public int MinPathSum(int[,] grid)
    {
        int row = grid.GetLength(0);
        int col = grid.GetLength(1);
        int r = 1;
        int c = 1;
        int sumu;
        int suml;

        //计算第一行和第一列
        for (; r < row; r++)
            grid[r, 0] += grid[r - 1, 0];
        for (; c < col; c++)
            grid[0, c] += grid[0, c - 1];

        //计算其他
        for (r = 1; r < row; r++)
            for (c = 1; c < col; c++)
            {
                suml = grid[r, c - 1] + grid[r, c];
                sumu = grid[r - 1, c] + grid[r, c];
                grid[r, c] = Math.Min(suml, sumu);
            }
        return grid[row - 1, col - 1];
    }
}