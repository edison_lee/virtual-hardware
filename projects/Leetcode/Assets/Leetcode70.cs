﻿/*
假设你正在爬楼梯。需要 n 步你才能到达楼顶。

每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？

注意：给定 n 是一个正整数。

示例 1：

输入： 2
输出： 2
解释： 有两种方法可以爬到楼顶。
1.  1 步 + 1 步
2.  2 步
示例 2：

输入： 3
输出： 3
解释： 有三种方法可以爬到楼顶。
1.  1 步 + 1 步 + 1 步
2.  1 步 + 2 步
3.  2 步 + 1 步
 */

using System;
using UnityEngine;
public class Leetcode70 : LeetcodeExecution
{
    public void Execute()
    {
        Debug.Log(ClimbStairs(44));
    }

    public int ClimbStairs(int n)
    {
        if(n<3) return n;
        int m1 = 2;		//最后一步走的是1步的走法
        int m2 = 1;		//最后一步走的是2步的走法
        int total = 0;	//每个台阶的所有走法 = m1 + m2
        for (int stair = 3; stair <= n; stair++)
        {
            total = m1 + m2;
            m2 = m1;
            m1 = total;
        }
        return total;
    }
}