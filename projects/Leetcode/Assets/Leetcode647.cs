/*
给定一个字符串，你的任务是计算这个字符串中有多少个回文子串。

具有不同开始位置或结束位置的子串，即使是由相同的字符组成，也会被计为是不同的子串。

示例 1:

输入: "abc"
输出: 3
解释: 三个回文子串: "a", "b", "c".
示例 2:

输入: "aaa"
输出: 6
说明: 6个回文子串: "a", "a", "a", "aa", "aa", "aaa".
注意:

输入的字符串长度不会超过1000。
 */

using System;
using UnityEngine;
public class Leetcode647 : LeetcodeExecution
{
    public void Execute()
    {
        Debug.Log(CountSubstrings("aaa"));
    }
    public int CountSubstrings(string s)
    {
        int result = 1;
        int begin, end;
        bool flag;
        for (int i = 1; i < s.Length; i++)
            for (int j = 0; j <= i; j++)
            {
                begin = j;
                end = i;
                flag = true;
                for (; begin <= end; begin++, end--)
                    if (!s[begin].Equals(s[end]))
                    {
                        flag = false;
                        break;
                    }
                if (flag)
                    result++;
            }
        return result;
    }
    private bool isSubstrings(string s, int begin, int end)
    {
        for (; begin <= end; begin++, end--)
            if (!s[begin].Equals(s[end]))
                return false;
        return true;
    }
}
