﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class Main : MonoBehaviour
{
    public int number = 771;
    void Start()
    {
        Type t = Type.GetType("Leetcode" + number.ToString());
        LeetcodeExecution exp = Activator.CreateInstance(t) as LeetcodeExecution;
        exp.Execute();
    }
}
