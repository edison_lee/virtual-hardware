﻿using UnityEngine;

[ExecuteInEditMode]
public class GLimitedList : GListBase {

    [SerializeField]
    private int _Count = 1;
    [SerializeField]
    private int _Space = 1;
    [SerializeField]
    private GUnlimitedList.Direction _Direction = GUnlimitedList.Direction.Horizontal;
    [SerializeField]
    private Align _Align = Align.Center;
    [SerializeField]
    private RectTransform _Item;
    [SerializeField]
    private bool _Generate = false;
    [SerializeField]
    private bool _ResetBound = false;

    private GUnlimitedList.Direction _OldDirection = GUnlimitedList.Direction.Horizontal;

    private Align _OldAlign = Align.Center;
    private Vector2 m_Size { get { return _Item.sizeDelta; } }

//     public void SetItemCount(int count)
//     {
//         this._Count = count;
//         this.generate();
//     }
//     public void SetDirection(int dir)
//     {
//         this._Direction = (GUnlimitedList.Direction)dir;
//     }
//     public void SetAlign(int align)
//     {
//         this._Align = (Align)align;
//     }

//     private void generate()
//     {
//         if (_Count < 0)
//         {
//             Debug.Log("数量必须为正整数。");
//             return;
//         }

//         if (_Item == null) 
//         {
//             _Item = Trans.GetChild(0) as RectTransform;
//             if (_Item == null)
//             {
//                 Debug.Log("请为Item制定一个模板！");
//                 return;
//             }
//             _Item.gameObject.SetActive(false);
//         }

//         _Item.gameObject.SetActive(false);

//         Util.DestroyChildren(transform, false, _Item.gameObject);
//         updateBound();
//         for (int i = 0; i < this._Count; i++)
//             createOne(i);
//     }
//     private void createOne(int index)
//     {
//         Vector2 pos = _Direction == GUnlimitedList.Direction.Horizontal ?
//                 new Vector2(index * (m_Size.x + _Space), 0) :
//                 new Vector2(0, -index * (m_Size.y + _Space));

//         RectTransform item = CUnityUtil.Instantiate(_Item) as RectTransform;
//         if (item.gameObject.activeSelf == false)
//             item.gameObject.SetActive(true);
//         item.SetParent(transform, false);
//         item.anchorMax = Vector2.up;
//         item.anchorMin = Vector2.up;
//         item.pivot = Vector2.up;
//         item.name = "item" + index;
//         item.anchoredPosition = pos;

//         GListItem itemCon = item.GetComponent<GListItem>();
//         if (itemCon == null)
//             itemCon = item.gameObject.AddComponent<GListItem>();
//         itemCon.OnItemClick = this.onItemSelect;
//         itemCon.UpdateItem(index);
//         if (itemCon.gameObject.activeSelf)
//         {
//             if (this.OnInitItem != null)
//                 this.OnInitItem(this.gameObject, itemCon.gameObject, index);
//         }
//         itemCon.OnItemClickButton = (go, idx) =>
//         {
//             if (this.OnSelectItemButton != null)
//                 this.OnSelectItemButton(this.gameObject, go, idx);
//         };
//     }
//     private void updateBound()
//     {
//         RectTransform rect = transform as RectTransform;
//         Vector2 align = Vector2.zero;
//         if (_Align == Align.Left)
//         {
//             align = new Vector2(0f, 0.5f);
//         }
//         else if(_Align == Align.Top)
//         {
//             align = new Vector2(0.5f, 1f);
//         }
//         else if (_Align == Align.Right)
//         {
//             align = new Vector2(1f, 0.5f);
//         }
//         else if (_Align == Align.Bottom)
//         {
//             align = new Vector2(0.5f, 0f);
//         }
//         else if (_Align == Align.Center)
//         {
//             align = Vector2.one * 0.5f;
//         }
//         rect.pivot = align;
//         //rect.anchorMax = rect.anchorMin = rect.pivot = Vector2.up;

//         //Vector2 m_Size = m_Item.sizeDelta;
//         float xx, yy;
//         int space = _Space * (_Count - 1);
//         space = Mathf.Max(space, 0);
//         if (this._Direction == GUnlimitedList.Direction.Horizontal)
//         {
//             xx = m_Size.x * _Count + space;
//             yy = m_Size.y;
//         }
//         else
//         {
//             xx = m_Size.x;
//             yy = m_Size.y * _Count + space;
//         }

//         rect.sizeDelta = new Vector2(xx, yy);
//     }

// #if UNITY_EDITOR && ART_USE
//     void Update()
//     {
//         if (_Generate)
//         {
//             _Generate = false;
//             generate();
//         }
//         if (_ResetBound)
//         {
//             _ResetBound = false;
//             updateBound();
//         }
//         if (_Direction != _OldDirection) 
//         {
//             generate();
//             _OldDirection = _Direction;
//         }
//         if (_Align != _OldAlign) 
//         {
//             generate();
//             _OldAlign = _Align;
//         }

// 	}

// #endif

// 	public void SetItem(RectTransform item)
//     {
//         _Item = item;
//     }

//     private void onItemSelect(GameObject itemGo, int index)
//     {
//         if (this.OnSelectItem != null)
//             this.OnSelectItem(this.gameObject, itemGo, index);
//     }
}
