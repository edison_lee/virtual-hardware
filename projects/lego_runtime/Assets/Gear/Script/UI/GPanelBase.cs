using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(GPanelSetting))]
public class GPanelBase : GBase
{
    private GPanelSetting m_Setting;
    public GPanelSetting Setting
    {
        get
        {
            if (!m_Setting)
            {
                m_Setting = GetComponent<GPanelSetting>();
            }
            return m_Setting;
        }
    }

    public bool IsShowing()
    {
        return isActiveAndEnabled;
    }

    //初始化界面的game object,一个game object只会初始化一次，初始化信息来自panel setting
    public void Create(Transform root)
    {
        gameObject.name = gameObject.name.Replace("(Clone)", "");
        transform.SetParent(root, false);
        Rect.anchorMin = Vector2.zero;
        Rect.anchorMax = Vector2.one;
        Rect.pivot = Vector2.one * .5f;
        Rect.offsetMax = Vector2.zero;
        Rect.offsetMin = Vector2.zero;
        transform.localPosition = Vector3.zero;

        Canvas cvs = GetComponent<Canvas>();
        if (cvs == null)
        {
            cvs = gameObject.AddComponent<Canvas>();
        }
        cvs.overrideSorting = true;
        cvs.sortingOrder = Setting.GetRealLayer();

        GraphicRaycaster raycaster = GetComponent<GraphicRaycaster>();
        if (raycaster == null)
        {
            raycaster = gameObject.AddComponent<GraphicRaycaster>();
        }
    }
    public void Open(object data)
    {
        if (!IsActive())
        {
            gameObject.SetActive(true);
        }
        SetData(data);
    }
    public void Close()
    {
        if (Setting.IsHideMode)
        {
            gameObject.SetActive(false);
            OnClose();
        }
        else
        {
            GameObject.Destroy(gameObject);
        }
    }
    protected override void OnDestroy()
    {
        OnClose();
    }
    protected virtual void OnClose()
    {
        // to be overrided
    }
}