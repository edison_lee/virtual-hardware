﻿using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class GRunningTip : GBase
{
    private static string _DefaultFontPath = "Assets/Outputs/Interfaces/Fonts/UIFONT.TTF";
    private static Font _DefaultFont;
    private static ObjectPool<GameObject> _TipPool = new ObjectPool<GameObject>(null, o => o.SetActive(false), m => Destroy(m), 5);
    private GRunningTipModel _Model;
    public override void SetData(object data)
    {
        _Model = data as GRunningTipModel;
        if (!IsActive())
            gameObject.SetActive(true);
        Rect.SetParent(GRunningTipModel.TipRoot, false);
        Rect.anchoredPosition3D = _Model.GetStartPos();
        Rect.anchorMax = Rect.anchorMin = Rect.pivot = Vector2.one * 0.5f;
        Text txt = gameObject.GetComponent<Text>();
        if (txt == null)
            txt = gameObject.AddComponent<Text>();
        txt.raycastTarget = false;
        if (_Model._Font)
            txt.font = _Model._Font;
        txt.fontSize = _Model._FontSize;
        txt.text = _Model.GetHtmlText();

        Outline outline = gameObject.GetComponent<Outline>();
        if (outline == null)
            outline = gameObject.AddComponent<Outline>();
        Color c;
        if (ColorUtility.TryParseHtmlString("#746307", out c))
        {
            outline.effectColor = c;
        }

        ContentSizeFitter fitter = gameObject.GetComponent<ContentSizeFitter>();
        if (fitter == null)
            fitter = gameObject.AddComponent<ContentSizeFitter>();
        fitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
        fitter.verticalFit = _Model._MultyLine ? ContentSizeFitter.FitMode.PreferredSize : ContentSizeFitter.FitMode.Unconstrained;

        Canvas cvs = gameObject.GetComponent<Canvas>();
        if (cvs == null)
        {
            cvs = gameObject.AddComponent<Canvas>();
        }
        cvs.sortingOrder = 99;
        Invoke("DoRun", _Model._Delay);
    }
    private void DoRun()
    {
        Graphic graphic = GetComponent<Graphic>();
        Color newColor = graphic.color;
        newColor.a = 0f;
        graphic.CrossFadeColor(newColor, _Model._Duration * .7f, true, true);
        Rect.DOMoveY(Rect.anchoredPosition.y + _Model._Offset, _Model._Duration).OnComplete(TweenComplete).SetEase(_Model._EaseType);
    }
    private void TweenComplete()
    {
        _TipPool.Release(gameObject);
    }
    public static void Show(string text, bool useDefaultPos = true, Vector2 pos = default(Vector2), string color = "#FCD70F", float offset = 3f, float delay = 1.6f, float duration = 1f, Ease easeType = Ease.InQuint, Font mFont = null, int fontSize = 28, bool multyLine = true)
    {
        GRunningTipModel model = new GRunningTipModel();
        model._UseDefaultPos = useDefaultPos;
        model._Pos = pos;
        model._Text = text;
        model._Color = color;
        model._MultyLine = multyLine;
        model._Offset = offset;
        model._Delay = delay;
        model._Duration = duration;
        model._FontSize = fontSize;
        model._EaseType = easeType;

        Action<Font> showAction = font =>
        {
            if (font)
            {
                model._Font = font;
                Show(model);
            }

        };

        if (mFont == null)
        {
            if (_DefaultFont == null)
            {
                showAction(Resources.Load<Font>("ZZGFJH"));
                // CAssetBundleManager.AsyncLoadResource(_DefaultFontPath, font_obj =>
                // {
                //     _DefaultFont = font_obj as Font;
                //     if (_DefaultFont == null)
                //     {
                //         Debug.LogWarning("the default font is null.");
                //         return;
                //     }
                //     showAction(_DefaultFont);
                // });
            }
            else
            {
                showAction(_DefaultFont);
            }
        }
        else
        {
            showAction(mFont);
        }
    }

    public static void Show(GRunningTipModel model)
    {
        if (model != null && model.IsValid())
        {
            GameObject obj = _TipPool.Get();
            obj.name = "_running_text";
            obj.layer = LayerMask.NameToLayer("UI");

            // Canvas cvs = obj.GetComponent<Canvas>();
            // if (!cvs)
            // {

            // }

            GRunningTip tip = obj.GetComponent<GRunningTip>();
            if (tip == null)
                tip = obj.AddComponent<GRunningTip>();
            tip.SetData(model);

            if (obj.GetComponent<Canvas>() == null)
            {
                var cvs = obj.AddComponent<Canvas>();
                cvs.overrideSorting = true;
                cvs.sortingOrder = 999;
            }
        }
    }
}