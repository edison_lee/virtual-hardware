﻿using UnityEngine;
using UnityEngine.EventSystems;

public abstract class GBase : UIBehaviour, IData
{
    private object m_Data;
    private Transform m_Trans;
    private RectTransform m_Rect;

    public object Data
    {
        get
        {
            return m_Data;
        }
    }

    public Transform Trans
    {
        get
        {
            if (m_Trans == null)
                m_Trans = transform;
            return m_Trans;
        }
    }

    public RectTransform Rect
    {
        get
        {
            if (m_Rect == null)
                m_Rect = Trans as RectTransform;
            if (m_Rect == null)
                m_Rect = gameObject.AddComponent<RectTransform>();
            return m_Rect;
        }
    }

    protected override void Awake()
    {

    }
    protected override void Start()
    {
        // to be override
    }
    protected override void OnEnable()
    {
        // to be override 
    }
    protected override void OnDisable()
    {
        //to be override
    }
    protected override void OnDestroy()
    {
        //to be override
    }

    public virtual void SetData(object data)
    {
        m_Data = data;
    }

    public virtual void OnButtonClickHandler(GameObject go)
    {
    }
    public virtual void OnButtonSelectHandler(GameObject go)
    {
    }
}
