﻿using UnityEngine;
using UnityEngine.UI;

public class GButtonColor : GButton
{

    private Graphic[] _ChildGraphics;

    [SerializeField]
    private Color _HightLightColor = new Color32(0, 0, 0, 50);
    [SerializeField]
    private bool _UseAlpha = false;
    protected override void Awake()
    {
        base.Awake();
        _ChildGraphics = GetComponentsInChildren<Graphic>();
    }
    public override void ButtonDownEffect()
    {
        StartChildrenColorTween(colors.pressedColor, false);
    }
    public override void ButtonUpEffect()
    {
        StartChildrenColorTween(colors.normalColor, false);
    }
    public override void ButtonSelectEffect()
    {
        StartChildrenColorTween(_HightLightColor, false);
    }
    public override void ButtonDeselectEffect()
    {
        StartChildrenColorTween(colors.normalColor, false);
    }
    private void StartChildrenColorTween(Color target, bool instant)
    {
        if (_ChildGraphics == null || _ChildGraphics.Length <= 0) return;
        for (var i = 0; i < _ChildGraphics.Length; i++)
        {
            Graphic graphic = _ChildGraphics[i];
            if (graphic == null) continue;
            graphic.CrossFadeColor(target, instant ? 0f : colors.fadeDuration, true, _UseAlpha);
        }
    }
}
