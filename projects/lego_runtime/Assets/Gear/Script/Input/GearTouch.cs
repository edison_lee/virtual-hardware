using UnityEngine;
public abstract class GearTouch
{
    private Touch[] m_Touches;
    protected abstract void TouchEntry();
    protected abstract void TouchTick();
    protected abstract void TouchExit();
}