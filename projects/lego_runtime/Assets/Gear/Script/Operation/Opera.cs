﻿using UnityEngine;
using System.Collections;
using System;

public class Opera : IOpera
{
    private CoroutineHolder m_CoroutineGo;
    public CoroutineHolder CoroutineGo
    {
        get
        {
            string hodlerName = "__CoroutineHolder";
            if (!m_CoroutineGo)
            {
                GameObject go = GameObject.Find(hodlerName);
                if (!go)
                {
                    go = new GameObject(hodlerName);
                }
                m_CoroutineGo = go.GetComponent<CoroutineHolder>();
                if (!m_CoroutineGo)
                    m_CoroutineGo = go.AddComponent<CoroutineHolder>();
            }
            return m_CoroutineGo;
        }
    }
    public Action<object> completeHandler;
    public Action<object> localHandler;
    public object param = null, result = null;
    public Opera(Action<object> completeHandler)
    {
        localHandler = completeHandler;
    }
    public virtual void Execute()
    {
    }
    public virtual void End()
    {
        if (localHandler != null)
            localHandler(result);
        if (completeHandler != null)
            completeHandler(result);
    }
    public virtual void Destroy()
    {
        completeHandler = null;
        localHandler = null;
    }
}