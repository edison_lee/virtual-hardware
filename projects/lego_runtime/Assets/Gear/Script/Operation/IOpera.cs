﻿public interface IOpera
{
    void Execute();
    void End();
}
