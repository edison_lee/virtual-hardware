﻿using UnityEngine;
using System.Collections;
using System;
public class FuctionOpera : Opera {

    private Action<object> function;
    private object data;
	public FuctionOpera(Action<object> function, object data, Action<object> compHandler=null):base(compHandler)
    {
        this.function = function;
        this.data = data;
    }

    public override void Execute()
    {
        base.Execute();
        if (function != null)
            function(data);
        End();
    }
}
