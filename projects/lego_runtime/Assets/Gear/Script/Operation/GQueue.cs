﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class GQueue : Opera
{
    private Queue<Opera> m_Queue = new Queue<Opera>();
    private bool m_AutoExeute;
    public bool AutoExecute { get { return m_AutoExeute; } set { m_AutoExeute = value; } }
    private bool m_IsWaiting = true;
    public bool IsWaiting { get { return m_IsWaiting; } }
    private Opera current;
    public Opera Current { get { return current; } }
    private int Count { get { return m_Queue.Count; } }
    public GQueue(List<Opera> queue, bool autoExecute = false, Action<object> completeHandler = null)
        : base(completeHandler)
    {
        this.m_Queue = new Queue<Opera>(queue);
        this.m_AutoExeute = autoExecute;
        if (m_AutoExeute)
        {
            Execute();
        }
    }
    public override void Execute()
    {
        if (m_Queue.Count > 0)
        {
            current = m_Queue.Dequeue();
            current.localHandler = rst => Execute();
            current.Execute();
            m_IsWaiting = false;
        }
        else
        {
            End();
            m_IsWaiting = true;
        }
    }
    public void Enqueue(Opera opera)
    {
        m_Queue.Enqueue(opera);
        if (m_AutoExeute)
        {
            if (m_IsWaiting)
            {
                Execute();
            }
        }
    }
    public override void Destroy()
    {
        base.Destroy();
        current.Destroy();
        foreach (Opera o in m_Queue)
            o.Destroy();

        m_IsWaiting = true;
    }
}
