using UnityEngine;
using System.Collections;
using System;
public class TimerOpera : Opera
{
    private float m_CurrentSecondsInt;
    private float m_CurrentSecondsFloat;
    private float m_TimeSeconds;
    private Action m_SecondsTickFunc;
    private Action<float> m_FrameTickFunc;
    private bool m_IsPausing = false;
    public TimerOpera(float timeSeconds, Action secondsTickFunc = null, Action<float> frameTickFunc = null, Action<object> compHandler = null) : base(compHandler)
    {
        m_TimeSeconds = timeSeconds;
        m_SecondsTickFunc = secondsTickFunc;
        m_FrameTickFunc = frameTickFunc;
    }
    public override void Execute()
    {
        m_CurrentSecondsFloat = 0f;
        m_CurrentSecondsInt = 0;
        CoroutineGo.AddTicker(Tick);
    }
    private void Tick(float delta)
    {
        if (m_IsPausing) return;
        m_CurrentSecondsFloat += delta;
        if (m_FrameTickFunc != null)
            m_FrameTickFunc(delta);

        int intSeconds = Mathf.FloorToInt(m_CurrentSecondsFloat);
        if (intSeconds > m_CurrentSecondsInt)
        {
            m_CurrentSecondsInt = intSeconds;
            if (m_SecondsTickFunc != null)
                m_SecondsTickFunc();
        }

        if (m_CurrentSecondsFloat >= m_TimeSeconds)
        {
            End();
            Destroy();
        }
    }
    public void Pause()
    {
        m_IsPausing = true;
    }
    public void Continue()
    {
        m_IsPausing = false;
    }
    public override void Destroy()
    {
        base.Destroy();
        CoroutineGo.RemoveTicker(Tick);
        m_CurrentSecondsInt = 0;
        m_CurrentSecondsFloat = 0f;
        m_SecondsTickFunc = null;
        m_FrameTickFunc = null;
    }
}
