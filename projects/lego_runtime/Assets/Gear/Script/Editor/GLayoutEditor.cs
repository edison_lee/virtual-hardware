﻿
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(GLayout), true)]
[CanEditMultipleObjects]
public class GLayoutEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        if (GUILayout.Button("预览"))
        {
            GLayout l = target as GLayout;
            l.LayoutChange();
        }
        serializedObject.ApplyModifiedProperties();
    }
}