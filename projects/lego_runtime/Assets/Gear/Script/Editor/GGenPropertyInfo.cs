using System;
public class GGenPropertyInfo
{
    public Type PType;
    public string PName;
    public string PPath;
    public GGenPropertyInfo(Type type, string name, string path)
    {
        PType = type;
        PName = name;
        PPath = path;
    }
}