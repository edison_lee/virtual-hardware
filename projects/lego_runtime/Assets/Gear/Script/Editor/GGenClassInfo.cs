using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;
public class GGenClassInfo
{
    string BasePanelClassCode = @"
using UnityEngine.UI;

public class {0} : GPanelBase 
{
{1}
    protected override void Awake()
    {
        base.Awake();
{2}
    }
}
";

    string PanelClassCode = @"
using UnityEngine;

public class {0} : {0}Base
{
    public override void OnButtonClickHandler(GameObject go)
    {
    }
}
";
    private GameObject m_PanelRoot;
    private GameObject[] m_Properties;
    private string m_ClassName;
    public List<GGenPropertyInfo> Properties = new List<GGenPropertyInfo>();

    public GGenClassInfo(string className)
    {
        m_ClassName = className;
    }

    public void AddProperty(Type type, string name, string path)
    {

    }

    public void Generate()
    {

    }
}