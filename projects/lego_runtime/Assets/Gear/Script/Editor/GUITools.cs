﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;
public static class GUITools
{
    public static Dictionary<Type, int> Priorities = new Dictionary<Type, int>();
    static GUITools()
    {
        Priorities[typeof(Graphic)] = 100;
        Priorities[typeof(Selectable)] = 200;
        Priorities[typeof(GBase)] = 300;
    }

    [MenuItem(Configs.MenuPathUIGenerate)]
    public static void Execute()
    {
        // Stack<string> stack = new Stack<string>();
        // stack.Push("c");
        // stack.Push("b");
        // stack.Push("a");
        // Debug.Log(string.Join("/",stack.ToArray()));
        // return;
        //check directories
        string dirCode = Application.dataPath + Configs.UIGeneratePathCode;
        if (!Directory.Exists(dirCode))
        {
            Debug.Log("Directory Created : " + dirCode);
            Directory.CreateDirectory(dirCode);
        }
        string dirPrefab = Application.dataPath + Configs.UIGeneratePathPrefab;
        if (!Directory.Exists(dirPrefab))
        {
            Debug.Log("Directory Created : " + dirPrefab);
            Directory.CreateDirectory(dirPrefab);
        }

        GameObject panel = GameObject.FindGameObjectWithTag(Configs.TagPanel);
        if (!panel)
        {
            Debug.Log("only one could be generated at the same time.");
            return;
        }

        List<Transform> properties = new List<Transform>();
        panel.GetComponentsInChildren<Transform>(true, properties);
        // properties.ForEach(p =>
        // {
        //     if (p.CompareTag(Configs.TagProperty))
        //     {
        //         Debug.Log(p.name);
        //     }
        // });
        
        properties = properties.FindAll(p => p.CompareTag(Configs.TagProperty));

        if (properties.Count <= 0)
        {
            Debug.Log("there isn't panel to be generate!");
            return;
        }

        GUICodeGenerator.GeneratePanel(panel, properties.ToArray());

    }
}
