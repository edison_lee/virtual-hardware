﻿// using UnityEngine;
// using UnityEditor;
// using System.Collections;
// using System;
// //using System.Collections;
// using System.Collections.Generic;
// //using System.Reflection;
// using UnityEngine.EventSystems;
// using UnityEngine.UI;

// /// <summary>
// /// 生成prefab和代码
// /// </summary>
// static public class Export {
//     public class Property
//     {
//         public Type type;
//         public string name;
//         public string path;
//         public Property(Type t, string n, string p)
//         {
//             type = t;
//             name = n;
//             path = p;
//         }
//     }

//     public static Dictionary<Type, int> PRIORITY = new Dictionary<Type, int>();

//     delegate Util.IterationArguments UnitProcess(Transform unit, string path);

//     private static string CurrentRoot;

//     private static Transform root;

//     private static List<Property> properties;

//     private static List<Property> listProperties;

//     private static string CurrentUIClassName;

//     static Export()
//     {
//         //UGUI原生组件
//         PRIORITY[typeof(RawImage)] = 9;
//         PRIORITY[typeof(Image)] = 9;
//         PRIORITY[typeof(Text)] = 9;
//         PRIORITY[typeof(Button)] = 10;
//         PRIORITY[typeof(Scrollbar)] = 10;
//         PRIORITY[typeof(Slider)] = 10;
//         PRIORITY[typeof(Toggle)] = 10;
//         PRIORITY[typeof(InputField)] = 10;
//         //自己写的组件
//         PRIORITY[typeof(GButton)] = 20;
//         PRIORITY[typeof(GElasticButton)] = 20;
//         PRIORITY[typeof(GElasticColorButton)] = 20;
//         PRIORITY[typeof(GUnlimitedList)] = 21;
//         PRIORITY[typeof(GLimitedList)] = 21;
//         PRIORITY[typeof(GViewGroup)] = 22;

//         PRIORITY[typeof(GStar)] = 23;
//     }

//     [MenuItem("Frame/ExportUI")]
//     public static void ExportUI()
//     {
//         properties = new List<Property>();
//         listProperties = new List<Property>();
//         Util.IterateChild(GameObject.Find(Config.UIHierarchyRoot).transform, Config.UIHierarchyRoot, unitProcessor);

//         if (root != null)
//             generateUI();
//     }

//     private static Util.IterationArguments unitProcessor(Transform unit, string path)
//     {
//         bool result = unit.CompareTag("_UI_");
//         if (result)
//         {
//             root = unit;
//             CurrentRoot = path;
//             CurrentUIClassName = root.name;
//         }
//         return result ? Util.IterationArguments.StopAll : Util.IterationArguments.Continue;
//     }

//     private static void generateUI()
//     {
//         Util.IterateChild(root, "", generateProperty);

//         generateUICodeAndPrefab(root.name);
//     }
//     private static UIBehaviour hasList(UIBehaviour[] behavirs)
//     {
//         foreach(UIBehaviour bhv in behavirs)
//         {
//             if (bhv.GetType() == typeof(GUnlimitedList) || bhv.GetType()==typeof(GLimitedList))
//                 return bhv;
//         }
//         return null;
//     }
//     private static Util.IterationArguments generateProperty(Transform unit, string path)
//     {

//         if (unit.CompareTag("_Property_"))
//         {
//             UIBehaviour[] behaviours = unit.GetComponents<UIBehaviour>();
//             UIBehaviour list = hasList(behaviours);
//             if (list!=null)
//             {
//                 Debug.Log("this is a list " + list.name);
//                 properties.Add(new Property(list.GetType(), unit.name, path));
//                 generateListCode(unit);
//                 return Util.IterationArguments.StopAll;
//             }
//             else
//             {
//                 Type type = null;
//                 int last = 1;
//                 foreach (UIBehaviour uib in behaviours)
//                 {
//                     Type current = uib.GetType();
//                     if (isUIClassOrSubClass(current))
//                     {
//                         int cur = getPriority(current);
//                         if (cur > last)
//                         {
//                             type = current;
//                             last = cur;
//                         }
//                     }
//                 }
//                 properties.Add(new Property(type == null ? typeof(GameObject) : type, unit.name, path));
//             }
//         }
//         return Util.IterationArguments.Continue;
//     }
//     private static bool isUIClassOrSubClass(Type t)
//     {
//         foreach (KeyValuePair<Type, int> pair in PRIORITY)
//         {
//             if (t == pair.Key || t.IsSubclassOf(pair.Key))
//                 return true;
//         }
//         return false;
//     }
//     private static int getPriority(Type t)
//     {
//         foreach (KeyValuePair<Type, int> pair in PRIORITY)
//         {
//             if (t == pair.Key || t.IsSubclassOf(pair.Key))
//                 return pair.Value;
//         }
//         return 0;
//     }
//     private static void generateUICodeAndPrefab(string className)
//     {
//         string code = "";
//         code += "using UnityEngine;\n";
//         code += "using UnityEngine.UI;\n";
//         //code += "using System.Collections;\n";
//         //code += "using System.Collections.Generic;\n";
//         code += "\n";
//         code += "public class " + className + "Base : GPanelBase \n{";
//         code += "\n";
//         //变量
//         foreach (Property property in properties)
//         {
//             code += "    public " + property.type.Name + " " + property.name + ";\n";
//         }
//         //函数Initialize
//         code += "\n";
//         code += "    protected override void Awake()\n";
//         code += "    {\n";
//         code += "        base.Awake();\n";
//         foreach (Property property in properties)
//         {
//             string str = property.path;
//             code += "        this." + property.name + " = " + "Util.FindObject<" + property.type + ">(transform,\"" + property.path + "\");\n";
//             if (property.type == typeof(Button) || property.type == typeof(GButton) || property.type == typeof(GElasticButton) || property.type == typeof(GElasticColorButton))
//             {
//                 code += "        this." + property.name + ".OnClick = this.OnButtonClickHandler;\n";
//             }
//             else if (property.type == typeof(GLimitedList) || property.type == typeof(GUnlimitedList))
//             {
//                 code += "        this." + property.name + ".ItemType = typeof(" + getCurrentItemClassName(className, property.name) + ");\n";
//             }
//         }
//         code += "    }\n";
//         code += "}";
//         string path = Config.UIClassPath+"Base/";
//         WriteClass(code, path, className+"Base");

//         ////////////////////////////////////////////////////UI Class/////////////////////////////////////////////////

//         path = Config.UIClassPath;

//         if (!System.IO.File.Exists(path + className + ".cs"))
//         {
//             code = "";
//             code += "using UnityEngine;\n";
//             code += "using UnityEngine.UI;\n";
//             code += "using System.Collections;\n";
//             code += "using System.Collections.Generic;\n";
//             code += "\n";
//             code += "public class " + className + " : " + className + "Base \n{";
//             code += "\n";
//             //函数Initialize
//             code += "\n";
//             code += "    protected override void Awake()\n";
//             code += "    {\n";
//             code += "        base.Awake();\n";
//             code += "    }\n";
//             //函数ButtonClickHandler
//             code += "\n";
//             code += "    public override void OnButtonClickHandler(GameObject go)\n";
//             code += "    {\n";
//             code += "        base.OnButtonClickHandler(go);\n";
//             code += "    }\n";
//             code += "}";
//             WriteClass(code, path, className);
//         }
        
//         ////////////////////////////////////////////////////Create prefab/////////////////////////////////////////////////
//         GameObject uigo = GameObject.Find(Config.UIHierarchyRoot + className);
//         GameObject prefab = PrefabUtility.CreatePrefab(Config.UIPrefabPath + className + ".prefab", uigo, ReplacePrefabOptions.Default);
//         GameObject.DestroyImmediate(uigo);
//         Debug.Log("生成完毕！");
//         AssetDatabase.Refresh();
//         AssetDatabase.SaveAssets();
//     }


//     private static void generateList(Transform listRoot,string listPath)
//     {
//         Transform item = listRoot;
//         Transform grid = null;
//         string itemPath = listPath;
//         for (int i = 0; i < 3; i++)
//         {
//             item = item.GetChild(0);
//             itemPath = itemPath + item.name + "/";

//             if (i == 1)
//                 grid = item;
//         }

//         if(grid!=null)
//         {
//             GridLayoutGroup layout = grid.GetComponent<GridLayoutGroup>();
//             if(layout==null)
//                 layout = grid.gameObject.AddComponent<GridLayoutGroup>();
//             RectTransform itemRect = item.GetComponent<RectTransform>();
//             //layout.padding;
//             layout.cellSize = itemRect.sizeDelta;
//             layout.spacing = new Vector2(5,5);
//         }

//         Util.IterateChild(item, itemPath, generateListProperty);

//         generateListCodeAndPrefab(item, getCurrentItemClassName(root.name, listRoot.name));
//     }

//     private static Util.IterationArguments generateListProperty(Transform unit, string path)
//     {
//         Debug.Log(path);
//         if (unit.tag.Equals("_Property_"))
//         {
//             Type type = null;
//             UIBehaviour[] behaviours = unit.GetComponents<UIBehaviour>();

//             if (behaviours == null || behaviours.Length < 1)
//             {
//                 type = typeof(GameObject);
//             }
//             else
//             {
//                 foreach (Behaviour bb in behaviours)
//                 {
//                     if (type == null)
//                     {
//                         type = bb.GetType();
//                     }
//                     else if (bb is Selectable)
//                     {
//                         type = bb.GetType();
//                     }
//                 }
//             }
//             listProperties.Add(new Property(type, unit.name, path));
//         }
//         return Util.IterationArguments.Continue;
//     }
//     private static void generateListCodeAndPrefab(Transform listRoot,string className)
//     {
//         bool handleBtn = false;
//         string code = "";
//         code += "using UnityEngine;\n";
//         code += "using UnityEngine.UI;\n";
//         //code += "using System.Collections;\n";
//         //code += "using System.Collections.Generic;\n";
//         code += "\n";
//         code += "public class " + className + "Base : GUnlimitedItem \n{";
//         code += "\n";
//         //变量
//         foreach (Property property in listProperties)
//         {
//             code += "    public " + property.type.Name + " " + property.name + ";\n";
//         }
//         //函数Initialize
//         code += "\n";
//         code += "    protected override void Awake()\n";
//         code += "    {\n";
//         foreach (Property property in listProperties)
//         {
//             code += "        this." + property.name + " = " + "Util.FindObject<" + property.type + ">(transform, \""+property.path + "\");\n";
//             if (property.type.IsSubclassOf(typeof(GButtonBase)))
//             {
//                 handleBtn = true;
//                 code += "        this." + property.name + ".OnClick = this.OnButtonClickHandler;\n";
//             }
//         }

//         GButtonBase btn = listRoot.GetComponent<GButtonBase>();
//         if(btn!=null)
//         {
//             handleBtn = true;
//             code += "        this.GetComponent<GButtonBase>().OnClick = this.OnButtonClickHandler;\n";
//         }

//         code += "    }\n";

//         code += "}";
//         string path = Config.UIClassPath + "Items/Base/";
//         WriteClass(code, path, className+"Base");

//         path = Config.UIClassPath + "Items/";
//         if (!System.IO.File.Exists(path + className + ".cs"))
//         {
//             code = "";
//             code += "using UnityEngine;\n";
//             code += "using UnityEngine.UI;\n";
//             //code += "using System.Collections;\n";
//             //code += "using System.Collections.Generic;\n";
//             code += "\n";
//             code += "public class " + className + " : " + className + "Base \n{";
//             code += "\n";
//             code += "    protected override void Awake()\n";
//             code += "    {\n";
//             code += "        base.Awake();\n";
//             code += "    }\n";
//             code += "    public override void Data(object data)\n";
//             code += "    {\n\n";
//             code += "    }\n";
//             if (handleBtn)
//             {
//                 code += "    public override void OnButtonClickHandler(GameObject go)\n";
//                 code += "    {\n";
//                 code += "        base.OnButtonClickHandler(go);\n";
//                 code += "    }\n";
//             }
//             code += "}\n";
//             path = Config.UIClassPath + "Items/";

//             WriteClass(code, path, className);
//         }

//         GameObject prefab = PrefabUtility.CreatePrefab(Config.UIPrefabPath+"Items/" + className + ".prefab", listRoot.gameObject, ReplacePrefabOptions.Default);
//         GameObject.DestroyImmediate(listRoot.gameObject);
//         Debug.Log("生成完毕！");
//         AssetDatabase.Refresh();
//         AssetDatabase.SaveAssets();
//     }

//     private static void generateListCode(Transform list)
//     {
//         Transform item = list.GetChild(0);
//         Util.IterateChild(item, "", generateListProperty);

//         generateListCodeAndPrefab(item, getCurrentItemClassName(root.name, list.name));
//     }

//     public static void WriteClass(string code, string path, string className)
//     {
//         System.IO.File.WriteAllText(path + className + ".cs", code, System.Text.UnicodeEncoding.UTF8);
//     }
//     private static string getCurrentItemClassName(string className, string itemName)
//     {
//         return System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(className) +
//             System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(itemName) +
//                 "Item";
//     }
// }
