﻿using System.Collections.Generic;
using UnityEngine;

public class ManagerData
{
    private Dictionary<int, DeviceData> m_DeviceMap = new Dictionary<int, DeviceData>();
    private Dictionary<int, PartData> m_PartMap = new Dictionary<int, PartData>();
    private Dictionary<int, ToolData> m_ToolMap = new Dictionary<int, ToolData>();
    private Dictionary<int, QuestionData> m_QuestionMap = new Dictionary<int, QuestionData>();
    private Dictionary<int, ActionData> m_ActionMap = new Dictionary<int, ActionData>();
    private Dictionary<int, TipData> m_TipMap = new Dictionary<int, TipData>();
    private static ManagerData m_Instance;
    public static ManagerData Instance
    {
        get
        {
            if (m_Instance == null)
                m_Instance = new ManagerData();
            return m_Instance;
        }
    }
    private static Constance m_Constance;
    public static Constance Constance
    {
        get
        {
            if (m_Constance == null)
            {
                AssetBundle bundle = AssetBundle.LoadFromFile(Application.streamingAssetsPath + "/constance");
                m_Constance = bundle.LoadAsset("constance") as Constance;
                bundle.Unload(false);
                if (m_Constance == null)
                {
                    Debug.LogWarning("静态数据是空数据！=> ManagerData.Constance");
                }
            }
            return m_Constance;
        }
    }

    //设备表
    public Dictionary<int, DeviceData> DeviceMap
    {
        get
        {
            if (m_DeviceMap.Count <= 0)
            {
                foreach (DeviceData dd in Constance.DeviceDataList)
                    m_DeviceMap.Add(dd.id, dd);
            }
            return m_DeviceMap;
        }
    }
    public DeviceData GetDeviceDataById(int deviceId)
    {
        return DeviceMap[deviceId];
    }

    //部件表
    public Dictionary<int, PartData> PartMap
    {
        get
        {
            if (m_PartMap.Count <= 0)
            {
                foreach (PartData pd in Constance.PartDataList)
                    m_PartMap.Add(pd.id, pd);
            }
            return m_PartMap;
        }
    }
    public PartData GetPartDataById(int partId)
    {
        return PartMap[partId];
    }
    //工具表
    public Dictionary<int, ToolData> ToolMap
    {
        get
        {
            if (m_ToolMap.Count <= 0)
            {
                foreach (ToolData td in Constance.ToolDataList)
                    m_ToolMap.Add(td.id, td);
            }
            return m_ToolMap;
        }
    }
    public ToolData GetToolDataById(int toolId)
    {
        return ToolMap[toolId];
    }
    //问题表
    public Dictionary<int, QuestionData> QuestionMap
    {
        get
        {
            if (m_QuestionMap.Count <= 0)
            {
                foreach (QuestionData qd in Constance.QuestionDataList)
                    m_QuestionMap.Add(qd.id, qd);
            }
            return m_QuestionMap;
        }
    }
    public QuestionData GetQuestionDataById(int questionId)
    {
        return QuestionMap[questionId];
    }
    //动作表
    public Dictionary<int, ActionData> ActionMap
    {
        get
        {
            if (m_ActionMap.Count <= 0)
            {
                foreach (ActionData ad in Constance.ActionDataList)
                    m_ActionMap.Add(ad.id, ad);
            }
            return m_ActionMap;
        }
    }
    public ActionData GetActionDataById(int actionId)
    {
        return ActionMap[actionId];
    }
    public ActionData GetActionDataByOrder(int playOrder)
    {
        return Constance.ActionDataList.Find(ad => ad.playOrder == playOrder);
    }
    public ActionData GetActionDataByClip(string clipName)
    {
        return Constance.ActionDataList.Find(ad => ad.clipName.Equals(clipName));
    }
    //提示表
    public Dictionary<int, TipData> TipMap
    {
        get
        {
            if (m_TipMap.Count <= 0)
            {
                foreach (TipData td in Constance.TipDataList)
                    m_TipMap.Add(td.id, td);
            }
            return m_TipMap;
        }
    }
    public TipData GetTipDataById(int tipId)
    {
        return TipMap[tipId];
    }
}
