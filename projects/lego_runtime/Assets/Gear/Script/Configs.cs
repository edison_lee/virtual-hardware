﻿using UnityEngine;

public static class Configs
{
    public const string TagPanel = "Panel";
    public const string TagProperty = "Property";
    public const string TagListProperty = "ListProperty";
    public const string UIRootPathInHierarchy = "Main/RootCanvas";
    public const string UIGeneratePathCode = "/Script/UI/";
    public const string UIGeneratePathPrefab = "/Resources/Prefab/UI/";
    public const string MenuPathRoot = "Gear/";
    public const string MenuPathUI = MenuPathRoot + "UI/";
    public const string MenuPathUIGenerate = MenuPathUI + "Generate";
}
