﻿using UnityEngine;
public interface IButtonClickCallBack
{
    void OnButtonClickHandler(GameObject value);
    void OnButtonSelectHandler(GameObject value);
}