using UnityEngine;
using System.Collections.Generic;
public static class Utility
{
    public static void SetGameObjectRecursively(GameObject obj, int layer)
    {
        obj.layer = layer;
        foreach (Transform t in obj.transform)
        {
            SetGameObjectRecursively(t.gameObject, layer);
        }
    }

    /*
    
     */
    public static GameObject FindObjectUnder(GameObject root, string targetName, bool recursive = true)
    {
        if (root == null) return null;
        Transform trans = FindTransformUnder(root.transform, targetName, recursive);
        return trans ? trans.gameObject : null;
    }
    public static Transform FindTransformUnder(Transform root, string targetName, bool recursive = true)
    {
        return FindTransformUnder(new Transform[] { root }, targetName);
    }
    static List<Transform> Children = new List<Transform>();
    private static Transform FindTransformUnder(Transform[] roots, string targetName)
    {
        Children.Clear();
        foreach (var t in roots)
        {
            Transform trans = t.Find(targetName);
            if (trans) return trans;
            foreach (Transform c in t)
            {
                Children.Add(c);
            }
        }
        return Children.Count > 0 ? FindTransformUnder(Children.ToArray(), targetName) : null;
    }
}