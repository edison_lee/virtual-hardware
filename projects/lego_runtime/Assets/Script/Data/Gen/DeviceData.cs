﻿using UnityEngine;
using System.Collections.Generic;
using System;
public class DeviceData : DeviceDataBase
{
    private List<PartData> m_Parts = new List<PartData>();
    public List<PartData> Parts
    {
        get
        {
            if (m_Parts == null || m_Parts.Count <= 0)
            {
                m_Parts = ManagerData.Constance.PartDataList.FindAll(p => p.deviceId == this.id);
            }
            return m_Parts;
        }
    }
    private List<ActionData> m_Actions = new List<ActionData>();
    public List<ActionData> Actions
    {
        get
        {
            if (m_Actions == null || m_Actions.Count <= 0)
            {
                m_Actions = ManagerData.Constance.ActionDataList.FindAll(a => a.deviceId == this.id);
            }
            return m_Actions;
        }

    }

    private List<ToolData> m_Tools = new List<ToolData>();
    public List<ToolData> Tools
    {
        get
        {
            if (m_Tools == null || m_Tools.Count <= 0)
            {
                m_Tools = ManagerData.Constance.ToolDataList.FindAll(t =>
                {
                    return Actions.Exists(a => a.Tools.Exists(at => at.id == t.id));
                });
            }
            return m_Tools;
        }
    }
    private int ActionDataSortFunc(ActionData a, ActionData b)
    {
        return a.playOrder.CompareTo(b.playOrder);
    }
    private List<ActionData> m_ActionsAssembly = new List<ActionData>();
    private List<ActionData> m_ActionsDissembly = new List<ActionData>();
    public List<ActionData> GetActionsByOperationType(bool isAssembly)
    {
        if (isAssembly)
        {
            if (m_ActionsAssembly == null || m_ActionsAssembly.Count <= 0)
            {
                m_ActionsAssembly = Actions.FindAll(a => a.isAssembly);
                m_ActionsAssembly.Sort(ActionDataSortFunc);
            }
            return m_ActionsAssembly;
        }
        else
        {
            if (m_ActionsDissembly == null || m_ActionsDissembly.Count <= 0)
            {
                m_ActionsDissembly = Actions.FindAll(a => !a.isAssembly);
                m_ActionsDissembly.Sort(ActionDataSortFunc);
            }
            return m_ActionsDissembly;
        }
    }
    public ActionData GetActionDataByPlayOrder(int order)
    {
        return Actions.Find(a => a.playOrder == order);
    }
    public ActionData GetActionDataByClip(string clipName)
    {
        return Actions.Find(ad => ad.clipName.Equals(clipName));
    }
    private List<PartData> m_FocusPartDatas = new List<PartData>();
    public List<PartData> GetFocusPartDatas()
    {
        if (m_FocusPartDatas.Count <= 0)
        {
            m_FocusPartDatas = Parts.FindAll(p => p.focusIndex > 0);
            m_FocusPartDatas.Sort((p1, p2) => p1.focusIndex.CompareTo(p2.focusIndex));
        }
        return m_FocusPartDatas;
    }
    public string GetDisplayName()
    {
        return "MOTO-" + model;
    }
    public string GetResourceName()
    {
        return "Device_" + model;
    }
}