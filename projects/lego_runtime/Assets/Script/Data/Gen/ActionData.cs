﻿using UnityEngine;
using System.Collections.Generic;
public class ActionData : ActionDataBase
{
    const string SEG = ",";
    private string[] m_ToolIds = null;
    public string[] ToolIds
    {
        get
        {
            if (m_ToolIds == null)
            {
                if (string.IsNullOrEmpty(tools))
                {
                    Debug.Log("the 'tools' param is empty in the excel table.");
                }
                else
                {
                    string toolStr = string.Empty;
                    if (tools.Contains(SEG))
                    {
                        m_ToolIds = tools.Split(SEG.ToCharArray());
                        if (m_ToolIds.Length < 2)
                        {
                            Debug.Log("action表tools字段配置有误。");
                        }
                    }
                    else
                    {
                        m_ToolIds = new string[] { tools };
                    }
                }
            }
            return m_ToolIds;
        }
    }
    public int displayToolId
    {
        get
        {
            if (ToolIds != null)
            {
                return int.Parse(ToolIds[0]);
            }
            return -1;
        }
    }
    public PartData Part { get { return ManagerData.Instance.GetPartDataById(this.partId); } }
    public ToolData Tool { get { return ManagerData.Instance.GetToolDataById(this.displayToolId); } }
    private List<ToolData> m_Tools = new List<ToolData>();
    public List<ToolData> Tools
    {
        get
        {
            if (m_Tools.Count <= 0)
            {
                if (ToolIds != null)
                {
                    foreach (var tid in ToolIds)
                        m_Tools.Add(ManagerData.Instance.GetToolDataById(int.Parse(tid)));
                }
            }
            return m_Tools;
        }
    }
    public ActionData Next
    {
        get
        {
            var acts = Profile.Instance.CurrentDeviceData.GetActionsByOperationType(isAssembly);
            int next = playOrder + 1;
            var rst = acts.Find(a => a.playOrder == next);
            rst = rst == null ? acts[0] : rst;
            return rst;
        }
    }
    public bool IsKeyAction { get { return Part ? Part.focusIndex > 0 : false; } }

    public bool IsLastActionOnThisPart
    {
        get
        {
            var acts = Profile.Instance.CurrentDeviceData.GetActionsByOperationType(isAssembly).FindAll(a => a.partId == this.partId);
            return acts.TrueForAll(e => e.playOrder <= this.playOrder);
        }
    }
    public bool NeedUpdateFocus
    {
        get
        {
            return IsKeyAction && IsLastActionOnThisPart;
        }
    }
    public bool IsLast
    {
        get
        {
            var acts = Profile.Instance.CurrentDeviceData.GetActionsByOperationType(isAssembly);
            return acts.TrueForAll(e => e.playOrder <= this.playOrder);
        }
    }
    private List<int> m_DelayTools = null;
    public List<int> DelayTools
    {
        get
        {
            if (m_DelayTools == null)
            {
                if (!string.IsNullOrEmpty(delayTools))
                {
                    string[] segs = null;
                    if (delayTools.Contains(SEG))
                    {
                        segs = delayTools.Split(SEG.ToCharArray());
                        if (segs.Length >= 2)
                        {
                            m_DelayTools = new List<string>(segs).ConvertAll<int>(e => int.Parse(e));
                        }
                    }
                    else
                    {
                        m_DelayTools = new List<int> { int.Parse(delayTools) };
                    }
                }
            }
            return m_DelayTools;
        }
    }
}