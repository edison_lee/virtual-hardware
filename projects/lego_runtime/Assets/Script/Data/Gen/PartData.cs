﻿using System;
using UnityEngine;
using System.Collections.Generic;
public class PartData : PartDataBase
{
    // private PartNodeType nodeType = PartNodeType.None;
    // public PartNodeType NodeType
    // {
    //     get
    //     {
    //         nodeType = nodeType == PartNodeType.None ? this.belongPartId == 0 ? PartNodeType.Root : ManagerData.Constance.PartDataList.Exists(pd => pd.belongPartId == this.id) ? PartNodeType.Normal : PartNodeType.Leaf : nodeType;
    //         return nodeType;
    //     }
    // }
    // private List<PartData> children = new List<PartData>();
    // public List<PartData> Children
    // {
    //     get
    //     {
    //         if (children == null || children.Count <= 0)
    //         {
    //             children = ManagerData.Constance.PartDataList.FindAll(data => data.belongPartId == this.id);
    //         }
    //         return children;
    //     }
    // }
    // private PartData m_Parent;
    // public PartData Parent
    // {
    //     get
    //     {
    //         if (!m_Parent)
    //             m_Parent = ManagerData.Instance.GetPartDataById(this.belongPartId);
    //         return m_Parent;
    //     }
    // }
    // private List<ActionData> actions = new List<ActionData>();
    // public List<ActionData> Actions
    // {
    //     get
    //     {
    //         if (actions == null || actions.Count <= 0)
    //         {
    //             actions = ManagerData.Constance.ActionDataList.FindAll(data => data.partId == this.id);
    //         }
    //         return actions;
    //     }
    // }
    public string ImageName { get { return Device.model + "_Part_" + this.imageName; } }
    // public bool IsFocusPart { get { return focusIndex > 0; } }
    public DeviceData Device { get { return ManagerData.Instance.GetDeviceDataById(this.deviceId); } }
    private List<string> m_Pns = null;
    public List<string> GetPNs()
    {
        if (m_Pns == null || m_Pns.Count <= 0)
            m_Pns = new List<string>(pn.Split(','));
        return m_Pns;
    }
}