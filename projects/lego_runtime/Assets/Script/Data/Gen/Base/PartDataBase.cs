﻿using UnityEngine;
public class PartDataBase : ScriptableObject
{
	public int id;
	public string partName;
	public string indexName;
	public string imageName;
	public int deviceId;
	public bool recovery;
	public string pn;
	public int focusIndex;
	public bool active;
}