using UnityEngine;
using System.Collections.Generic;
using System;
public class LegoDeviceBehaviour : MonoBehaviour
{
    private const float DefaultAnimationSpeed = 0.1f;
    private float m_AnimationSpeed = DefaultAnimationSpeed;
    private ActionPlayMode m_Mode;
    public ActionPlayMode Mode { get { return m_Mode; } }
    private Animator m_Anim;
    private Dictionary<int, LegoPartBehaviour> m_PartMap = new Dictionary<int, LegoPartBehaviour>();
    private Dictionary<int, GameObject> m_ToolMap = new Dictionary<int, GameObject>();
    private LegoPartBehaviour m_Silhouette;
    public Action<PartData> PartClickEvent;
    public Action<ActionData> ClipEndEvent;
    private ActionData m_CurrentActionData;
    private bool m_IsPlaying;
    public bool IsPlaying { get { return m_IsPlaying; } }
    protected void Awake()
    {
        LegoPartBehaviour[] parts = GetComponentsInChildren<LegoPartBehaviour>();
        foreach (LegoPartBehaviour lpb in parts)
        {
            if (m_PartMap.ContainsKey(lpb.PartId))
            {
                Debug.LogWarning("存在相同的Id请检查编辑器的生成情况，id=" + lpb.PartId.ToString());
            }
            m_PartMap.Add(lpb.PartId, lpb);
        }

        var tools = ManagerData.Constance.ToolDataList;
        foreach (var td in tools)
        {
            var to = Utility.FindObjectUnder(gameObject, td.modelName);
            if (to)
            {
                m_ToolMap.Add(td.id, to);
            }
        }

        m_Anim = GetComponent<Animator>();
        m_Anim.speed = DefaultAnimationSpeed;
    }
    public void SetAnimSpeed(float speed)
    {
        m_AnimationSpeed = speed;
        m_Anim.speed = speed;
    }
    public void Play(ActionData action, ActionPlayMode mode)
    {
        m_Mode = mode;
        m_CurrentActionData = action;
        updateActionToolsVisible(action, true);
        // Debug.Log("ActionId:" + action.id.ToString() + ", ActionName" + action.clipName);
        m_Anim.Play(action.clipName, 0, 0);
        m_IsPlaying = true;
    }
    public void ResetModelStatus(bool isAssemply)
    {
        hideAllTools();
        m_Anim.Play(isAssemply ? "Install_Original" : "Uninstall_Original", 0, 0);
    }
    public void Pause()
    {
        m_Anim.speed = 0;
    }
    public void Continue()
    {
        m_Anim.speed = m_AnimationSpeed;
    }

    //-------------------事件函数-------------------
    //部件被点击
    public void OnPartClick(int partId)
    {
        PartData pd = ManagerData.Instance.GetPartDataById(partId);
        if (PartClickEvent != null)
            PartClickEvent(pd);
    }
    //一个clip的最后一帧
    public void OnClipEnd(string clipName)
    {
        ActionData ad = Profile.Instance.CurrentDeviceData.GetActionDataByClip(clipName);
        updateActionToolsVisible(ad, false);
        if (ClipEndEvent != null)
        {
            ClipEndEvent(ad);
        }
        m_IsPlaying = false;
    }

    public void ShowPartSilhouette(int partId)
    {
        if (m_Silhouette)
            m_Silhouette.HideSilhouette();
        if (m_PartMap.TryGetValue(partId, out m_Silhouette))
        {
            m_Silhouette.ShowSilhouette();
        }
    }
    public LegoPartBehaviour GetPartBehaviour(int partId)
    {
        return m_PartMap[partId];
    }
    /**
      *  函数名：隐藏所有工具
     */
    private void hideAllTools()
    {
        foreach (var pair in m_ToolMap)
        {
            if (pair.Value.activeSelf)
                pair.Value.SetActive(false);
        }
    }
    private void updateActionToolsVisible(ActionData actionData, bool begin)
    {
        hideAllTools();
        foreach (var t in actionData.Tools)
        {
            GameObject gameObject;
            if (m_ToolMap.TryGetValue(t.id, out gameObject))
            {
                bool v = begin;
                if (!begin)
                {
                    if (actionData.DelayTools != null && actionData.DelayTools.Exists(d => d == t.id))
                    {
                        v = true;
                    }
                }

                if (gameObject.activeSelf != v)
                {
                    gameObject.SetActive(v);
                }
            }
        }
    }
}