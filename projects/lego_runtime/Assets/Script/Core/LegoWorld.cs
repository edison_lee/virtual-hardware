using UnityEngine;
using System;
using System.Collections.Generic;
public class LegoWorld
{
    private static LegoWorld instance;
    private GameMode m_Mode;
    public GameMode Mode { get { return m_Mode; } }
    private bool m_IsAssemply;
    public bool IsAssembly { get { return m_IsAssemply; } }
    private LegoCamera m_CameraContoller;
    private LegoCamera CameraController
    {
        get
        {
            if (m_CameraContoller == null)
            {
                var prefab = Resources.Load("Prefab/LegoCamera");
                GameObject objectCamera = GameObject.Instantiate(prefab) as GameObject;
                m_CameraContoller = objectCamera.GetComponent<LegoCamera>();
            }
            return m_CameraContoller;
        }
    }
    public List<ActionData> Actions { get { return Profile.Instance.CurrentDeviceData.GetActionsByOperationType(IsAssembly); } }
    private ActionData m_CurrentAction;
    public ActionData CurrentAction { get { return m_CurrentAction; } }
    private PanelOperation m_OperationPanel { get { return PanelOperation.Instance; } }
    private LegoDeviceBehaviour m_DeviceBehaviour;
    public static LegoWorld Instance
    {
        get
        {
            if (instance == null)
                instance = new LegoWorld();
            return instance;
        }
    }
    // 函数说明     游戏开始函数
    // 参数说明     model机型, gameType游戏类型(教学、比赛), opType操作类型(0拆、1装)     
    public void StartUp(GameMode gm, bool isAssembly, Action<object> callBack = null, int target = 0, int mode = 0)
    {
        CheckGuid(gm, isAssembly, () =>
        {
            if (gm == GameMode.Teaching)
            {
                Enter(gm, isAssembly);
            }
            else if (gm == GameMode.Competition)
            {
                new RpcExamStart(target, isAssembly ? 1 : 0, (o) =>
                    {
                        if (callBack != null)
                            callBack(null);
                        Enter(gm, isAssembly);
                    }).Execute();
            }
            else if (gm == GameMode.Examination)
            {
                ManagerUI.Instance.Open<PanelExam>();
            }
        });
    }
    //检测新手引导
    private void CheckGuid(GameMode gm, bool isAssembly, Action callBack)
    {
        GuidPage gp = (GuidPage)gm;
        if (!Main.Instance.HasEverLogin && !Main.Instance.IsPanelOpened(gp))
        {
            ManagerUI.Instance.Open<PanelGuid>(new PanelGuidParam(gp, () =>
            {
                callBack();
            }));
        }
        else
        {
            callBack();
        }
    }
    //初始化场景
    private void Enter(GameMode gm, bool isAssembly)
    {
        m_Mode = gm;
        m_IsAssemply = isAssembly;

        UnityEngine.Object prefab = ManagerResource.Instance.GetDevicePrefab(Profile.Instance.CurrentDeviceData.model);
        GameObject deviceObj = GameObject.Instantiate(prefab) as GameObject;
        m_DeviceBehaviour = deviceObj.AddComponent<LegoDeviceBehaviour>();
        m_DeviceBehaviour.ClipEndEvent = this.onClipEnd;
        m_DeviceBehaviour.PartClickEvent = this.onPartClick;
        if (gm != GameMode.Competition)
        {
            MeshCollider[] colliders = m_DeviceBehaviour.GetComponentsInChildren<MeshCollider>(true);
            foreach (MeshCollider mc in colliders)
                mc.enabled = false;
        }
        CameraController.Target = deviceObj.transform;
        ManagerUI.Instance.Open<PanelOperation>();
        m_OperationPanel.ResetFocusPartEnable(m_IsAssemply);
        if (gm == GameMode.Teaching)
        {
            StartTeachingFrom(Actions[0]);
        }
        else if (gm == GameMode.Competition)
        {
            updatePanelByAction(Actions[0]);
            m_DeviceBehaviour.ResetModelStatus(isAssembly);
        }
    }
    private void updatePanelByAction(ActionData actionData)
    {
        m_CurrentAction = actionData;
        m_OperationPanel.UpdateActionInfo(actionData);
        if (m_Mode == GameMode.Teaching)
        {
            OnPartSelected(actionData.Part);
            m_OperationPanel.UpdateToolInfo(actionData.Tool);
        }
    }
    private void onPartClick(PartData partData)
    {
        OnPartSelected(partData);
    }
    private void onClipEnd(ActionData actionData)
    {
        if (actionData.NeedUpdateFocus)
        {
            m_OperationPanel.UpdateFocusPartEnableOnClipEnd(actionData.Part.focusIndex, actionData.isAssembly);
        }

        if (m_DeviceBehaviour.Mode == ActionPlayMode.List)
        {
            updatePanelByAction(actionData.Next);
            m_DeviceBehaviour.Play(actionData.Next, ActionPlayMode.List);
        }
        else if (m_DeviceBehaviour.Mode == ActionPlayMode.Single)
        {
            if (!actionData.IsLast)
            {
                updatePanelByAction(actionData.Next);
            }
        }
    }
    public void Exit()
    {
        GameObject.Destroy(m_DeviceBehaviour.gameObject);
    }

    //教学模式
    #region Teaching
    public void StartTeachingFrom(ActionData act)
    {
        updatePanelByAction(act);
        m_DeviceBehaviour.Play(act, ActionPlayMode.List);
    }

    #endregion

    private ToolData m_CurrentTool;
    public void OnToolSelected(ToolData toolData)
    {
        if (m_DeviceBehaviour.IsPlaying) return;
        m_CurrentTool = toolData;
        if (IsSelectedPartCorrect())
        {
            if (!toolData.needParam)
            {
                if (IsSelectedToolCorrect())
                {
                    m_DeviceBehaviour.Play(m_CurrentAction, ActionPlayMode.Single);
                }
            }
        }
    }

    public void OnTimeAndTemperatureSubmit(int temperature, int timeInMinute)
    {
        if (IsSelectedPartCorrect() && IsSelectedToolCorrect())
        {
            if (m_CurrentTool.needParam && m_CurrentAction.heatTemperature == temperature && m_CurrentAction.heatTime == timeInMinute)
            {
                m_DeviceBehaviour.Play(m_CurrentAction, ActionPlayMode.Single);
            }
        }
    }
    private bool IsSelectedPartCorrect()
    {
        return m_OperationPanel.CurrentPartData && m_OperationPanel.CurrentPartData.id == m_CurrentAction.partId;
    }
    private bool IsSelectedToolCorrect()
    {
        return m_CurrentTool && m_CurrentTool.id == m_CurrentAction.displayToolId;
    }

    public void OnPartSelected(PartData partData)
    {
        m_DeviceBehaviour.ShowPartSilhouette(partData.id);
        if (m_OperationPanel)
        {
            m_OperationPanel.UpdatePartInfo(partData);
        }
    }

    //公共接口
    #region Common Interface
    public void BreakAll()
    {
        m_DeviceBehaviour.ResetModelStatus(true);
    }
    public void JoinAll()
    {
        m_DeviceBehaviour.ResetModelStatus(false);
    }
    public void ChangeFocusPart(int partId)
    {
        LegoPartBehaviour lpart = m_DeviceBehaviour.GetPartBehaviour(partId);
        if (lpart)
        {
            CameraController.Target = lpart.transform;
            CameraController.ForceDirty();
        }
    }
    public void PlayOrContinue()
    {
        m_DeviceBehaviour.Continue();
    }
    public void Pause()
    {
        m_DeviceBehaviour.Pause();
    }
    public void SetSpeed(float speed)
    {
        m_DeviceBehaviour.SetAnimSpeed(speed);
    }
    #endregion
}
