public enum GuidPage
{
    Main = 0,
    Teaching,
    Competition,
    Examination
}
public enum GameMode
{
    Teaching = 1,   //教学
    Competition,    //比赛
    Examination
}
public enum ActionPlayMode
{
    Single, //单步播放，播放当前动作
    List    //顺序播放，从当前动作开始按顺序播放
}
public enum PartNodeType
{
    None,
    Root,
    Normal,
    Leaf
}