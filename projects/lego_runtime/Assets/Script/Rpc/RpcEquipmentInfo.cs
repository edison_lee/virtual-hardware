// 请求 url：    /equipment/info.do  （Post请求）
// 请求参数:     Long model                        // 型号
//               Long userNo                       // 用户帐号

// 响应参数：    Long model;                       // 型号
//               String modelName;                 // 型号名称
//               String icon;                      // 图标
//               String component;                 // 包含零件的配置文件路径
//               String dismantle;                 // 拆机动画的配置文件路径
//               String install ;                  // 装机动画的配置文件路径
//               String PN ;                       // PN码
//               String tool ;                     // 工具的配置文件路径
//               String exam ;                     // 考试题的配置文件路径
//               String remark;                    // 描述