// 请求 url：    /exam/start.do  （Post请求）
// 请求参数:     Long model                        // 型号
//               Long userNo                       // 用户帐号
//               Long targetPK                     // 如果有PK对象  那么这就是pk模式； 如果为空  就是普通考试；    examType=2时  这个值一定为空
//               Integer examType                  // 考试类型  0 拆机  1 装机  2  答题


// 响应参数：    Integer result                    //0 成功
using System;
public class RpcExamStart : RpcBase<S2CExamStart>
{
    public RpcExamStart(int target, int type, Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "exam";
        this.funcName = "start";
        C2SExamStart data = new C2SExamStart();
        data.model = Profile.Instance.CurrentDeviceData.GetDisplayName();
        data.targetPK = target;
        data.examType = type;
        sendData = data;
    }
    // protected override void OnData(S2CExamStart data)
    // {
    //     C2SExamStart param = sendData as C2SExamStart;
    //     if (param == null) return;
    //     string sceneName = param.examType == 0 ? "goGeek_D_V" : "goGeek_A_V";
    //     new LoadSceneOpera("Scene/" + sceneName).Execute();
    // }
}