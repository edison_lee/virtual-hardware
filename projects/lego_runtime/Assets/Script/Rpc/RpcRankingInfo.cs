using System;
using UnityEngine;
public class RpcRankingInfo : RpcBase<DataRanking>
{
    public RpcRankingInfo(int examType, Action<object> onComplete = null) : base(onComplete)
    {
        this.modeule = "ranking";
        this.funcName = "info";
        C2SRankingInfo data = new C2SRankingInfo();
        data.model = Profile.Instance.CurrentDeviceData.GetDisplayName();
        data.examType = examType;
        sendData = data;
    }
}