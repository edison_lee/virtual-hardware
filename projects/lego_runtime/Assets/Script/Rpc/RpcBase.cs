using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using LitJson;
public class RpcBase<T> : SyncOpera //where T : Object
{
    // const string gate = "http://47.92.140.31:8888/";
    //正式服
    const string gate = "http://121.201.28.34/";
    public string modeule = "";
    public string funcName = "";
    public string method { get { return modeule + "/" + funcName + ".do"; } }
    public DataRemote sendData;
    public T recieveData;
    public RpcBase(Action<object> completeHandler)
        : base(completeHandler)
    { }
    private WWWForm GetWWWForm(Data data)
    {
        WWWForm form = new WWWForm();
        Type type = data.GetType();
        FieldInfo[] infos = type.GetFields();
        foreach (FieldInfo inf in infos)
        {
            form.AddField(inf.Name, inf.GetValue(data).ToString());
        }
        return form;
    }
    protected override IEnumerator SyncExecute()
    {
        if (sendData.GetType().IsSubclassOf(typeof(DataC2S)))
        {
            var send = sendData as DataC2S;
            send.userNo = Profile.Instance.UserNumber;
        }

        WWW www = new WWW(gate + method, GetWWWForm(sendData));
        Debug.Log(this.GetType().Name + " RpcSend  =" + sendData.ToString());
        // Log(this.GetType().Name + "\nRpcSend  =" + sendData.ToString());

        yield return www;

        Debug.Log(this.GetType().Name + " RpcRecieve    =" + www.text);
        // Log(this.GetType().Name + " RpcRecieve    =" + www.text);

        string json = www.text;
        if (string.IsNullOrEmpty(json))
        {
            Debug.Log("the response json string is null or empty.");
            yield break;
        }

        OnJson(json);

        JsonData jdata = JsonMapper.ToObject(json);
        int errorCode = int.Parse(jdata["result"].ToString());
        // Debug.Log(errorCode);
        if (errorCode > 0)
        {
            string error = getErrorMsg(errorCode);
            Debug.Log(error);
            GRunningTip.Show(error);
            if (onError != null)
                onError(error);
        }
        else
        {
            recieveData = JsonMapper.ToObject<T>(jdata["data"].ToJson());
            OnData(recieveData);
            if (localHandler != null)
            {
                localHandler(recieveData);
            }
        }
    }
    static List<string> errors = new List<string> { "Session expired", "Please login", "ID cannot be empty", "ID is not correct", "Login error", "System error" };
    private string getErrorMsg(int errorId)
    {
        int idx = errorId % 1000 - 1;
        return errors[idx];
    }
    protected virtual void OnJson(string json)
    { }
    protected virtual void OnData(T data)
    { }
    private Action<string> onError;
    public RpcBase<T> OnError(Action<string> onerror)
    {
        onError = onerror;
        return this;
    }
}