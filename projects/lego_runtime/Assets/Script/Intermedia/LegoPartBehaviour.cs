using UnityEngine;
using UnityEngine.EventSystems;
public class LegoPartBehaviour : MonoBehaviour
{
    [SerializeField]
    private int m_PartId;
    public int PartId { get { return m_PartId; } }
    void OnMouseUpAsButton()
    {
        if (IsPointerOverUI()) return;
        this.SendMessageUpwards("OnPartClick", m_PartId);
    }
    public void ShowSilhouette()
    {
        Utility.SetGameObjectRecursively(gameObject, LayerMask.NameToLayer("SilhouettePart"));
    }
    public void HideSilhouette()
    {
        Utility.SetGameObjectRecursively(gameObject, LayerMask.NameToLayer("Default"));
    }
    private bool IsPointerOverUI()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        return EventSystem.current.IsPointerOverGameObject();
#else
        int cnt = Input.touchCount;
        for (int i = 0; i < cnt; i++)
        {
            if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId))
                return true;
        }
        return false;
#endif
    }
}