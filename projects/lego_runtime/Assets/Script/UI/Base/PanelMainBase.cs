﻿using UnityEngine.UI;
using UnityEngine;
public class PanelMainBase : GPanelBase
{
    public GButtonColor BtnHead;
    public Text TxtUserName;
    public Text TxtCountry;
    public GUnlimitedList ListInfoScore;
    public Text TxtCurrentEquipName;
    public GButtonColorElastic BtnSearch;
    public GUnlimitedList ListDevice;
    public GButtonColorElastic BtnTutorialAs;
    public GButtonColorElastic BtnTutorialDs;
    public GButtonColorElastic BtnChallengeAs;
    public GButtonColorElastic BtnChallengeDs;
    public GButtonColorElastic BtnQuiz;
    public GButtonColor BtnRank;
    public Image ImgDevice;
    public GButtonColor BtnEmail;

    protected override void Awake()
    {
        base.Awake();
        BtnHead = Trans.Find("Container/FrameUserInfo/BtnHead").GetComponent<GButtonColor>();
        BtnHead.OnClick = OnButtonClickHandler;
        TxtUserName = Trans.Find("Container/FrameUserInfo/ImgNameBack/TxtUserName").GetComponent<Text>();
        TxtCountry = Trans.Find("Container/FrameUserInfo/ImgInfoBack/TxtCountry").GetComponent<Text>();
        ListInfoScore = Trans.Find("Container/FrameUserInfo/ImgInfoBack/ViewInfoList/ListInfoScore").GetComponent<GUnlimitedList>();
        TxtCurrentEquipName = Trans.Find("Container/FrameOperation/ImgEquipNameBack/TxtCurrentEquipName").GetComponent<Text>();
        BtnSearch = Trans.Find("Container/FrameOperation/BtnSearch").GetComponent<GButtonColorElastic>();
        BtnSearch.OnClick = OnButtonClickHandler;
        ListDevice = Trans.Find("Container/FrameOperation/FrameListEquips/ViewDevice/ListDevice").GetComponent<GUnlimitedList>();
        BtnTutorialAs = Trans.Find("Container/FrameOperation/BtnTutorialAs").GetComponent<GButtonColorElastic>();
        BtnTutorialAs.OnClick = OnButtonClickHandler;
        BtnTutorialDs = Trans.Find("Container/FrameOperation/BtnTutorialDs").GetComponent<GButtonColorElastic>();
        BtnTutorialDs.OnClick = OnButtonClickHandler;
        BtnChallengeAs = Trans.Find("Container/FrameOperation/BtnChallengeAs").GetComponent<GButtonColorElastic>();
        BtnChallengeAs.OnClick = OnButtonClickHandler;
        BtnChallengeDs = Trans.Find("Container/FrameOperation/BtnChallengeDs").GetComponent<GButtonColorElastic>();
        BtnChallengeDs.OnClick = OnButtonClickHandler;
        BtnQuiz = Trans.Find("Container/FrameOperation/BtnQuiz").GetComponent<GButtonColorElastic>();
        BtnQuiz.OnClick = OnButtonClickHandler;
        BtnRank = Trans.Find("Container/FrameShow/BtnRank").GetComponent<GButtonColor>();
        BtnRank.OnClick = OnButtonClickHandler;
        ImgDevice = Trans.Find("Container/FrameShow/ImgDevice").GetComponent<Image>();
        BtnEmail = Trans.Find("Container/FrameShow/BtnEmail").GetComponent<GButtonColor>();
        BtnEmail.OnClick = OnButtonClickHandler;

    }
}
