﻿using UnityEngine.UI;
using UnityEngine;
public class PanelExamResultBase : GPanelBase
{
    public Text TxtScore;
    public Image ImgPass;
    public Image ImgFail;
    public GButtonColor BtnRetry;
    public GButtonColor BtnClose;

    protected override void Awake()
    {
        base.Awake();
        TxtScore = Trans.Find("Container/TxtScore").GetComponent<Text>();
        ImgPass = Trans.Find("Container/FrameLogo/ImgPass").GetComponent<Image>();
        ImgFail = Trans.Find("Container/FrameLogo/ImgFail").GetComponent<Image>();
        BtnRetry = Trans.Find("Container/BtnRetry").GetComponent<GButtonColor>();
        BtnRetry.OnClick = OnButtonClickHandler;
        BtnClose = Trans.Find("Container/BtnClose").GetComponent<GButtonColor>();
        BtnClose.OnClick = OnButtonClickHandler;

    }
}
