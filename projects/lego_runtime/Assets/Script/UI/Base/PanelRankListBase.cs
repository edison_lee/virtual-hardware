﻿using UnityEngine.UI;
using UnityEngine;
public class PanelRankListBase : GPanelBase
{
    public GButtonColorElastic BtnChai;
    public GButtonColorElastic BtnZhuang;
    public GButtonColorElastic BtnBack;
    public Button BtnShowDeviceList;
    public Text TxtDeviceName;
    public Text TxtRank;
    public GUnlimitedList ListRank;
    public GUnlimitedList ListDeivces;

    protected override void Awake()
    {
        base.Awake();
        BtnChai = Trans.Find("Container/BtnChai").GetComponent<GButtonColorElastic>();
        BtnChai.OnClick = OnButtonClickHandler;
        BtnZhuang = Trans.Find("Container/BtnZhuang").GetComponent<GButtonColorElastic>();
        BtnZhuang.OnClick = OnButtonClickHandler;
        BtnBack = Trans.Find("Container/BtnBack").GetComponent<GButtonColorElastic>();
        BtnBack.OnClick = OnButtonClickHandler;
        BtnShowDeviceList = Trans.Find("Container/BtnShowDeviceList").GetComponent<Button>();
        TxtDeviceName = Trans.Find("Container/TxtDeviceName").GetComponent<Text>();
        TxtRank = Trans.Find("Container/TxtRank").GetComponent<Text>();
        ListRank = Trans.Find("Container/ViewRank/ListRank").GetComponent<GUnlimitedList>();
        ListDeivces = Trans.Find("Container/FrameDeviceList/ViewDeivces/ListDeivces").GetComponent<GUnlimitedList>();

    }
}
