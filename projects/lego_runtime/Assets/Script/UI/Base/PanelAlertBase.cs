﻿using UnityEngine.UI;
using UnityEngine;
public class PanelAlertBase : GPanelBase
{
    public Text TxtMessage;
    public GButtonColorElastic BtnClose;

    protected override void Awake()
    {
        base.Awake();
        TxtMessage = Trans.Find("Container/ImgBack/TxtMessage").GetComponent<Text>();
        BtnClose = Trans.Find("Container/BtnClose").GetComponent<GButtonColorElastic>();
        BtnClose.OnClick = OnButtonClickHandler;

    }
}
