﻿using UnityEngine.UI;
using UnityEngine;
public class PanelGuidBase : GPanelBase
{
    public GameObject FrameContainer;
    public GButton BtnOk;

    protected override void Awake()
    {
        base.Awake();
        FrameContainer = Trans.Find("Container/FrameContainer").gameObject;
        BtnOk = Trans.Find("Container/BtnOk").GetComponent<GButton>();
        BtnOk.OnClick = OnButtonClickHandler;

    }
}
