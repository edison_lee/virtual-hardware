﻿using UnityEngine.UI;
using UnityEngine;
public class PanelTip : PanelTipBase
{
    private TipData m_TipData;
    public override void SetData(object data)
    {
        base.SetData(data);
        m_TipData = data as TipData;
        if (!m_TipData) return;
        this.TxtMessage.text = m_TipData.content;
        this.ImgTip.texture = Resources.Load<Texture>("Tip/" + m_TipData.imageName);
        this.ImgTip.SetNativeSize();
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnClose.gameObject)
        {
            ManagerUI.Instance.Close(this);
        }
    }
}
