﻿using UnityEngine.UI;
using UnityEngine;
public class PanelMain : PanelMainBase
{
    protected override void Awake()
    {
        base.Awake();

        this.ListDevice.OnInitItem = initItem;
        this.ListDevice.OnSelectItem = clickItem;
        this.ListDevice.SetItemCount(ManagerData.Constance.DeviceDataList.Count);

        this.ListInfoScore.OnInitItem = initItem;
        this.ListInfoScore.OnSelectItem = clickItem;
    }
    public override void SetData(object data)
    {
        base.SetData(data);
        this.TxtUserName.text = Profile.Instance.UserName;
        this.TxtCountry.text = Profile.Instance.Country;

        this.ListInfoScore.SetItemCount(ManagerData.Constance.DeviceDataList.Count);

        if (!Main.Instance.HasEverLogin && !Main.Instance.IsPanelOpened(GuidPage.Main))
        {
            ManagerUI.Instance.Open<PanelGuid>(new PanelGuidParam(GuidPage.Main, null));
        }
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == BtnTutorialAs.gameObject)
        {
            ManagerUI.Instance.Close(this);
            LegoWorld.Instance.StartUp(GameMode.Teaching, true);
        }
        else if (go == BtnTutorialDs.gameObject)
        {
            ManagerUI.Instance.Close(this);
            LegoWorld.Instance.StartUp(GameMode.Teaching, false);
        }
        else if (go == BtnChallengeAs.gameObject)
        {
            LegoWorld.Instance.StartUp(GameMode.Competition, true, rst =>
            {
                ManagerUI.Instance.Close(this);
            });
        }
        else if (go == BtnChallengeDs.gameObject)
        {
            LegoWorld.Instance.StartUp(GameMode.Competition, false, rst =>
            {
                ManagerUI.Instance.Close(this);
            });
        }
        else if (go == BtnQuiz.gameObject)
        {
            ManagerUI.Instance.Close(this);
            LegoWorld.Instance.StartUp(GameMode.Examination, false);
        }
        else if (go == BtnRank.gameObject)
        {
            ManagerUI.Instance.Open<PanelRankList>();
        }
        else if (go == BtnEmail.gameObject)
        {
            Application.OpenURL("mailto:gami@lenovo.com");
        }
    }

    private void setDeviceStatus(Transform t, bool correct)
    {
        t.Find("right").gameObject.SetActive(correct);
        t.Find("wrong").gameObject.SetActive(!correct);
    }
    private void initItem(GameObject list, GameObject item, int index)
    {
        if (list == ListDevice.gameObject)
        {
            Text TxtDeviceName = item.transform.Find("TxtDeviceName").GetComponent<Text>();
            TxtDeviceName.text = ManagerData.Constance.DeviceDataList[index].GetDisplayName();
        }
        else if (list == ListInfoScore.gameObject)
        {

            DeviceData dd = ManagerData.Constance.DeviceDataList[index];

            S2CUserInfoExamItem info = null;
            foreach (S2CUserInfoExamItem itm in Profile.Instance.DeviceInfos)
            {
                if (itm.model.ToLower() == dd.GetDisplayName().ToLower())
                {
                    info = itm;
                    break;
                }
            }
            Text TxtDeviceName = item.transform.Find("TxtDeviceName").GetComponent<Text>();
            TxtDeviceName.text = dd.GetDisplayName();
            setDeviceStatus(item.transform.Find("ImgQuiz"), info != null && info.exam > 0);
            setDeviceStatus(item.transform.Find("ImgDessembly"), info != null && info.dismantle > 0);
            setDeviceStatus(item.transform.Find("ImgAssembly"), info != null && info.install > 0);
        }
    }
    private void clickItem(GameObject list, GameObject item, int index)
    {
        if (list == ListDevice.gameObject)
        {
            DeviceData dd = ManagerData.Constance.DeviceDataList[index];
            this.TxtCurrentEquipName.text = dd.GetDisplayName();
            Profile.Instance.CurrentDeviceData = dd;
            this.ImgDevice.overrideSprite = ManagerResource.Instance.GetDeviceSpriteByName(dd.GetResourceName());
        }
    }
}
