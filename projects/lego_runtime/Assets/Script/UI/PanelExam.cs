﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
public class PanelExam : PanelExamBase
{
    public const int QUESTION_COUNT = 5;
    public const int PASS_COUNT = 4;
    public const float RESULT_DELAY_TIME = 2f;
    readonly List<QuestionData> questions = new List<QuestionData>();
    private QuestionData question;
    private int questionCount;
    private Dictionary<int, bool> selectMap = new Dictionary<int, bool>();
    private Constance map;
    private GLayout[] opts;
    private PanelExamResultModel resultModel;
    
    private float timePoint;
    private bool m_IsCommited;
    protected override void Awake()
    {
        base.Awake();
        Toggle t1 = this.Option1.GetComponent<Toggle>();
        t1.isOn = false;
        t1.onValueChanged.AddListener((select) =>
        {
            onToggle(Option1.gameObject, select);
        });

        Toggle t2 = this.Option1.GetComponent<Toggle>();
        t2.isOn = false;
        this.Option2.GetComponent<Toggle>().onValueChanged.AddListener((select) =>
        {
            onToggle(Option2.gameObject, select);
        });

        Toggle t3 = this.Option1.GetComponent<Toggle>();
        t3.isOn = false;
        this.Option3.GetComponent<Toggle>().onValueChanged.AddListener((select) =>
        {
            onToggle(Option3.gameObject, select);
        });

        Toggle t4 = this.Option1.GetComponent<Toggle>();
        t4.isOn = false;
        this.Option4.GetComponent<Toggle>().onValueChanged.AddListener((select) =>
        {
            onToggle(Option4.gameObject, select);
        });

        Toggle t5 = this.Option1.GetComponent<Toggle>();
        t5.isOn = false;
        this.Option5.GetComponent<Toggle>().onValueChanged.AddListener((select) =>
        {
            onToggle(Option5.gameObject, select);
        });

        Toggle t6 = this.Option1.GetComponent<Toggle>();
        t6.isOn = false;
        this.Option6.GetComponent<Toggle>().onValueChanged.AddListener((select) =>
        {
            onToggle(Option6.gameObject, select);
        });
        opts = new GLayout[] { Option1, Option2, Option3, Option4, Option5, Option6 };
        map = ManagerData.Constance;
    }
    private void onToggle(GameObject go, bool select)
    {
        int answerIndex = int.Parse(go.name.Substring(go.name.Length - 1));
        selectMap[answerIndex] = select;
    }
    public override void SetData(object data)
    {
        questionCount = 0;
        this.TxtEquipName.text = Profile.Instance.CurrentDeviceData.GetDisplayName();
        List<int> indics = new List<int>();
        for (int i = 0; i < map.QuestionDataList.Count; i++)
            indics.Add(i);
        questions.Clear();
        for (int i = 0; i < QUESTION_COUNT; i++)
        {
            int idx = Random.Range(0, indics.Count - 1);
            questions.Add(map.QuestionDataList[indics[idx]]);
            indics.RemoveAt(idx);
        }
        resultModel = new PanelExamResultModel();
        timePoint = Time.time;
        ShowNext();
    }
    private void SetOptionMode(GameObject go, bool isCorrect)
    {
        var node = go.transform.Find("FrameRsult");
        var imgRight = go.transform.Find("ImgRight");
        var imgRong = go.transform.Find("ImgWrong");
        var logoRight = node.Find("ImgRight");
        var logoWrong = node.Find("ImgWrong");
        node.gameObject.SetActive(true);
        logoRight.gameObject.SetActive(isCorrect);
        logoWrong.gameObject.SetActive(!isCorrect);
        imgRight.gameObject.SetActive(isCorrect);
        imgRong.gameObject.SetActive(!isCorrect);
    }
    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnClose.gameObject)
        {
            ManagerUI.Instance.Close(this);
            Main.Instance.Home();
        }
        else
        {
            if (m_IsCommited)
            {
                ManagerUI.Instance.ShowTip("the answer has aready been submited.");
            }
            else
            {
                int answer = int.Parse(question.answer);
                if (!IsSelected())
                {
                    ManagerUI.Instance.ShowTip("you should make a choice!");
                }
                else
                {
                    m_IsCommited = true;
                    questionCount++;

                    string[] os = question.GetOptions();
                    for (int i = 0; i < opts.Length; i++)
                    {
                        Toggle toggle = opts[i].GetComponent<Toggle>();
                        toggle.interactable = true;

                        int answerIndex = i + 1;
                        GLayout layout = opts[i];
                        if (question.IsSingleAnswerCorrect(answerIndex))
                        {
                            SetOptionMode(layout.gameObject, true);
                        }
                        else
                        {
                            bool r;
                            if (selectMap.TryGetValue(answerIndex, out r))
                            {
                                if (r)
                                {
                                    SetOptionMode(layout.gameObject, false);
                                }
                            }
                        }
                    }

                    this.BtnSubmit.gameObject.SetActive(false);
                    new WaitForSecondsOpera(RESULT_DELAY_TIME, obj =>
                    {
                        resultModel.UseTime = Mathf.RoundToInt(Time.time - timePoint);
                        resultModel.AddChoice(question.id, selectMap, question.IsAnswerCorrect(selectMap));
                        if (questionCount >= QUESTION_COUNT)
                        {
                            new RpcExamResult(resultModel).Execute();
                            ManagerUI.Instance.Open<PanelExamResult>(resultModel);
                        }
                        else
                        {
                            ShowNext();
                        }
                    }).Execute();
                }
            }
        }
    }
    private void ShowNext()
    {
        selectMap = new Dictionary<int, bool>();
        m_IsCommited = false;
        question = questions[questionCount];
        this.BtnSubmit.gameObject.SetActive(true);
        this.TxtQuestionNum.GetComponent<Text>().text = string.Format("Q{0}:", questionCount + 1);
        this.TxtQuestion.GetComponent<Text>().text = question.description;

        ToggleGroup group = this.LayOptions.GetComponent<ToggleGroup>();
        group.SetAllTogglesOff();
        group.allowSwitchOff = !question.IsSingleOption;
        ToggleGroup target = question.IsSingleOption ? group : null;

        string[] os = question.GetOptions();
        for (int i = 0; i < opts.Length; i++)
        {
            Toggle t = opts[i].GetComponent<Toggle>();
            if (i >= os.Length)
            {
                t.gameObject.SetActive(false);
            }
            else
            {
                t.gameObject.SetActive(true);
                t.transform.Find("TxtOptionTag").GetComponent<Text>().text = "<color=#73FFDE>" + Chr(65 + i) + " : </color>";
                t.transform.Find("TxtOptionDesc").GetComponent<Text>().text = os[i];
                t.transform.Find("FrameRsult").gameObject.SetActive(false);
                var r = t.transform.Find("ImgRight");
                var w = t.transform.Find("ImgWrong");
                r.gameObject.SetActive(false);
                w.gameObject.SetActive(false);
            }
            t.interactable = true;
            t.group = target;
        }
        this.LayRoot.LayoutChange();
        this.LayRoot.Rect.DOScale(Vector3.zero, .3f).From().SetEase(Ease.OutBack).OnComplete(() =>
        {
            this.LayRoot.LayoutChange();
        });
    }
    //asc码转字符
    private static string Chr(int asciiCode)
    {
        if (asciiCode >= 65 && asciiCode <= 90)
        {
            System.Text.ASCIIEncoding asciiEncoding = new System.Text.ASCIIEncoding();
            byte[] byteArray = new byte[] { (byte)asciiCode };
            string strCharacter = asciiEncoding.GetString(byteArray);
            return (strCharacter);
        }
        return "";
    }
    //是否做出了选择
    private bool IsSelected()
    {
        foreach (KeyValuePair<int, bool> p in selectMap)
        {
            if (p.Value)
                return true;
        }
        return false;
    }
}
