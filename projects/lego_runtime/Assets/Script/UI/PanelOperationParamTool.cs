﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class PanelOperationParamTool : UIBehaviour
{
    public Text TxtTime;
    public Text TxtTemperature;
    private int m_TargetTime;
    private int m_TargetTemperature;
    private int m_CurrentTime;
    private string m_Suffix;
    private int m_Increment;
    public int CurrentTime
    {
        get
        {
            return m_CurrentTime;
        }
        set
        {
            m_CurrentTime = value;
            TxtTime.text = m_CurrentTime.ToString() + (m_CurrentTime > 0 ? " " + m_Suffix : string.Empty);
            TxtTime.color = (m_IsRecommendMode && m_CurrentTime > 0 && m_CurrentTime == m_TargetTime) ? Color.green : Color.white;
        }
    }
    private int m_CurrentTemperature;
    public int CurrentTemperature
    {
        get
        {
            return m_CurrentTemperature;
        }
        set
        {
            m_CurrentTemperature = value;
            TxtTemperature.text = m_CurrentTemperature.ToString();
            TxtTemperature.color = (m_IsRecommendMode && m_CurrentTemperature > 0 && m_CurrentTemperature == m_TargetTemperature) ? Color.green : Color.white;
        }
    }
    public bool IsShow { get { return isActiveAndEnabled; } }
    private bool m_IsRecommendMode;

    public void OnBtnTimeIncrease()
    {
        CurrentTime += m_Increment;
    }
    public void OnBtnTimeDecrease()
    {
        int newValue = CurrentTime - m_Increment;
        if (newValue >= 0)
        {
            CurrentTime = newValue;
        }
    }
    public void OnBtnTemperatureIncrease()
    {
        CurrentTemperature += 10;
    }
    public void OnBtnTemperatureDecrease()
    {
        int newValue = CurrentTemperature - 10;
        if (newValue >= 0)
        {
            CurrentTemperature = newValue;
        }
    }

    public void Show(bool isRecommendMode = false, int targetTime = 0, int targetTemperature = 0, string suffix = "min", int increment = 1)
    {
        gameObject.SetActive(true);
        m_IsRecommendMode = isRecommendMode;
        m_TargetTime = targetTime;
        m_TargetTemperature = targetTemperature;
        m_Suffix = suffix;
        m_Increment = increment;
        CurrentTime = CurrentTemperature = 0;

        if (LegoWorld.Instance.Mode == GameMode.Teaching)
        {
            CurrentTime = targetTime;
            CurrentTemperature = targetTemperature;
        }
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }
}
