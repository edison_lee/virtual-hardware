﻿using UnityEngine.UI;
using UnityEngine;
using System;
public class PanelAlert : PanelAlertBase
{
    private PanelAlertModel model;
    public override void SetData(object data)
    {
        base.SetData(data);
        model = data as PanelAlertModel;
        this.TxtMessage.text = model.Message;
    }

    public override void OnButtonClickHandler(GameObject go)
    {
        if (go == this.BtnClose.gameObject)
        {
            ManagerUI.Instance.Close(this);
            if (model.OnClose != null)
            {
                Action f = model.OnClose;
                f();
            }
        }
    }
}
