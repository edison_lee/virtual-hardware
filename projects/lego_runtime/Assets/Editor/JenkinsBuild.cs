﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Text;

public class JenkinsAdapter
{
    [MenuItem("Lego/发布apk")]
    public static void Build()
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTargetGroup.Android, BuildTarget.Android);

        PlayerSettings.keystorePass = "123456";
        PlayerSettings.keyaliasPass = "123456";
        PlayerSettings.Android.bundleVersionCode = 7;

        List<string> sceneList = new List<string>();
        EditorBuildSettingsScene[] temp = EditorBuildSettings.scenes;
        for (int i = 0, iMax = temp.Length; i < iMax; ++i)
            sceneList.Add(temp[i].path);
        string sdkPath = string.Empty;
        string path = string.Empty;
#if UNITY_EDITOR_OSX
        path = "/Users/lizhixiong/Others/Lego/apk/";
        sdkPath = "/Users/lizhixiong/Others/Unity/Project-Unity/sdk";
#elif UNITY_EDITOR_WIN
        path = "C:/Users/leon/Desktop/lego_output/apk/";
        sdkPath = "D:/Android/sdk_r25.2.3";
#endif
        path += "com.dce.Lego_" + string.Format("{0:yyyyMMdd_HHmm}", DateTime.Now) + ".apk";
        BuildPipeline.BuildPlayer(sceneList.ToArray(), path, BuildTarget.Android, BuildOptions.None);
    }

    [MenuItem("Lego/检查图标资源")]
    public static void CheckImages()
    {
        Sprite spt = null;
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.AppendLine("---------------设备图标无法找到：---------------");
        var dvs = ManagerData.Constance.DeviceDataList;
        foreach (var d in dvs)
        {
            string resName = d.GetResourceName();
            spt = getSpirteInEditor(resName);
            if (spt == null)
            {
                stringBuilder.AppendLine(resName);
            }
        }

        stringBuilder.AppendLine("---------------部件图标无法找到：---------------");
        foreach (var d in dvs)
        {
            stringBuilder.AppendLine("/////" + d.GetResourceName());
            foreach (var p in d.Parts)
            {
                spt = getSpirteInEditor(p.ImageName);
                if (spt == null)
                {
                    stringBuilder.AppendLine(p.ImageName);
                }
            }
        }

        stringBuilder.AppendLine("---------------工具图标无法找到：---------------");
        var tools = ManagerData.Constance.ToolDataList;
        foreach (var t in tools)
        {
            spt = getSpirteInEditor(t.imageName);
            if (spt == null)
            {
                stringBuilder.AppendLine(t.imageName);
            }
        }

        Debug.Log(stringBuilder);
    }
    private static Sprite getSpirteInEditor(string sptName)
    {
        return null;
    }
}

